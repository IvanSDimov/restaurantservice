﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RestaurantService.Data.Models
{
    public class ModerationReason
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [NotMapped]
        public IList<ClientComment> ClientComments { get; set; }
    }
}
