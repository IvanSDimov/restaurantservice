﻿using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RestaurantService.Data.Models
{
    public class Logbook
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ManagerComment> ManagerComments { get; set; }
        public int? EstablishmentId { get; set; }
        public Establishment Establishment { get; set; }
        //TODO
        [NotMapped]
        //public List<ManagerComment> managerComments { get; set; }
        public ICollection<ManagersLogbooks> ManagersLogbooks { get; set; }
        [NotMapped]
        public ICollection<ApplicationUser> Managers { get; set; }
        public bool IsDeleted { get; set; }
    }
}
