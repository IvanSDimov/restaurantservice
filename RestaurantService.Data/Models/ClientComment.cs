﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantService.Data.Models
{
    public class ClientComment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string  Author { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsDeleted { get; set; }
        public int ? EstablishmentId { get; set; }
        public Establishment Establishment { get; set; }
        public int ? ModerationId { get; set; }
        public Moderation Moderation { get; set; }

    }
}
