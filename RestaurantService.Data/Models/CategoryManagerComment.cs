﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantService.Data.Models
{
    public class CategoryManagerComment
    {
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int ManagerCommentId { get; set; }
        public ManagerComment ManagerComment { get; set; }
    }
}
