﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RestaurantService.Data.Models
{
    public class Address
    {
        public int Id { get; set; }
        public int? CityId { get; set; }
        public City City { get; set; }

        public ICollection<Establishment> Establishment { get; set; }

        public string Street { get; set; }
    }
}
