﻿using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantService.Data.Models
{
    public class ManagersLogbooks
    {
        public ApplicationUser Manager { get; set; }
        public string ManagerId { get; set; }
        public Logbook LogBook { get; set; }
        public int LogbookId { get; set; }
    }
}
