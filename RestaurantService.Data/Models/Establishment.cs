﻿using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RestaurantService.Data.Models
{
    public class Establishment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? AddressId { get; set; }
        public  Address Address { get; set; }
        public string AdminId { get; set; }
        public ApplicationUser Admin { get; set; }
        //public ICollection<EstablishmentsKinds> EstablishmentsKinds { get; set; }
        public int? KindId { get; set; }
        public Kind Kind { get; set; }
        //public ICollection<ManagersEstablishments> Managers{get;set;}
        public ICollection<ClientComment> ClientComments{get;set;}
        //public ICollection<ManagerComment> ManagerComments{get;set;}
        [NotMapped]
        public ICollection<Logbook> Logbooks { get; set; }
        [NotMapped]
        public ICollection<Logbook> DeletedLogbooks { get; set; }
        public string Image { get; set; }
        //public ICollection<Category> Categories{get;set;} 
        public bool IsDeleted { get; set; }
    }
}
