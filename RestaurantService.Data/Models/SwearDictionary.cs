﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantService.Data.Models
{
    public class SwearDictionary
    {
        public int Id {get;set;}
        public string Words { get; set; }        
    }
}
