﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RestaurantService.Data.Models
{
    public class Kind
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public IList<Establishment> Establishments { get; set; }
    }
}
