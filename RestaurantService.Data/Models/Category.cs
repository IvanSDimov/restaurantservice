﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RestaurantService.Data.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [NotMapped]
        IList<ManagerComment> ManagerComments { get; set; }
        public IList<CategoryManagerComment>CategoryManagerComment{ get; set; }

    }
}
