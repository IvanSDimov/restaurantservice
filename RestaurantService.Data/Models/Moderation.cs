﻿using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantService.Data.Models
{
    public class Moderation
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string AuthorId { get; set; }
        public ApplicationUser Author{get;set;}
        public ModerationReason ModerationReason { get; set; }
        public int? ModerationReasonId { get; set; }
    }
}
