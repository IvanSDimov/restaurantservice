﻿using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RestaurantService.Data.Models
{
    public class ManagerComment
    {
        public int Id { get; set; }

        public string Text { get; set; }
        public string AuthorId { get; set; }
        public ApplicationUser Author { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsDone { get; set; }
        public bool IsDeleted { get; set; }
        public int? LogbookId { get; set; }
        public Logbook Logbook { get; set; }
        public byte[] ByteImage { get; set; }
        [NotMapped]
        public IList<Category> Category { get; set; }
        public IList<CategoryManagerComment> CategoryManagerComment { get; set; }
    }
}
