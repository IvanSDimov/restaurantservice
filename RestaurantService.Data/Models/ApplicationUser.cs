﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using RestaurantService.Data.Models;

namespace RestaurantService.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string Avatar { get; set; }
        [NotMapped]
        public string Role { get; set; }
        public bool IsDeleted { get; set; }
        public string  AdminId { get; set; }
        public ApplicationUser Admin { get; set; }
        [NotMapped]
        public IList<ClientComment>ClientComments{get;set;}
        public IList<ManagerComment>ManagerComments{get;set;}
        public IList<ManagersLogbooks> ManagerLogbooks { get; set; }
        public IList<Establishment>Establishment{get;set;} 
        [NotMapped]
        public IList<Logbook> Logbooks { get; set; }
    }
}
