﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestaurantService.Data.Migrations
{
    public partial class databaseupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Logbooks_AspNetUsers_ApplicationUserId",
                table: "Logbooks");

            migrationBuilder.DropIndex(
                name: "IX_Logbooks_ApplicationUserId",
                table: "Logbooks");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "Logbooks");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "Logbooks",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Logbooks_ApplicationUserId",
                table: "Logbooks",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Logbooks_AspNetUsers_ApplicationUserId",
                table: "Logbooks",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
