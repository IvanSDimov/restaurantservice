﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestaurantService.Data.Migrations
{
    public partial class NullableModerationReason : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Moderations_AspNetUsers_AutorId",
                table: "Moderations");

            migrationBuilder.DropForeignKey(
                name: "FK_Moderations_ModerationReasons_ModerationReasonId",
                table: "Moderations");

            migrationBuilder.DropIndex(
                name: "IX_Moderations_AutorId",
                table: "Moderations");

            migrationBuilder.DropColumn(
                name: "AutorId",
                table: "Moderations");

            migrationBuilder.AlterColumn<int>(
                name: "ModerationReasonId",
                table: "Moderations",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "AuthorId",
                table: "Moderations",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Moderations_AuthorId",
                table: "Moderations",
                column: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Moderations_AspNetUsers_AuthorId",
                table: "Moderations",
                column: "AuthorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Moderations_ModerationReasons_ModerationReasonId",
                table: "Moderations",
                column: "ModerationReasonId",
                principalTable: "ModerationReasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Moderations_AspNetUsers_AuthorId",
                table: "Moderations");

            migrationBuilder.DropForeignKey(
                name: "FK_Moderations_ModerationReasons_ModerationReasonId",
                table: "Moderations");

            migrationBuilder.DropIndex(
                name: "IX_Moderations_AuthorId",
                table: "Moderations");

            migrationBuilder.AlterColumn<int>(
                name: "ModerationReasonId",
                table: "Moderations",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AuthorId",
                table: "Moderations",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AutorId",
                table: "Moderations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Moderations_AutorId",
                table: "Moderations",
                column: "AutorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Moderations_AspNetUsers_AutorId",
                table: "Moderations",
                column: "AutorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Moderations_ModerationReasons_ModerationReasonId",
                table: "Moderations",
                column: "ModerationReasonId",
                principalTable: "ModerationReasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
