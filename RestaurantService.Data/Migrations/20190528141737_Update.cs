﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestaurantService.Data.Migrations
{
    public partial class Update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientComments_AspNetUsers_ApplicationUserId",
                table: "ClientComments");

            migrationBuilder.DropForeignKey(
                name: "FK_ClientComments_ModerationReasons_ModerationReasonId",
                table: "ClientComments");

            migrationBuilder.DropIndex(
                name: "IX_ClientComments_ApplicationUserId",
                table: "ClientComments");

            migrationBuilder.DropIndex(
                name: "IX_ClientComments_ModerationReasonId",
                table: "ClientComments");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "ClientComments");

            migrationBuilder.DropColumn(
                name: "ModerationReasonId",
                table: "ClientComments");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "ClientComments",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ModerationReasonId",
                table: "ClientComments",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ClientComments_ApplicationUserId",
                table: "ClientComments",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientComments_ModerationReasonId",
                table: "ClientComments",
                column: "ModerationReasonId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClientComments_AspNetUsers_ApplicationUserId",
                table: "ClientComments",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClientComments_ModerationReasons_ModerationReasonId",
                table: "ClientComments",
                column: "ModerationReasonId",
                principalTable: "ModerationReasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
