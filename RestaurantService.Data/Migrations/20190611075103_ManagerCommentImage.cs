﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RestaurantService.Data.Migrations
{
    public partial class ManagerCommentImage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "ByteImage",
                table: "ManagerComments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ByteImage",
                table: "ManagerComments");
        }
    }
}
