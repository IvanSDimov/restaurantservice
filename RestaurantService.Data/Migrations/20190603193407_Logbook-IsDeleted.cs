﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestaurantService.Data.Migrations
{
    public partial class LogbookIsDeleted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Logbooks",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Logbooks");
        }
    }
}
