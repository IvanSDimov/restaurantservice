﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RestaurantService.Data.Models;
using RestaurantService.Models;

namespace RestaurantService.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Category>Categories { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<ClientComment> ClientComments { get; set; }
        public DbSet<ManagerComment> ManagerComments { get; set; }
        public DbSet<Kind> Kinds { get; set; }
        public DbSet<Establishment> Establishments { get; set; }
        public DbSet<Moderation> Moderations { get; set; }
        public DbSet<ModerationReason> ModerationReasons { get; set; }
        public DbSet<SwearDictionary> SwearDictionaries { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<ManagersLogbooks> ManagersLogbooks { get; set; }
        public DbSet<CategoryManagerComment> CategoryManagerComment { get; set; }
        public DbSet<Logbook> Logbooks { get; set; }
        

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);



            builder.Entity<ManagersLogbooks>()
                .HasKey(x => new { x.ManagerId, x.LogbookId });
            builder.Entity<CategoryManagerComment>()
                .HasKey(x => new { x.CategoryId, x.ManagerCommentId });
        }

    }
}
