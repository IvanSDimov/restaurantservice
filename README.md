# RestaurantService

***1.  Relationships***

The architecture of the data base is represented by the following [Database diagram](https://gitlab.com/IvanSDimov/restaurantservice/blob/master/restaurantserviceDiagram-1.pdf).

Users are **administrators**, **managers**, **moderators** and **clients** (not logged-in).

**Administrators** create **managers**, **moderators** and other **administrators**. Each administrator can edit (update or delete/undelete) managers and moderators. created by himself.
**Administrators** also create **establishments** and **logbooks** for each establishment. They assign/unassign managers to the logbooks and logbooks to the managers. 
Administrators can edit establishments and logbooks created by them. **Logbooks** can be deleted/undeleted.
Each **administrator** operates only on the users, establishments and logbooks created by himself.

Each **manager** can be assigned to one or more **logbooks**. A **logbook** can be assigned to one or more **managers**. 
A **manager** leaves **comments** to the **logbooks** he is assign to and is able to see the comments by other managers in these logbooks.
A **manager's comment** can be edited only by its author.
A **category** can be assigned/unassigned to a comment by every manager who sees this comment.

**Clients** leave **comments** to **establishments**. These comments are visible for everyone.
**Clients' comments** are automoderated if contain words from a **Swear Dictionary**. If more then 20% of the words in the comment are inappropriate, the comment is not saved in the data base.

**Moderators** can edit **clients' comments**. They can point a **reason for the moderation**.

An **establishment** has a list of **logbooks** and a list of **clients' comments**.
A **logbook** has a list of **managers** and a list of **managers' comments**. 
A **manager** has a list of **logbooks**.
An **administrator** has a list of **users** and and a list of **establishments** created by himself.
A **client's comment** can have a **moderation** (with **author** and/or **moderation reason**)

***2.  Project architecture***

The project is organized in 4 layers.
*  **Data layer**

Contains the entities from the database.
* [ ]      Address (with City and Street - for an establishment)
* [ ]      Application User(moderators, administrators, managers)
* [ ]      Category (for managers' comments)
* [ ]      ClientComment
* [ ]      Establishment
* [ ]      Kind (for establishmens)
* [ ]      Logbook
* [ ]      ManagerComment
* [ ]      Moderation (with Author - moderator, and ModerationReason
* [ ]      SwearDictionary

Also contains classes for the relationships between the entities.
* [ ]      ManagerComments
* [ ]      ManagersLogbooks

*  **Servise layer**

Contains the business logic of the application.
* [ ]      AddressService - methods for creating and searching.
* [ ]      CategoryService - methods for listing and searching.
* [ ]      CityService - a method for listing.
* [ ]      ClientCommentService - methods for creating, searching, updating, checking for swear words.
* [ ]      EstablishmentService - methods for creating, searching, listing, updating.
* [ ]      JSonService - methods for seeding the data base.
* [ ]      KindService - a method for searching.
* [ ]      LogbookService - methods for creating, searching, listing, updating, deleting/undeleting, sorting.
* [ ]      ManagerCommentService - methods for creating, searching, listing, editing, deleting, sorting.
* [ ]      ModerationService - methods for creating, searching, listing.
* [ ]      UserService - methods for searching, listing, deleting/undeliting, assigning managers to logbooks.



*  **UI Layer**

Contains the controllers, views, view models representing the database entities in the views, and mappers translating database entities to view models.

  **Free Area** - for clients and moderators.
    
        *Controllers*
* [ ]      AccountController - from ASP.Net Core.
* [ ]      CommentController - an action for creating, saving and editing clients' comments.
* [ ]      EstablishmentController - an action for displaying details (clients' comments) for an estyabllishment.
* [ ]      HomeController - actions for displaying establishments.
* [ ]      InputController - a generic action for uploading an image.
* [ ]      ErrorController - an action for displaying user-fiendly error pages.
* [ ]      ManageController - from ASP.Net Core.
* [ ]      SearchController - removig the cache search model.

    **Admin Area** - for administrators
    
        *Controllers*
* [ ]      CreateEstablishmentController - actions for creating and updating establishments and for adding managers to logbooks.
* [ ]      HomeController - actions for listing users and seeding the data base.
* [ ]      EstablishmentController - an action for displaying details (clients' comments) for an estyabllishment.
* [ ]      HomeController - actions for displaying establishments.
* [ ]      LogbookController - actions for updating a logbook.
* [ ]      UserController - actions for creating, updating, deleting and undeleting users.
* 
    **Manager Area** - for managers

        *Controllers*
* [ ]      HomeController - an actions for displaying logbooks of an user.
* [ ]      LogbookController - actions for deleting images, displaying immages from managers' commants, listing and displaying managers' comments.



*  **Test layer**

Contains unit tests for the business logic of the application.

