﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KindService.Tests
{
    [TestClass]
    public class GetKindByName_Should
    {
        [TestMethod]
        public async Task GetKindByNameIfExists()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "FindKindByNameIfExists")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var kind1 = new Kind { Name = "kind1" };
                context.Kinds.Add(kind1);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.KindService(context);
                var foundKind = await sut.GetKindByName("kind1");

                Assert.IsTrue(foundKind.Name== "kind1");


            }
        }

        [TestMethod]
        public async Task ReturnNullIfKindDoesNotExist()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnNullIfKindDoesNotExist")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.KindService(context);
                var foundKind = await sut.GetKindByName("kind1");

                Assert.IsTrue(foundKind == null);
            }
        }
    }
}