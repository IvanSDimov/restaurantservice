﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KindService.Tests
{
    [TestClass]
    public class ListKinds_Should
    {
        [TestMethod]
        public async Task ListKinds()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ListKinds")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var kind1 = new Kind { Name = "kind1" };
                var kind2 = new Kind { Name = "kind2" };
                context.Kinds.Add(kind1);
                context.Kinds.Add(kind2);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.KindService(context);
                var foundKinds = await sut.ListKinds();

                Assert.IsTrue(foundKinds.Count==2);
                Assert.IsTrue(foundKinds.Any(k=>k.Name == "kind1"));
                Assert.IsTrue(foundKinds.Any(k => k.Name == "kind2"));


            }
        }
    }
}
