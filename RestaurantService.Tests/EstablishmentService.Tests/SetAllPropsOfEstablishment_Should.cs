﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.EstablishmentService.Tests
{
    [TestClass]
    public class SetAllPropsOfEstablishment_Should
    {
        [TestMethod]
        public async Task SetLogbookToEstablishmentWhenRightValuesArePassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
              .UseInMemoryDatabase(databaseName: "SetLogbookToEstablishmentWhenRightValuesArePassed")
              .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var establishment = new Establishment() { Id = 1 };
                arrangeContext.Add(establishment);

                var kind = new Kind() { Id = 1, Establishments = new List<Establishment>() { establishment } };
                arrangeContext.Add(kind); 

                var city1 = new City { Name = "City1", Id = 1 };                
                arrangeContext.Cities.AddRange( city1 );

                var address1 = new Address() { Id = 1, CityId = 1, Establishment = new List<Establishment>() { establishment } };               
                arrangeContext.Addresses.AddRange( address1 );

                var logbook1 = new Logbook() { Id = 1, Name = "Logbook1", EstablishmentId = 1 };
                arrangeContext.Logbooks.Add(logbook1);

                var logbook2 = new Logbook() { Id = 2, Name = "Logbook2", EstablishmentId = 1 };
                arrangeContext.Logbooks.Add(logbook2);

                var logbook3 = new Logbook() { Id = 3, Name = "Logbook3", EstablishmentId = 1 };
                arrangeContext.Logbooks.Add(logbook3);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);
                var sut = new Services.EstablishmentService(context, logbookService);

                var establishmentId = 1;
                var establishment = await sut.SetAllPropsOfEstablishment(establishmentId);
                var expectedKind = await context.Kinds.FindAsync(1);
                var expectedAddress = await context.Addresses.FindAsync(1);
                var espectedLogbooks = await context.Logbooks.ToListAsync();
                var actualLogbooks = establishment.Logbooks.ToList();
                CollectionAssert.AreEqual(espectedLogbooks, actualLogbooks);
                Assert.AreEqual(expectedKind, establishment.Kind);
                Assert.AreEqual(expectedAddress, establishment.Address);
            }
        }

    }
}
