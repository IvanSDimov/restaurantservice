﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.EstablishmentService.Tests
{
    [TestClass]
    public class GetEstablishmentById_Should
    {
        [TestMethod]
        public async Task ReturnRightEstablishment()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ReturnRightEstablishment")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var establishment1 = new Establishment() { Id = 1, Name= "Establishment1" };
                arrangeContext.Add(establishment1);

                var establishment2 = new Establishment() { Id = 2, Name = "Establishment2" };
                arrangeContext.Add(establishment2);

                var establishment3 = new Establishment() { Id = 3, Name = "Establishment3" };
                arrangeContext.Add(establishment3);

                var establishment4 = new Establishment() { Id = 4, Name = "Establishment4" };
                arrangeContext.Add(establishment4);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);
                var sut = new Services.EstablishmentService(context, logbookService);

                var establishmentId = 1;
                var establishment = await sut.GetEstablishmentById(establishmentId);
                Assert.AreEqual(establishment.Id, establishmentId);
                Assert.AreEqual(establishment.Name, "Establishment1");
            }
        }
        [TestMethod]
        public async Task ReturnNullIfEstablishmentDoesNotExist()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ReturnNullIfEstablishmentDoesNotExist")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var establishment1 = new Establishment() { Id = 1, Name = "Establishment1" };
                arrangeContext.Add(establishment1);

                var establishment2 = new Establishment() { Id = 2, Name = "Establishment2" };
                arrangeContext.Add(establishment2);

                var establishment3 = new Establishment() { Id = 3, Name = "Establishment3" };
                arrangeContext.Add(establishment3);

                var establishment4 = new Establishment() { Id = 4, Name = "Establishment4" };
                arrangeContext.Add(establishment4);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);
                var sut = new Services.EstablishmentService(context, logbookService);

                var establishmentId = 5;
                var establishment = await sut.GetEstablishmentById(establishmentId);
                Assert.AreEqual(establishment, null);                
            }
        }
    }
}
