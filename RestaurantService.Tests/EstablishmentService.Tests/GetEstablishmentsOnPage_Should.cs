﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.EstablishmentService.Tests
{
    [TestClass]
    public class GetEstablishmentsOnPage_Should
    {
        [TestMethod]
        public async Task ReturnsRightEstablishmentsOnPageWhenNoAdminIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ReturnsRightEstablishmentsOnPageWhenNoAdminIsPassed")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var establishment1 = new Establishment() { Id = 1 };
                arrangeContext.Add(establishment1);

                var establishment2 = new Establishment() { Id = 2 };
                arrangeContext.Add(establishment2);

                var establishment3 = new Establishment() { Id = 3 };
                arrangeContext.Add(establishment3);

                var establishment4 = new Establishment() { Id = 4 };
                arrangeContext.Add(establishment4);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);
                var sut = new Services.EstablishmentService(context, logbookService);

                var expectedEstablishments = context.Establishments.Where(e => e.Id > 2).ToList();
                var establishments1 = await sut.GetEstablishmentsOnPage(2,1,null);
                var establishments = establishments1.ToList();
                CollectionAssert.AreEqual(expectedEstablishments, establishments);
            }
        }
        [TestMethod]
        public async Task ReturnsRightEstablishmentsOnPage()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ReturnsRightEstablishmentsOnPage")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var admin = new ApplicationUser() { UserName = "Admin" };
                arrangeContext.Users.Add(admin);

                var establishment1 = new Establishment() { Id = 1 };
                establishment1.Admin = admin;
                arrangeContext.Add(establishment1);

                var establishment2 = new Establishment() { Id = 2 };
                establishment2.Admin = admin;
                arrangeContext.Add(establishment2);

                var establishment3 = new Establishment() { Id = 3 };
                establishment3.Admin = admin;
                arrangeContext.Add(establishment3);

                var establishment4 = new Establishment() { Id = 4 };
                arrangeContext.Add(establishment4);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);
                var sut = new Services.EstablishmentService(context, logbookService);

                var admin = await context.Users.FirstOrDefaultAsync(c => c.UserName == "Admin");
                var adminId = admin.Id;
                var expectedEstablishments = context.Establishments.Where(e => e.Id == 3).ToList();
                var establishments1 = await sut.GetEstablishmentsOnPage(2, 1, adminId);
                var establishments = establishments1.ToList();
                CollectionAssert.AreEqual(expectedEstablishments, establishments);
            }
        }
    }
}
