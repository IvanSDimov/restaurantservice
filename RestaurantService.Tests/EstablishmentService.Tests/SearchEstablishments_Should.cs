﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Tests.EstablishmentService.Tests
{
    [TestClass]
    public class SearchEstablishments_Should
    {
        [TestMethod]
        public async Task ReturnAllEstablishmentsWhenNoSerchValuesAndNoAdminArePassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ReturnAllEstablishmentsWhenNoSerchValuesArePassed")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var admin = new ApplicationUser() { UserName = "Admin" };
                arrangeContext.Users.Add(admin);

                var establishment1 = new Establishment() { Id = 1 };
                establishment1.Admin = admin;
                arrangeContext.Add(establishment1);

                var establishment2 = new Establishment() { Id = 2 };
                establishment2.Admin = admin;
                arrangeContext.Add(establishment2);

                var establishment3 = new Establishment() { Id = 3 };
                establishment3.Admin = admin;
                arrangeContext.Add(establishment3);

                var establishment4 = new Establishment() { Id = 4 };
                arrangeContext.Add(establishment4);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);

                var sut = new Services.EstablishmentService(context, logbookService);

                var establishments1 = await sut.SearchEstablishments(5, 0,null,null,null,null);
                var establishments = establishments1.Item1.ToList();
                var expectedcount = 4;
                var expectedEstablishments = context.Establishments.ToList();

                Assert.AreEqual(establishments1.Item2, expectedcount);
                CollectionAssert.AreEqual(establishments, expectedEstablishments);
            }
        }
        [TestMethod]
        public async Task ReturnRightEstablishmentsWhenNoSerchValuesOnlyAdminArePassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ReturnRightEstablishmentsWhenNoSerchValuesOnlyAdminArePassed")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var admin = new ApplicationUser() { UserName = "Admin" };
                arrangeContext.Users.Add(admin);

                var establishment1 = new Establishment() { Id = 1 };
                establishment1.Admin = admin;
                arrangeContext.Add(establishment1);

                var establishment2 = new Establishment() { Id = 2 };
                establishment2.Admin = admin;
                arrangeContext.Add(establishment2);

                var establishment3 = new Establishment() { Id = 3 };
                establishment3.Admin = admin;
                arrangeContext.Add(establishment3);

                var establishment4 = new Establishment() { Id = 4 };
                arrangeContext.Add(establishment4);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);

                var sut = new Services.EstablishmentService(context, logbookService);

                var admin = await context.Users.FirstOrDefaultAsync(c => c.UserName == "Admin");
                var adminId = admin.Id;

                var establishments1 = await sut.SearchEstablishments(5, 0, null, null, null, adminId);
                var establishments = establishments1.Item1.ToList();
                var expectedcount = 3;
                var expectedEstablishments = context.Establishments.Where(e=>e.Id<4).ToList();

                Assert.AreEqual(establishments1.Item2, expectedcount);
                CollectionAssert.AreEqual(establishments, expectedEstablishments);
            }
        }
        [TestMethod]
        public async Task ReturnRightEstablishmentsWhenOnlyTownsArePassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ReturnRightEstablishmentsWhenOnlyTownsArePassed")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var admin = new ApplicationUser() { UserName = "Admin" };
                arrangeContext.Users.Add(admin);

                var city1 = new City { Name = "City1", Id = 1 };
                var city2 = new City { Name = "City2", Id = 2 };
                arrangeContext.Cities.AddRange(new List<City> { city1, city2 });

                var address1 = new Address() { Id = 1, CityId = 1 };
                var address2 = new Address() { Id = 2, CityId = 2 };
                arrangeContext.Addresses.AddRange(new List<Address> { address1, address2 });

                var establishment1 = new Establishment() { Id = 1,Name="Establishment1" };
                establishment1.Admin = admin;
                establishment1.AddressId = 1;
                arrangeContext.Establishments.Add(establishment1);

                var establishment2 = new Establishment() { Id = 2, Name = "Establishment2" };
                establishment2.Admin = admin;
                establishment2.AddressId = 1;
                arrangeContext.Establishments.Add(establishment2);

                var establishment3 = new Establishment() { Id = 3, Name = "Establishment3" };
                establishment3.Admin = admin;
                establishment3.AddressId = 2;
                arrangeContext.Establishments.Add(establishment3);

                var establishment4 = new Establishment() { Id = 4, Name = "Establishment4" };
                establishment4.Admin = admin;
                arrangeContext.Establishments.Add(establishment4);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);

                var sut = new Services.EstablishmentService(context, logbookService);

                var admin = await context.Users.FirstOrDefaultAsync(c => c.UserName == "Admin");
                var adminId = admin.Id;

                var townIds = new List<int>() { 1 };

                var establishments1 = await sut.SearchEstablishments(5, 0, null, townIds, null, adminId);
                var establishments = establishments1.Item1.ToList();
                var expectedcount = 2;
                var expectedEstablishments = context.Establishments.Where(e => e.Id < 3).ToList();

                Assert.AreEqual(expectedcount, establishments1.Item2);
                CollectionAssert.AreEqual(establishments, expectedEstablishments);
            }
        }
    }
}
