﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.EstablishmentService.Tests
{
    [TestClass]
    public class FindEstablishmentByNameAndAddress_Should
    {
        [TestMethod]
        public async Task ReturnRightEstablishmentWhenValidInputIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
              .UseInMemoryDatabase(databaseName: "ReturnRightEstablishmentWhenValidInputIsPassed")
              .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {

                var city1 = new City { Name = "City1", Id = 1 };
                var city2 = new City { Name = "City2", Id = 2 };
                arrangeContext.Cities.AddRange(new List<City> { city1, city2 });

                var address1 = new Address() { Id = 1, CityId = 1 };
                var address2 = new Address() { Id = 2, CityId = 2 };
                arrangeContext.Addresses.AddRange(new List<Address> { address1, address2 });

                var establishment1 = new Establishment() { Id = 1, AddressId = 1, Name = "Establisment" };
                arrangeContext.Add(establishment1);

                var establishment2 = new Establishment() { Id = 2, AddressId = 2, Name = "Establisment" };
                arrangeContext.Add(establishment2);

                var establishment3 = new Establishment() { Id = 3, AddressId = 2, Name = "Establisment2" };
                arrangeContext.Add(establishment3);

                var establishment4 = new Establishment() { Id = 4, AddressId = 1, Name = "Establisment2" };
                arrangeContext.Add(establishment4);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);
                var sut = new Services.EstablishmentService(context, logbookService);

                var name = "Establisment";
                var addressId = 1; 
                var expectedEstablishment = context.Establishments.Find(1);
                var actualEstablishment = await sut.FindEstablishmentByNameAndAddress(name, addressId);
                Assert.AreEqual(expectedEstablishment, actualEstablishment);
            }
        }
        [TestMethod]
        public async Task ReturnNullWhenWrongNameIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
              .UseInMemoryDatabase(databaseName: "ReturnNullWhenWrongNameIsPassed")
              .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {

                var city1 = new City { Name = "City1", Id = 1 };
                var city2 = new City { Name = "City2", Id = 2 };
                arrangeContext.Cities.AddRange(new List<City> { city1, city2 });

                var address1 = new Address() { Id = 1, CityId = 1 };
                var address2 = new Address() { Id = 2, CityId = 2 };
                arrangeContext.Addresses.AddRange(new List<Address> { address1, address2 });

                var establishment1 = new Establishment() { Id = 1, AddressId = 1, Name = "Establisment" };
                arrangeContext.Add(establishment1);

                var establishment2 = new Establishment() { Id = 2, AddressId = 2, Name = "Establisment" };
                arrangeContext.Add(establishment2);

                var establishment3 = new Establishment() { Id = 3, AddressId = 2, Name = "Establisment2" };
                arrangeContext.Add(establishment3);

                var establishment4 = new Establishment() { Id = 4, AddressId = 1, Name = "Establisment2" };
                arrangeContext.Add(establishment4);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);
                var sut = new Services.EstablishmentService(context, logbookService);

                var name = "WrongName";
                var addressId = 1;
                
                var actualEstablishment = await sut.FindEstablishmentByNameAndAddress(name, addressId);
                Assert.AreEqual(null, actualEstablishment);
            }
        }
        [TestMethod]
        public async Task ReturnNullWhenWrongAddressIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
              .UseInMemoryDatabase(databaseName: "ReturnNullWhenWrongAddressIsPassed")
              .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {

                var city1 = new City { Name = "City1", Id = 1 };
                var city2 = new City { Name = "City2", Id = 2 };
                arrangeContext.Cities.AddRange(new List<City> { city1, city2 });

                var address1 = new Address() { Id = 1, CityId = 1 };
                var address2 = new Address() { Id = 2, CityId = 2 };
                arrangeContext.Addresses.AddRange(new List<Address> { address1, address2 });

                var establishment1 = new Establishment() { Id = 1, AddressId = 1, Name = "Establisment" };
                arrangeContext.Add(establishment1);

                var establishment2 = new Establishment() { Id = 2, AddressId = 2, Name = "Establisment" };
                arrangeContext.Add(establishment2);

                var establishment3 = new Establishment() { Id = 3, AddressId = 2, Name = "Establisment2" };
                arrangeContext.Add(establishment3);

                var establishment4 = new Establishment() { Id = 4, AddressId = 1, Name = "Establisment2" };
                arrangeContext.Add(establishment4);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);
                var sut = new Services.EstablishmentService(context, logbookService);

                var name = "Establishment";
                var addressId = 4;

                var actualEstablishment = await sut.FindEstablishmentByNameAndAddress(name, addressId);
                Assert.AreEqual(null, actualEstablishment);
            }
        }
    }
}
