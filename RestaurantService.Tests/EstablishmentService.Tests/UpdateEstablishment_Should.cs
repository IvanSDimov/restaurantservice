﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.EstablishmentService.Tests
{
    [TestClass]
    public class UpdateEstablishment_Should
    {
        [TestMethod]
        public async Task UpdateEstablishment()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "UpdateEstablishment")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var admin = new ApplicationUser() { UserName = "Admin" };
                arrangeContext.Users.Add(admin);

                var establishment = new Establishment() { Id = 1, Name = "Establishment1" };                
                arrangeContext.Establishments.Add(establishment);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);

                var sut = new Services.EstablishmentService(context, logbookService);

                var establishment = await context.Establishments.FindAsync(1);
                var admin = await context.Users.FirstOrDefaultAsync(c => c.UserName == "Admin");
                var adminId = admin.Id;

                establishment.Name = "TestName";
                establishment.AdminId = adminId;
                await sut.UpdateEstablishment(establishment);
                var actualEstablishment= await context.Establishments.FindAsync(1);
                Assert.AreEqual("TestName", actualEstablishment.Name);
                Assert.AreEqual(admin, actualEstablishment.Admin);
            }
        }
    }
}
