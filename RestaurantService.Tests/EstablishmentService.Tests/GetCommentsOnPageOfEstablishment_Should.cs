﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.EstablishmentService.Tests
{
    [TestClass]
    public class GetCommentsOnPageOfEstablishment_Should
    {
        [TestMethod]
        public async Task ReturnsRightComents()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ReturnsRightComents")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var establishment = new Establishment() { Id = 1, Name = "Establishment1" };
                arrangeContext.Establishments.Add(establishment);

                var comment1 = new ClientComment() { Id = 1, EstablishmentId = 1 };
                arrangeContext.ClientComments.Add(comment1);

                var comment2 = new ClientComment() { Id = 2, EstablishmentId = 1 };
                arrangeContext.ClientComments.Add(comment2);

                var comment3 = new ClientComment() { Id = 3, EstablishmentId = 1 };
                arrangeContext.ClientComments.Add(comment3);

                var comment4 = new ClientComment() { Id = 4 };
                arrangeContext.ClientComments.Add(comment4);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);

                var sut = new Services.EstablishmentService(context, logbookService);

                var expectedComments = context.ClientComments.Where(c=>c.Id<3).ToList();
                var commentsTuple = await sut.GetCommentsOnPageOfEstablishment(1, 0, 2);
                Assert.AreEqual(3, commentsTuple.Item2);
                CollectionAssert.AreEqual(expectedComments, commentsTuple.Item1.ToList());
            }
        }
    }
}
