﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Models;
using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.EstablishmentService.Tests
{
    [TestClass]
    public class CreateEstablishment_Should
    {
        public async Task CreatesNewEstablishmentWhitPssedValues()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
              .UseInMemoryDatabase(databaseName: "ReturnsRightEstablishmentsOnPage")
              .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var admin = new ApplicationUser() { UserName = "Admin" };
                arrangeContext.Users.Add(admin);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);

                var logbookService = new Services.LogbookService(context, userService);
                var sut = new Services.EstablishmentService(context, logbookService);

                var name = "Establishment";
                var address = new Address() { Id=1 };
                var newFileName = "newFileName";
                var kind = new Kind() { Name="Kind" };
                var admin = await context.Users.FirstOrDefaultAsync(c => c.UserName == "Admin");
                var adminId = admin.Id;

                var establishment = await sut.CreateEstablishment(name,address,newFileName,kind,adminId);
                Assert.AreEqual(establishment.Name,name);
                Assert.AreEqual(establishment.Address, address);
                Assert.AreEqual(establishment.Image, newFileName);
                Assert.AreEqual(establishment.Kind, kind);
                Assert.AreEqual(establishment.Admin,admin);
            }
        }
    }
}
