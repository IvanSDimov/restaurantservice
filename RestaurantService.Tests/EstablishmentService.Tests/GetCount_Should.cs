﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.EstablishmentService.Tests
{
    [TestClass]
    public class GetCount_Should
    {
        [TestMethod]
        public async Task ReturnCountOfAllEstablishments()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ReturnCountOfAllEstablishments")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var establishment1 = new Establishment() { Id = 1 };
                arrangeContext.Add(establishment1);

                var establishment2 = new Establishment() { Id = 2 };
                arrangeContext.Add(establishment2);

                var establishment3 = new Establishment() { Id = 3 };
                arrangeContext.Add(establishment3);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {                
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var userService = new Services.UserService(context, fakeServiceProvider.Object, userManager);
                
                var logbookService = new Services.LogbookService(context,userService);
                var sut = new Services.EstablishmentService(context, logbookService);

                var  count = await sut.GetCount();
                Assert.AreEqual(3,count);
            }
        }
    }
}
