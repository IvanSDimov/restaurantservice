﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using RestaurantService.Services;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace ClientCommentService.Test
{
    [TestClass]
    public class UpdateComment_Should
    {

        [TestMethod]
        public async Task ChangeCommentText()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ChangeCommentText")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                context.ClientComments.Add(new ClientComment { EstablishmentId = 1, Text = "text" });
                context.SaveChanges();

            }

            using (var context = new ApplicationDbContext(options))
            {
                var comment = context.ClientComments.FirstOrDefault(c => c.EstablishmentId == 1);
                var sut = new RestaurantService.Services.ClientCommentService(context);
                comment = await sut.UpdateComment(comment.Id, "edited text");
            }
            using (var context = new ApplicationDbContext(options))
            {
                var editedComment = await context.ClientComments.FirstOrDefaultAsync(c => c.EstablishmentId == 1);
                Assert.AreEqual(editedComment.Text, "edited text");
            }
        }


        [TestMethod]
        public async Task AddModerationIfPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "AddModerationIfPassed")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.ClientComments.Add(new ClientComment { EstablishmentId = 1, Text = "text" });
                arrangeContext.ModerationReasons.Add(new ModerationReason { Name = "reason" });
                arrangeContext.Users.Add(new ApplicationUser { UserName = "moderator" });
                arrangeContext.SaveChanges();
                var reason = arrangeContext.ModerationReasons.FirstOrDefault(mr => mr.Name == "reason");
                var moderator = arrangeContext.Users.FirstOrDefault(u => u.UserName == "moderator");
                arrangeContext.Moderations.Add(new Moderation
                {
                    ModerationReasonId = reason.Id,
                    AuthorId = moderator.Id
                });
                arrangeContext.SaveChanges();
            }

            using (var context = new ApplicationDbContext(options))
            {
                var comment = context.ClientComments.FirstOrDefault(c => c.EstablishmentId == 1);
                var moderator = context.Users.FirstOrDefault(u => u.UserName == "moderator");
                var moderation = context.Moderations.FirstOrDefault(m => m.AuthorId == moderator.Id);
                var sut = new RestaurantService.Services.ClientCommentService(context);
                comment = await sut.UpdateComment(comment.Id, "text", moderation.Id);
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var moderator = assertContext.Users.FirstOrDefault(u => u.UserName == "moderator");
                var moderation = assertContext.Moderations.FirstOrDefault(m => m.AuthorId == moderator.Id);
                var editedComment = await assertContext.ClientComments.FirstOrDefaultAsync(c => c.EstablishmentId == 1);

                Assert.AreEqual(editedComment.ModerationId, moderation.Id);
            }
        }




    }
}
