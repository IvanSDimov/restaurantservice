﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using RestaurantService.Services;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace ClientCommentService.Test
{
    [TestClass]
    public class FindCommentById_Should
    {

        [TestMethod]
        public async Task FindCommentIfExists()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ChangeCommentText")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                context.ClientComments.Add(new ClientComment { EstablishmentId = 1, Text = "text" });
                context.SaveChanges();

            }

            using (var context = new ApplicationDbContext(options))
            {
                var comment = context.ClientComments.FirstOrDefault(c => c.EstablishmentId == 1);
                var sut = new RestaurantService.Services.ClientCommentService(context);
                var foundComment = await sut.FindCommentById(comment.Id);
                Assert.AreEqual(foundComment, comment);
            }
        }

        [TestMethod]
        public async Task ReturnNullIfCommentDoesNotExist()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnNullIfCommentDoesNotExist")
                .Options;


            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.ClientCommentService(context);
                var foundComment = await sut.FindCommentById(1);
                Assert.AreEqual(foundComment, null);
            }
        }
    }
}
