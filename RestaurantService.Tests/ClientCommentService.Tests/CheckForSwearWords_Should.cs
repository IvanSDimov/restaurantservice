﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientCommentService.Tests
{
    [TestClass]
    public class CheckForSwearWords_Should
    {
        [TestMethod]
        public async Task ReturnListOfSwearWordsIfAny()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnListOfSwearWordsIfAny")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var swear1 = new SwearDictionary { Words = "swear1" };
                var swear2 = new SwearDictionary { Words = "swear2" };
                var swear3 = new SwearDictionary { Words = "swear3" };
                context.SwearDictionaries.AddRange(new List<SwearDictionary> { swear1, swear2, swear3 });
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.ClientCommentService(context);
                var words = await sut.CheckForSwearWords(new List<string> { "Swear1", "swear1", "SWEAR2", "Swear2", "Swear4", "Swear5" });

                Assert.IsTrue(words.Count == 2);
                Assert.IsTrue(words.Any(w => w == "swear1"));
                Assert.IsTrue(words.Any(w => w == "swear2"));
            }
        }
        [TestMethod]
        public async Task ReturnEmptyListIfNoSwearWords()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnEmptyListIfNoSwearWords")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var swear1 = new SwearDictionary { Words = "swear1" };
                var swear2 = new SwearDictionary { Words = "swear2" };
                var swear3 = new SwearDictionary { Words = "swear3" };
                context.SwearDictionaries.AddRange(new List<SwearDictionary> { swear1, swear2, swear3 });
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.ClientCommentService(context);
                var words = await sut.CheckForSwearWords(new List<string> { "Swear4", "Swear5" });

                Assert.IsTrue(words.Count == 0);
            }
        }
    }
}



