﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using RestaurantService.Services;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace ClientCommentService.Test
{
    [TestClass]
    public class AddComment_Should
    {

        [TestMethod]
        public async Task AddCommentToDatabase()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "AddCommentToDatabase")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.ClientCommentService(context);
                var comment = await sut.AddComment(1,"text","Author",null);

            }
            using (var assertContext = new ApplicationDbContext(options))
            {
                var newComment=await assertContext.ClientComments.FirstOrDefaultAsync(c=>c.Text=="text");
                Assert.AreEqual(newComment.EstablishmentId, 1);
                Assert.AreEqual(newComment.Author, "Author");

            }
        }

        [TestMethod]
        public async Task AddAnonymousAsAuthorIfNoAuthorPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "AddAnonymousAsAuthorIfNoAuthorPassed")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.ClientCommentService(context);
                var comment = await sut.AddComment(1, "text", null, null);

            }
            using (var assertContext = new ApplicationDbContext(options))
            {
                var newComment = await assertContext.ClientComments.FirstOrDefaultAsync(c => c.Text == "text");
                Assert.AreEqual(newComment.EstablishmentId, 1);
                Assert.AreEqual(newComment.Author, "Anonymous");

            }
        }

        [TestMethod]
        public async Task AddModerationIfPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "AddModerationIfPassed")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.ModerationReasons.Add(new ModerationReason { Name = "reason" });
                arrangeContext.Users.Add(new ApplicationUser { UserName = "moderator" });
                arrangeContext.SaveChanges();
                var reason = arrangeContext.ModerationReasons.FirstOrDefault(mr => mr.Name == "reason");
                var moderator = arrangeContext.Users.FirstOrDefault(u => u.UserName == "moderator");
                arrangeContext.Moderations.Add(new Moderation
                {
                    ModerationReasonId = reason.Id,
                    AuthorId = moderator.Id
                });
                arrangeContext.SaveChanges();


            }

            using (var context = new ApplicationDbContext(options))
            {
                var moderator = context.Users.FirstOrDefault(u => u.UserName == "moderator");
                var moderation = context.Moderations.FirstOrDefault(m => m.AuthorId == moderator.Id);
                var sut = new RestaurantService.Services.ClientCommentService(context);
                var comment = await sut.AddComment(1, "text", null, moderation);

            }
            using (var assertContext = new ApplicationDbContext(options))
            {
                var moderator = assertContext.Users.FirstOrDefault(u => u.UserName == "moderator");
                var moderation = assertContext.Moderations.FirstOrDefault(m => m.AuthorId == moderator.Id);
                var newComment = await assertContext.ClientComments.FirstOrDefaultAsync(c => c.Text == "text");

                Assert.AreEqual(newComment.ModerationId, moderation.Id);

            }
        }

        [TestMethod]
        public async Task ThrowIfTextIsLessThen3Chars()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ThrowIfTextIsLessThen3Chars")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.ClientCommentService(context);
                var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.AddComment(1, "t", null, null));
                Assert.AreEqual(ex.Message, "Comment should be at least 3 characters long!");
            }

        }

        [TestMethod]
        public async Task ThrowIfTextIsNull()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ThrowIfTextIsNull")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.ClientCommentService(context);
                var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.AddComment(1, null, null, null));
                Assert.AreEqual(ex.Message, "Comment should be at least 3 characters long!");
            }

        }

    }
}

