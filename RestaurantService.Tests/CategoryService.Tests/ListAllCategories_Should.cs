﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryService.Tests
{
    [TestClass]
    public class ListAllCategories_Should
    {
        [TestMethod]
        public async Task ListAllCategories()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ListAllCategories")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var category1 = new Category { Name = "Category1" };
                var category2 = new Category { Name = "Category2" };
                context.Categories.AddRange(new List<Category> { category1, category2 });
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.CategoryService(context);
                var categories = await sut.ListAllCategories();

                Assert.IsTrue(categories.Count==2);
                Assert.IsTrue(categories.Any(c=>c.Name== "Category1"));
                Assert.IsTrue(categories.Any(c => c.Name == "Category2"));

            }
        }
    }
}


