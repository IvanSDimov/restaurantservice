﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryService.Tests
{
    [TestClass]
    public class FindCategoryById_Should
    {
        [TestMethod]
        public async Task FindCategoryByIdIfExists()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "FindCategoryByIdIfExists")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var category1 = new Category { Name = "Category1" };
                context.Categories.Add(category1);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var category = context.Categories.FirstOrDefault(c => c.Name == "Category1");
                var sut = new RestaurantService.Services.CategoryService(context);
                var foundCategory = await sut.FindCategoryById(category.Id);

                Assert.IsTrue(category == foundCategory);


            }
        }

        [TestMethod]
        public async Task ReturnNullIfCategoryDoesNotExist()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnNullIfCategoryDoesNotExist")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.CategoryService(context);
                var foundCategory = await sut.FindCategoryById(1);

                Assert.IsTrue(foundCategory==null);
            }
        }
    }
}