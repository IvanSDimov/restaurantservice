﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityService.Tests
{
    [TestClass]
    public class ListCities_Should
    {
        [TestMethod]
        public async Task ListAllCities()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ListAllCities")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var city1 = new City { Name = "City1" };
                var city2 = new City { Name = "City2" };
                context.Cities.AddRange(new List<City> { city1, city2 });
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.CityService(context);
                var cities = await sut.ListCities();

                Assert.IsTrue(cities.Count == 2);
                Assert.IsTrue(cities.Any(c => c.Name == "City1"));
                Assert.IsTrue(cities.Any(c => c.Name == "City2"));

            }
        }
    }
}



