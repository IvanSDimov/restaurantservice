﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AddressService.Tests
{
    [TestClass]
    public class CreateAddress_Should
    {
        [TestMethod]
        public async Task CreateAddressWithDesiredPropertiesIfExists()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "CreateAddressWithDesiredPropertiesIfExists")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var city = new City { Name = "City1" };
                context.Cities.Add(city);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.AddressService(context);
                var newAddress = await sut.CreateAddress("City1", "Street");
                newAddress = await context.Addresses.FirstOrDefaultAsync(a=>a.Street== "STREET");
                var city = await context.Cities.FirstOrDefaultAsync(c => c.Name == "City1");

                Assert.IsTrue(newAddress.CityId == city.Id);
                Assert.IsTrue(newAddress.Street == "STREET");

            }
        }
    }
}
