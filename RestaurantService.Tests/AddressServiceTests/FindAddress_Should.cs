﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AddressService.Tests
{
    [TestClass]
    public class FindAddress_Should
    {
        [TestMethod]
        public async Task FindAddressWithDesiredPropertiesIfExists()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "FindAddressWithDesiredPropertiesIfExists")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var city = new City { Name = "City1"};
                context.Cities.Add(city);
                context.SaveChanges();
                city = await context.Cities.FirstOrDefaultAsync(c => c.Name == "City1");
                var address = new Address { CityId = city.Id, Street = "Street" };
                context.Addresses.Add(address);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.AddressService(context);
                var foundAddress = await sut.FindAddress("City1", "Street");
                var city = await context.Cities.FirstOrDefaultAsync(c => c.Name == "City1");

                Assert.IsTrue(foundAddress.CityId == city.Id);
                Assert.IsTrue(foundAddress.Street == "Street");

            }
        }

        [TestMethod]
        public async Task ReturnNullIfNoAddressWithDesiredPropertiesIfExists()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnNullIfNoAddressWithDesiredPropertiesIfExists")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var sut = new RestaurantService.Services.AddressService(context);
                var foundLogbook = await sut.FindAddress("City1", "Street");

                Assert.IsTrue(foundLogbook == null);

            }
        }
    }
}

