﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.ManagerCommentService.Test
{
    [TestClass]
    public class EditComment_Should
    {
        [TestMethod]
        public async Task ThrowWhenNotExistingCommentIdIsPased1()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ThrowWhenNotExistingCommentIdIsPased1")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var comment = new ManagerComment() { Id = 1, Text = "CommentText" };
                arrangeContext.ManagerComments.Add(comment);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var newText = "NewText";
                var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.EditComment(newText, null));
                Assert.AreEqual(ex.Message, "Comment does not exists.");
            }
        }
        [TestMethod]
        public async Task ThrowWhenCommentToEditIsDeleted()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ThrowWhenCommentToEditIsDeleted")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var comment = new ManagerComment() { Id = 1, Text = "CommentText",IsDeleted = true };
                arrangeContext.ManagerComments.Add(comment);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var newText = "NewText";
                var comment = await context.ManagerComments.FindAsync(1);
                var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.EditComment(newText, comment));
                Assert.AreEqual(ex.Message, "Comment does not exists.");
            }
        }
        [TestMethod]
        public async Task ReturnsRightEditedCommentWhenValidParametursArePassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ReturnsRightEditedCommentWhenValidParametursArePassed")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var comment = new ManagerComment() { Id = 1, Text = "CommentText" };
                arrangeContext.ManagerComments.Add(comment);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var newText = "NewText";
                var comment = await context.ManagerComments.FindAsync(1);
                await sut.EditComment(newText, comment);
                var editedComment = await context.ManagerComments.FindAsync(1);
                Assert.AreEqual(editedComment, comment);
            }
        }

    }
}
