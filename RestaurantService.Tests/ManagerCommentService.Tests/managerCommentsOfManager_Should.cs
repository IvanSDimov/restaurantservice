﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.ManagerCommentService.Test
{
    [TestClass]
    public class managerCommentsOfManager_Should
    {
        [TestMethod]
        public async Task ThrowWhenNotValidManagerIdIsPased()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ThrowWhenNotValidManagerIdIsPased")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = new Logbook() { Id = 5, Name = "Logbook" };
                arrangeContext.Logbooks.Add(logbook);
                var comment = new ManagerComment() { Id = 1, Text = "CommentText" };
                arrangeContext.ManagerComments.Add(comment);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                string managerId = "Id";
                int logbookId = 5;
                var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.ManagerCommentsOfManager(managerId,logbookId,1,1));
                Assert.AreEqual(ex.Message, "No comments was found.");
            }
        }
            
        [TestMethod]
        public async Task ThrowWhenNotValidLogbookIdIsPased()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ThrowWhenNotValidLogbookIdIsPased")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = new Logbook() { Id = 5, Name = "Logbook" };
                arrangeContext.Logbooks.Add(logbook);
                var comment = new ManagerComment() { Id = 1, Text = "CommentText" };
                arrangeContext.ManagerComments.Add(comment);
                var manager = new ApplicationUser() { UserName = "Manager" };
                arrangeContext.Users.Add(manager);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var manager = await context.Users.FirstOrDefaultAsync(c=>c.UserName == "Manager");
                var managerId = manager.Id;
                int logbookId = 1;
                var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.ManagerCommentsOfManager(managerId, logbookId, 1, 1));
                Assert.AreEqual(ex.Message, "No comments was found.");
            }
        }
        [TestMethod]
        public async Task ReturnsRightCommentsWhenValidInputIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnsRightCommentsWhenValidInputIsPassed")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = new Logbook() { Id = 5, Name = "Logbook" };
                arrangeContext.Logbooks.Add(logbook);

                var manager = new ApplicationUser() { UserName = "Manager" };
                arrangeContext.Users.Add(manager);

                var comment1 = new ManagerComment() { Id = 1, Text = "CommentText1" };
                comment1.AuthorId = manager.Id;
                comment1.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment1);
                
                var comment2 = new ManagerComment() { Id = 2, Text = "CommentText2" };
                comment2.AuthorId = manager.Id;
                comment2.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment2);
              
                var comment3 = new ManagerComment() { Id = 3, Text = "CommentText3" };
                comment3.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment3);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var manager = await context.Users.FirstOrDefaultAsync(c => c.UserName == "Manager");
                var managerId = manager.Id;
                var realComments = context.ManagerComments.Where(c => c.AuthorId == managerId).ToList();
                int logbookId = 5;
                var comments = await sut.ManagerCommentsOfManager(managerId, logbookId, 0, 4);
                Assert.AreEqual(comments.Item2, 2);
               CollectionAssert.AreEqual(comments.Item1, realComments);
            }
        }

    }
}
