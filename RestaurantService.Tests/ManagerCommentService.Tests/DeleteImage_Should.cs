﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.ManagerCommentService.Test
{ 
    [TestClass]
    public class DeleteImage_Should
    {
        [TestMethod]
        public async Task DeleteImageWhenRightValuesArePassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "DeleteImageWhenRightValuesArePassed")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = new Logbook() { Id = 5, Name = "Logbook" };
                arrangeContext.Logbooks.Add(logbook);
                var manager = new ApplicationUser() { UserName = "Manager" };
                arrangeContext.Users.Add(manager);
                var comment1 = new ManagerComment() { Id = 1, Text = "CommentText1",ByteImage= new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 } };
                comment1.AuthorId = manager.Id;
                comment1.LogbookId = logbook.Id;
                
                arrangeContext.Add(comment1);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var comment =await context.ManagerComments.FindAsync(1);
                var testImage = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };
                CollectionAssert.AreEqual(comment.ByteImage, testImage);
                var newComment = await sut.DeleteImage(1);
                Assert.AreEqual(newComment.ByteImage,null);
            }
        }
    }
}
