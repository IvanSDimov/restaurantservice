﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using RestaurantService.Services;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.ManagerCommentService.Test
{
    [TestClass]
    public class AddComment_Should
    {
        [TestMethod]
        public async Task ThrowWhenTextPropertyIsNullOrEmpty()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ThrowWhenTextPropertyIsNullOrEmpty")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                
            }
            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(assertContext);
                var manager = new ApplicationUser() { UserName = "Ivan" };
                string text = "";
                var logbook = new Logbook() { Name = "Logbook1" };
                byte[] image = null;               
                var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.AddComment(text, manager, logbook,image));
                Assert.AreEqual(ex.Message, "Comment must not be empty!");
            }
        }
        [TestMethod]
        public async Task AddCommentToDatabeseWithoutImageReturnsRightComment()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "AddCommentToDatabeseWithoutImageReturnsRightComment")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Users.Add(new ApplicationUser() { UserName = "Ivan" });
                arrangeContext.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var manager = await context.Users.FirstOrDefaultAsync(m => m.UserName == "Ivan");
                string text = "CommentText";
                var logbook = new Logbook() { Name = "Logbook1" };
                byte[] image = null;
                var testComment = await sut.AddComment(text, manager, logbook, image);
               
                Assert.AreEqual(testComment.Text, "CommentText");
                Assert.AreEqual(testComment.AuthorId,manager.Id);
            }
        }
        [TestMethod]
        public async Task AddCommentToDatabeseWithoutImage()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "AddCommentToDatabese")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var manager = new ApplicationUser() { UserName = "Ivan" };
                string text = "CommentText";
                var logbook = new Logbook() { Name = "Logbook1" };
                byte[] image = null;
                var testComment = await sut.AddComment(text, manager, logbook, image);
                var comment = await context.ManagerComments.FirstOrDefaultAsync(c => c.Text == testComment.Text);
                Assert.AreEqual(testComment, comment);
            }
        }
    }
}
