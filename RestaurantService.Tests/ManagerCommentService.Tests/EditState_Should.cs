﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.ManagerCommentService.Test
{
    [TestClass]
    public class EditState_Should
    {
        [TestMethod]
        public async Task ThrowWhenUnValidCmmentIdIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ThrowWhenUnValidCmmentIdIsPassed")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var comment = new ManagerComment() { Id = 1, Text = "CommentText",IsDone = false };
                arrangeContext.ManagerComments.Add(comment);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                int testCommentId = 5;
                var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.EditState(testCommentId,true));
                Assert.AreEqual(ex.Message, "Comment does not exsist.");
            }
        }
        [TestMethod]
        public async Task ChangesCommentIsDoneStateWhenValidCmmentIdIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ChangesCommentIsDoneStateWhenValidCmmentIdIsPassed")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var comment = new ManagerComment() { Id = 1, Text = "CommentText", IsDone = false };
                arrangeContext.ManagerComments.Add(comment);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var testComment = context.ManagerComments.FirstOrDefault(mc => mc.Id == 1);
                await sut.EditState(testComment.Id, true);
                Assert.AreEqual(testComment.IsDone, true);
            }
        }
        [TestMethod]
        public async Task NotChangeCommentIsDoneStateWhenCommentIsInThePassedState()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "NotChangeCommentIsDoneStateWhenCommentIsInThePassedState")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var comment = new ManagerComment() { Id = 1, Text = "CommentText", IsDone = true };
                arrangeContext.ManagerComments.Add(comment);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var testComment = context.ManagerComments.FirstOrDefault(mc => mc.Id == 1);
                await sut.EditState(testComment.Id, true);
                Assert.AreEqual(testComment.IsDone, true);
            }
        }
    }
}
