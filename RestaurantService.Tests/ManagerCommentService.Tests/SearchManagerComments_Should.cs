﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.ManagerCommentService.Test
{
    [TestClass]
    public class SerchManagerComments_Should
    {
        [TestMethod]
        public async Task ReturnAllCommentsWhenNoSerchValuesArePassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnAllCommentsWhenNoSerchValuesArePassed")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = new Logbook() { Id = 5, Name = "Logbook" };
                arrangeContext.Logbooks.Add(logbook);

                var manager = new ApplicationUser() { UserName = "Manager" };
                arrangeContext.Users.Add(manager);

                //var category = new Category { Id = 1, Name = "Category1" };
                //arrangeContext.Add(category);

                var comment1 = new ManagerComment() { Id = 1, Text = "CommentText1" };
                comment1.AuthorId = manager.Id;
                comment1.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment1);

                var comment2 = new ManagerComment() { Id = 2, Text = "CommentText2" };
                comment2.AuthorId = manager.Id;
                comment2.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment2);

                var comment3 = new ManagerComment() { Id = 3, Text = "CommentText3" };
                comment3.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment3);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var realComments = await context.ManagerComments.ToListAsync();
                int logbookId = 5;
                var tupleComments = await sut.SearchManagerComments(5,0,logbookId, DateTime.MinValue, DateTime.MinValue, null,null);
                var comments = tupleComments.Item1.ToList();
                Assert.AreEqual(tupleComments.Item2, 3);
                CollectionAssert.AreEqual(comments, realComments);
            }
        }
        [TestMethod]
        public async Task ReturnRightCommentsWhenOnlyDateValuesArePassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnRightCommentsWhenOnlyDateValuesArePassed")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = new Logbook() { Id = 5, Name = "Logbook" };
                arrangeContext.Logbooks.Add(logbook);

                var manager = new ApplicationUser() { UserName = "Manager" };
                arrangeContext.Users.Add(manager);

                //var category = new Category { Id = 1, Name = "Category1" };
                //arrangeContext.Add(category);

                var comment1 = new ManagerComment() { Id = 1, Text = "CommentText1", DateCreated = new DateTime(2019,6,12) };
                comment1.AuthorId = manager.Id;
                comment1.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment1);

                var comment2 = new ManagerComment() { Id = 2, Text = "CommentText2", DateCreated = new DateTime(2019, 6, 11) };
                comment2.AuthorId = manager.Id;
                comment2.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment2);

                var comment3 = new ManagerComment() { Id = 3, Text = "CommentText3" , DateCreated = new DateTime(2019, 3, 12) };
                comment3.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment3);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var realComments = await context.ManagerComments.Where(mc =>mc.Id < 3).ToListAsync();
                int logbookId = 5;
                var tupleComments = await sut.SearchManagerComments(5, 0, logbookId, new DateTime(2019, 6, 01), new DateTime(2019, 6, 20), null, null);
                var comments = tupleComments.Item1.ToList();
                Assert.AreEqual(tupleComments.Item2, 2);
                CollectionAssert.AreEqual(comments, realComments);
            }
        }
        [TestMethod]
        public async Task ReturnRightCommentsWhenOnlyManagerIdsValueIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnRightCommentsWhenOnlyManagerIdsValueIsPassed")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = new Logbook() { Id = 5, Name = "Logbook" };
                arrangeContext.Logbooks.Add(logbook);

                var manager = new ApplicationUser() { UserName = "Manager" };
                arrangeContext.Users.Add(manager);

                //var category = new Category { Id = 1, Name = "Category1" };
                //arrangeContext.Add(category);

                var comment1 = new ManagerComment() { Id = 1, Text = "CommentText1", DateCreated = new DateTime(2019, 6, 12) };
                comment1.AuthorId = manager.Id;
                comment1.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment1);

                var comment2 = new ManagerComment() { Id = 2, Text = "CommentText2", DateCreated = new DateTime(2019, 6, 11) };
                comment2.AuthorId = manager.Id;
                comment2.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment2);

                var comment3 = new ManagerComment() { Id = 3, Text = "CommentText3", DateCreated = new DateTime(2019, 3, 12) };
                comment3.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment3);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var realComments = await context.ManagerComments.Where(mc => mc.Id < 3).ToListAsync();
                int logbookId = 5;
                var manager = await context.Users.FirstOrDefaultAsync(c => c.UserName == "Manager");

                var managerIds = new List<string>() { manager.Id };
                var tupleComments = await sut.SearchManagerComments(5, 0, logbookId, DateTime.MinValue, DateTime.MinValue, managerIds, null);
                var comments = tupleComments.Item1.ToList();
                Assert.AreEqual(tupleComments.Item2, 2);
                CollectionAssert.AreEqual(comments, realComments);
            }
        }
        [TestMethod]
        public async Task ReturnRightCommentsWhenOnlyCategoryIdsValueIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnRightCommentsWhenOnlyCategoryIdsValueIsPassed")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = new Logbook() { Id = 5, Name = "Logbook" };
                arrangeContext.Logbooks.Add(logbook);

                var manager = new ApplicationUser() { UserName = "Manager" };
                arrangeContext.Users.Add(manager);

                var category = new Category { Id = 1, Name = "Category1" };
                arrangeContext.Add(category);

                var comment1 = new ManagerComment() { Id = 1, Text = "CommentText1", DateCreated = new DateTime(2019, 6, 12) };
                comment1.AuthorId = manager.Id;
                comment1.LogbookId = logbook.Id;                
                arrangeContext.ManagerComments.Add(comment1);

                var comment2 = new ManagerComment() { Id = 2, Text = "CommentText2", DateCreated = new DateTime(2019, 6, 11) };
                comment2.AuthorId = manager.Id;
                comment2.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment2);

                var comment3 = new ManagerComment() { Id = 3, Text = "CommentText3", DateCreated = new DateTime(2019, 3, 12) };
                comment3.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment3);

                arrangeContext.CategoryManagerComment.Add(new CategoryManagerComment { CategoryId = 1, ManagerCommentId = 1 });
                arrangeContext.CategoryManagerComment.Add(new CategoryManagerComment { CategoryId = 1, ManagerCommentId = 2 });

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var realComments = await context.ManagerComments.Where(mc => mc.Id < 3).ToListAsync();
                int logbookId = 5;
                var categoryIds = new List<int>() { 1 };
                var tupleComments = await sut.SearchManagerComments(5, 0, logbookId, DateTime.MinValue, DateTime.MinValue, null, categoryIds);
                var comments = tupleComments.Item1.ToList();
                Assert.AreEqual(tupleComments.Item2, 2);
                CollectionAssert.AreEqual(comments, realComments);
            }
        }
        [TestMethod]
        public async Task ReturnRightCommentsWhenAllValuesArePassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnRightCommentsWhenAllValuesArePassed")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = new Logbook() { Id = 5, Name = "Logbook" };
                arrangeContext.Logbooks.Add(logbook);

                var manager = new ApplicationUser() { UserName = "Manager" };
                arrangeContext.Users.Add(manager);

                var category = new Category { Id = 1, Name = "Category1" };
                arrangeContext.Add(category);

                var comment1 = new ManagerComment() { Id = 1, Text = "CommentText1", DateCreated = new DateTime(2019, 6, 12) };
                comment1.AuthorId = manager.Id;
                comment1.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment1);

                var comment2 = new ManagerComment() { Id = 2, Text = "CommentText2", DateCreated = new DateTime(2019, 6, 11) };
                comment2.AuthorId = manager.Id;
                comment2.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment2);

                var comment3 = new ManagerComment() { Id = 3, Text = "CommentText3", DateCreated = new DateTime(2019, 3, 12) };
                comment3.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment3);

                arrangeContext.CategoryManagerComment.Add(new CategoryManagerComment { CategoryId = 1, ManagerCommentId = 1 });
                arrangeContext.CategoryManagerComment.Add(new CategoryManagerComment { CategoryId = 1, ManagerCommentId = 2 });

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var realComments = await context.ManagerComments.Where(mc => mc.Id < 3).ToListAsync();
                int logbookId = 5;
                var categoryIds = new List<int>() { 1 };
                var manager = await context.Users.FirstOrDefaultAsync(c => c.UserName == "Manager");
                var managerIds = new List<string>() { manager.Id }; 

                var tupleComments = await sut.SearchManagerComments(5, 0, logbookId, new DateTime(2019, 6, 01), new DateTime(2019, 6, 20), managerIds, categoryIds);
                var comments = tupleComments.Item1.ToList();
                Assert.AreEqual(tupleComments.Item2, 2);
                CollectionAssert.AreEqual(comments, realComments);
            }
        }
    }
}
