﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.ManagerCommentService.Test
{
    [TestClass]
    public class ManagerCommentsByCategory_Should
    {
        [TestMethod]
        public async Task ThrowsWhenNotValidCategoryIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ThrowsWhenNotValidCategoryIsPassed")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = new Logbook() { Id = 5, Name = "Logbook" };
                arrangeContext.Logbooks.Add(logbook);

                var comment = new ManagerComment() { Id = 1, Text = "CommentText" };
                arrangeContext.ManagerComments.Add(comment);

                var category = new Category() { Id = 1, Name = "Category1" };
                arrangeContext.Categories.Add(category);
                
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var categoryId = 5;
                int logbookId = 5;
                var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.ManagerCommentsByCategory(categoryId, logbookId, 0, 4));
                Assert.AreEqual(ex.Message, "No comments was found.");
            }
        }
        [TestMethod]
        public async Task ThrowsWhenNotValidLogbookIdIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ThrowsWhenNotValidLogbookIdIsPassed")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = new Logbook() { Id = 5, Name = "Logbook" };
                arrangeContext.Logbooks.Add(logbook);

                var comment = new ManagerComment() { Id = 1, Text = "CommentText" };
                arrangeContext.ManagerComments.Add(comment);

                var category = new Category() { Id = 1, Name = "Category1" };
                arrangeContext.Categories.Add(category);

                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var categoryId = 1;
                int logbookId = 1;
                var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.ManagerCommentsByCategory(categoryId, logbookId, 0, 4));
                Assert.AreEqual(ex.Message, "No comments was found.");
            }
        }
        [TestMethod]
        public async Task ReturnsRightCommentsWhenValidParametursArePassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnsRightCommentsWhenValidParametursArePassed")
                .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var logbook = new Logbook() { Id = 5, Name = "Logbook" };
                arrangeContext.Logbooks.Add(logbook);

                var category = new Category { Id = 1, Name = "Category1" };
                arrangeContext.Add(category);

                var comment1 = new ManagerComment() { Id = 1, Text = "CommentText1" };
                comment1.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment1);
                
                var comment2 = new ManagerComment() { Id = 2, Text = "CommentText2" };                
                comment2.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment2);

                var comment3 = new ManagerComment() { Id = 3, Text = "CommentText3" };
                comment3.LogbookId = logbook.Id;
                arrangeContext.ManagerComments.Add(comment3);

                arrangeContext.CategoryManagerComment.Add(new CategoryManagerComment { CategoryId = 1, ManagerCommentId = 1 });
                arrangeContext.CategoryManagerComment.Add(new CategoryManagerComment { CategoryId = 1, ManagerCommentId = 2 });
                
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);               
                var realComments = await context.ManagerComments.Where(mc => mc.Id < 3).ToListAsync();
                int logbookId = 5;
                var comments = await sut.ManagerCommentsByCategory(1, logbookId, 0, 4);
                CollectionAssert.AreEqual(comments.Item1, realComments);
            }
        }
    }
}
