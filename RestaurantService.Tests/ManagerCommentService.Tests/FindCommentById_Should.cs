﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Tests.ManagerCommentService.Test
{
    [TestClass]
    public class FindCommentById_Should
    {
        [TestMethod]
        public async Task ThrowWhenNotExistingCommentIdIsPased()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ThrowWhenNotExistingCommentIdIsPased")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var comment = new ManagerComment() { Id = 1, Text = "CommentText" };
                arrangeContext.ManagerComments.Add(comment);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.FindCommentById(0));
                Assert.AreEqual(ex.Message, "No comment whith ID 0 exists.");
            }
        }
        [TestMethod]
        public async Task ThrowWhenCommentIsDeleted()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ThrowWhenCommentIsDeleted")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var comment = new ManagerComment() {Id = 1, Text = "CommentText",IsDeleted=true };
                arrangeContext.ManagerComments.Add(comment);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);                
                var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.FindCommentById(1));
                Assert.AreEqual(ex.Message, "No comment whith ID 1 exists.");
            }
        }
        [TestMethod]
        public async Task ReturnsRightCommentWhenValidParametursArePassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ReturnsRightCommentWhenValidParametursArePassed")
               .Options;
            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var comment = new ManagerComment() { Id = 1, Text = "CommentText"};
                arrangeContext.ManagerComments.Add(comment);
                await arrangeContext.SaveChangesAsync();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new Services.ManagerCommentService(context);
                var comment = await sut.FindCommentById(1);
                Assert.AreEqual(comment.Text, "CommentText");
            }
        }        
    }
}
