﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class CreateLogbookUsers_Should
    {
        [TestMethod]
        public async Task CreateLogbookWithGivenNameAndEstablishmentId()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "CreateLogbookWithGivenNameAndEstablishmentId")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var logbook = await sut.CreateLogbookUsers("TestLogbook",1,null);
            }
            using (var context = new ApplicationDbContext(options))
            {
                var foundlogbook = await context.Logbooks.FirstOrDefaultAsync(l => l.Name == "TestLogbook");
                Assert.IsTrue(foundlogbook != null);
                Assert.IsTrue(foundlogbook.EstablishmentId == 1);
            }
        }

        [TestMethod]
        public async Task AssignThePassedManagersToTheNewLogbook()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "AssignThePassedManagersToTheNewLogbook")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var logbook = await sut.CreateLogbookUsers("TestLogbook", 1, new List<string> { "n1","n2"});
            }
            using (var context = new ApplicationDbContext(options))
            {
                var foundlogbook = await context.Logbooks.FirstOrDefaultAsync(l => l.Name == "TestLogbook");
                var assignedManagers = await context.ManagersLogbooks
                            .Where(ml => ml.LogbookId == foundlogbook.Id)
                            .Select(ml => ml.ManagerId)
                            .ToListAsync();
                Assert.IsTrue(assignedManagers.Count==2);
                Assert.IsTrue(assignedManagers.Contains("n1"));
                Assert.IsTrue(assignedManagers.Contains("n2"));
            }
        }

    }
}


