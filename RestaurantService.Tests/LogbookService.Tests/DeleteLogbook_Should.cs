﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class DeleteLogbooks_Should
    {
        [TestMethod]
        public async Task SetIsDeletedToTrue()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "SetIsDeletedToTrue")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1", EstablishmentId = 1};
                context.Logbooks.Add(logbook1);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var logbook = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                await sut.DeleteLogbook(logbook.Id);
                var logbookToTest = context.Logbooks.Find(logbook.Id);

                Assert.IsTrue(logbookToTest.IsDeleted == true);

            }
        }
    }
}


