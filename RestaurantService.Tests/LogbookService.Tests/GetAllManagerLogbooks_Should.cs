﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System.Linq;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class GetAllManagerLogbooks_Should
    {
        [TestMethod]
        public async Task GetAllLogbooksOfAManager()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "GetAllLogbooksOfAManager")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                context.Logbooks.Add(new Logbook { Name = "logbook1" });
                context.Logbooks.Add(new Logbook { Name = "logbook2" });
                context.Logbooks.Add(new Logbook { Name = "logbook3" });
                context.SaveChanges();
                var managerLogbooks = context.Logbooks
                    .Where(l=>l.Name!= "logbook3")
                    .Select(l => l.Id)
                    .Select(lid => new ManagersLogbooks { ManagerId = "m", LogbookId = lid });
                context.ManagersLogbooks.AddRange(managerLogbooks);
                context.ManagersLogbooks.Add(new ManagersLogbooks {
                    ManagerId = "n",
                    LogbookId = context.Logbooks.FirstOrDefault(l=>l.Name=="logbook3").Id });
                context.SaveChanges();
                var mls = context.ManagersLogbooks.ToList();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbooks = await sut.GetAllManagerLogbooks("m");

                Assert.IsTrue(foundLogbooks.Count == 2);
                Assert.IsTrue(foundLogbooks.Any(l=>l.Name == "logbook1"));
                Assert.IsTrue(foundLogbooks.Any(l => l.Name == "logbook2"));
                Assert.IsTrue(!foundLogbooks.Any(l => l.Name == "logbook3"));
            }
        }


        [TestMethod]
        public async Task NotReturnALogbookIfItIsDeleted()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "NotReturnALogbookIfItIsDeleted_")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                context.Logbooks.Add(new Logbook { Name = "logbook1" });
                context.Logbooks.Add(new Logbook { Name = "logbook2", IsDeleted=true });
                context.SaveChanges();
                var managerLogbooks = context.Logbooks.Select(l => l.Id)
                    .Select(lid => new ManagersLogbooks { ManagerId = "m", LogbookId = lid });
                context.ManagersLogbooks.AddRange(managerLogbooks);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var logbook = await context.Logbooks.FirstOrDefaultAsync(l => l.Name == "logbook1");
                var id = logbook.Id;
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbooks = await sut.GetAllManagerLogbooks("m");

                Assert.IsTrue(foundLogbooks.Count == 1);
                Assert.IsTrue(foundLogbooks.Any(l => l.Name == "logbook1"));
                Assert.IsTrue(!foundLogbooks.Any(l => l.Name == "logbook2"));

            }
        }
    }
}


