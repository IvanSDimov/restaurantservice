﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class FindLogbookByNameAndEstablishment_Should
    {
        [TestMethod]
        public async Task FindLogbookWithDesiredPropertiesIfExists()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "FindLogbookWithDesiredPropertiesIfExists")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var logbook = new Logbook { Name = "logbook1", EstablishmentId=1 };
                context.Logbooks.Add(logbook);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.FindLogbookByNameAndEstablishment("logbook1",1);

                Assert.IsTrue(foundLogbook.Name == "logbook1");
                Assert.IsTrue(foundLogbook.EstablishmentId == 1);

            }
        }

        [TestMethod]
        public async Task ReturnNullIfWithDesiredPropertiesIfExists()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnNullIfWithDesiredPropertiesIfExists")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.FindLogbookByNameAndEstablishment("logbook1", 1);

                Assert.IsTrue(foundLogbook == null);

            }
        }
    }
}
