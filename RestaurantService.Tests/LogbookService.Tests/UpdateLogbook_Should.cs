﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class UpdateLogbook_Should
    {
        [TestMethod]
        public async Task FindLogbookByNameAndEstablishmentIfExists()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "FindLogbookByNameAndEstablishmentIfExists")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1", EstablishmentId = 1 };
                context.Logbooks.Add(logbook1);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.FindLogbookByNameAndEstablishment("logbook1",1);

                Assert.IsTrue(foundLogbook.Name== "logbook1");
                Assert.IsTrue(foundLogbook.EstablishmentId==1);
            }
        }

        [TestMethod]
        public async Task ReturnNullIfLogbookWithPointedNameAndEstIdDoesNotExist()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnNullIfLogbookWithPointedNameAndEstIdDoesNotExist")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.UpdateLogbook("logbook1", 1, null);


                Assert.IsTrue(foundLogbook== null);

            }
        }

        [TestMethod]
        public async Task UndeleteTheFoundLogbookIfItIsDeleted()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "UndeleteTheFoundLogbookIfItIsDeleted")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1", EstablishmentId = 1, IsDeleted = true };
                context.Logbooks.Add(logbook1);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.UpdateLogbook("logbook1", 1, null);

                Assert.IsTrue(foundLogbook.IsDeleted == false);
            }
        }

        [TestMethod]
        public async Task AssignManagersToLogbookIfThereAreNoManagersAssigned()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "AssignManagersToLogbookIfThereAreNoManagersAssigned")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1", EstablishmentId = 1};
                context.Logbooks.Add(logbook1);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.UpdateLogbook("logbook1", 1, new List<string> {"m1","m2","m3" });

                Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId=="m1"));
                Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "m2"));
                Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "m3"));
            }
        }

        [TestMethod]
        public async Task AssignNewManagersToLogbookIfThereAreManagersAssigned()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "AssignNewManagersToLogbookIfThereAreManagersAssigned")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1", EstablishmentId = 1, IsDeleted = true };
                
                context.Logbooks.Add(logbook1);
                context.SaveChanges();
                logbook1 = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");
                var manager_logbooks = new List<string> { "n1", "n2", "n3" }
                        .Select(m => new ManagersLogbooks { ManagerId = m, LogbookId = logbook1.Id }) ;
                context.ManagersLogbooks.AddRange(manager_logbooks);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.UpdateLogbook("logbook1", 1, new List<string> { "m1", "m2", "m3" });

                Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "m1"));
                Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "m2"));
                Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "m3"));
            }
        }

        [TestMethod]
        public async Task UnassignOldManagersFromNotDeletedLogbookIfNewManagersAreAssigned()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "UnassignOldManagersFromNotDeletedLogbookIfNewManagersAreAssigned")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1", EstablishmentId = 1, IsDeleted = true };

                context.Logbooks.Add(logbook1);
                context.SaveChanges();
                logbook1 = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");
                var manager_logbooks = new List<string> { "n1", "n2", "n3" }
                        .Select(m => new ManagersLogbooks { ManagerId = m, LogbookId = logbook1.Id });
                context.ManagersLogbooks.AddRange(manager_logbooks);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.UpdateLogbook("logbook1", 1, new List<string> { "m1", "m2", "m3" });

                Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "m1"));
                Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "m2"));
                Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "m3"));
                Assert.IsTrue(!context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "n1"));
                Assert.IsTrue(!context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "n2"));
                Assert.IsTrue(!context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "n3"));
            }
        }

        [TestMethod]
        public async Task NotUnassignOldManagersFromNotDeletedLogbookIfNewManagersAreNotAssigned()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "NotUnassignOldManagersFromNotDeletedLogbookIfNewManagersAreNotAssigned")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1", EstablishmentId = 1};

                context.Logbooks.Add(logbook1);
                context.SaveChanges();
                logbook1 = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");
                var manager_logbooks = new List<string> { "n1", "n2", "n3" }
                        .Select(m => new ManagersLogbooks { ManagerId = m, LogbookId = logbook1.Id });
                context.ManagersLogbooks.AddRange(manager_logbooks);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.UpdateLogbook("logbook1", 1, null);

                Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "n1"));
                Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "n2"));
                Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "n3"));
            }
        }

        [TestMethod]
        public async Task UnassignOldManagersFromDeletedLogbook()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "UnassignOldManagersFromDeletedLogbook")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1", EstablishmentId = 1, IsDeleted = true };

                context.Logbooks.Add(logbook1);
                context.SaveChanges();
                logbook1 = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");
                var manager_logbooks = new List<string> { "n1", "n2", "n3" }
                        .Select(m => new ManagersLogbooks { ManagerId = m, LogbookId = logbook1.Id });
                context.ManagersLogbooks.AddRange(manager_logbooks);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.UpdateLogbook("logbook1", 1, null);

                Assert.IsTrue(!context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "n1"));
                Assert.IsTrue(!context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "n2"));
                Assert.IsTrue(!context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == "n3"));
            }
        }

        [TestMethod]
        public async Task ChangeNameIfNewNonExistingNameIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ChangeNameIfNewNonExistingNameIsPassed")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1", EstablishmentId = 1};

                context.Logbooks.Add(logbook1);
                context.SaveChanges();
                
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var logbook = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");
                var foundLogbook = await sut.UpdateLogbook(logbook.Id, "logbook2", null, null);
                var logbookToTest= context.Logbooks.Find(logbook.Id);

                Assert.IsTrue(logbookToTest.Name=="logbook2");
            }
        }

        [TestMethod]
        public async Task NotChangeNameIfNewExistingNameIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "NotChangeNameIfNewExistingNameIsPassed")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1", EstablishmentId = 1};
                var logbook2 = new Logbook { Name = "logbook2", EstablishmentId = 1 };

                context.Logbooks.Add(logbook1);
                context.Logbooks.Add(logbook2);
                context.SaveChanges();

            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var logbook = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");
                var foundLogbook = await sut.UpdateLogbook(logbook.Id, "logbook2", null, null);
                var logbookToTest = context.Logbooks.Find(logbook.Id);

                Assert.IsTrue(logbookToTest.Name == "logbook1");
            }
        }

        [TestMethod]
        public async Task AddNewManagersIfLogbooksIdIsPassed()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "AddNewManagersIfLogbooksIdIsPassed")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1", EstablishmentId = 1};

                context.Logbooks.Add(logbook1);
                context.SaveChanges();
                logbook1 = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");
                var managers = (new List<string> { "n1", "n2", "m1", "m2" })
                            .Select(n => new ApplicationUser { UserName = n });
                context.Users.AddRange(managers);
                context.SaveChanges();
                var manager_logbooks = context.Users
                            .Where(u => u.UserName == "n1" || u.UserName == "n2")
                            .Select(u => new ManagersLogbooks { ManagerId = u.Id, LogbookId = logbook1.Id });
                context.ManagersLogbooks.AddRange(manager_logbooks);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var logbook = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");
                var existing = context.Users
                            .Where(u => u.UserName == "n1" || u.UserName == "n2")
                            .Select(u=>u.Id)
                            .ToList();
                var toAdd = context.Users
                            .Where(u => u.UserName == "m1" || u.UserName == "m2")
                            .Select(u => u.Id)
                            .ToList();
                var foundLogbook=await sut.UpdateLogbook(logbook.Id, "logbook1", toAdd,existing);

                foreach (var item in toAdd)
                {
                    Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == item));
                }
                foreach (var item in existing)
                {
                    Assert.IsTrue(context.ManagersLogbooks.Any(ml => ml.LogbookId == foundLogbook.Id && ml.ManagerId == item));
                }

            }
        }
    }
}


