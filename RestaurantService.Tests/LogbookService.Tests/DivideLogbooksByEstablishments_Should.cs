﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class DivideLogbooksByEstablishments_Should
    {


        [TestMethod]
        public async Task ReturnEstablismentNameAndIdAsKeyAndPropperListOfLogbooksAsValue()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnEstablismentNameAndIdAsKeyAndPropperListOfLogbooksAsValue")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                context.Establishments.Add(new Establishment { Name = "Est1" });
                context.Establishments.Add(new Establishment { Name = "Est2" });
                context.SaveChanges();
                var e1Id = context.Establishments.FirstOrDefault(e => e.Name == "Est1").Id;
                var e2Id = context.Establishments.FirstOrDefault(e => e.Name == "Est2").Id;
                context.Logbooks.Add(new Logbook { Name = "logbook1",EstablishmentId=e1Id });
                context.Logbooks.Add(new Logbook { Name = "logbook2", EstablishmentId = e1Id });
                context.Logbooks.Add(new Logbook { Name = "logbook3", EstablishmentId = e2Id });
                context.Logbooks.Add(new Logbook { Name = "logbook4", EstablishmentId = e2Id });
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var logbooks = context.Logbooks.ToList();
                var e1Id = context.Establishments.FirstOrDefault(e => e.Name == "Est1").Id;
                var e2Id = context.Establishments.FirstOrDefault(e => e.Name == "Est2").Id;

                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);

                var dictionary = await sut.DivideLogbooksByEstablishments(logbooks);
                Assert.IsTrue(dictionary.Count == 2);
                Assert.IsTrue(dictionary.Any(d=>d.Key.Item1== "Est1"
                                        && d.Key.Item2 == e1Id
                                        && d.Value.Count==2
                                        && d.Value.Any(l=>l.Name== "logbook1")
                                        && d.Value.Any(l => l.Name == "logbook2")));
                Assert.IsTrue(dictionary.Any(d => d.Key.Item1 == "Est2"
                                        && d.Key.Item2 == e2Id
                                        && d.Value.Count == 2
                                        && d.Value.Any(l => l.Name == "logbook3")
                                        && d.Value.Any(l => l.Name == "logbook4")));

            }
        }
    }
}






