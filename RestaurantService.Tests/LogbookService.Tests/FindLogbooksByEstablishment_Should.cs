﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class FindLogbooksByEstablishment_Should
    {
        [TestMethod]
        public async Task FindAllLogbooksOfTheEstablishment()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "FindAllLogbooksOfTheEstablishment")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1", EstablishmentId = 1 };
                var logbook2 = new Logbook { Name = "logbook2", EstablishmentId = 1 };
                context.Logbooks.AddRange(new List<Logbook> {logbook1,logbook2});
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbooks = await sut.FindLogbooksByEstablishment(1);

                Assert.IsTrue(foundLogbooks.Count == 2);
                Assert.IsTrue(foundLogbooks.Any(l=>l.Name=="logbook1"));
                Assert.IsTrue(foundLogbooks.Any(l => l.Name == "logbook2"));
            }
        }

        [TestMethod]
        public async Task ReturnEmptyListIfThereAreNoLogbooksInTheEstablishment()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnEmptyListIfThereAreNoLogbooksInTheEstablishment")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.FindLogbooksByEstablishment(1);

                Assert.IsTrue(foundLogbook.Count==0);

            }
        }

        [TestMethod]
        public async Task NotReturnALogbookIfItIsDeleted()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "NotReturnALogbookIfItIsDeleted")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1", EstablishmentId = 1 };
                var logbook2 = new Logbook { Name = "logbook2", EstablishmentId = 1, IsDeleted=true };
                context.Logbooks.AddRange(new List<Logbook> { logbook1, logbook2 });
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbooks = await sut.FindLogbooksByEstablishment(1);

                Assert.IsTrue(foundLogbooks.Count == 1);
                Assert.IsTrue(foundLogbooks.Any(l => l.Name == "logbook1"));
                Assert.IsTrue(!foundLogbooks.Any(l => l.Name == "logbook2"));
            }
        }
    }
}

