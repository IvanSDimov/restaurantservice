using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class FindLogbookById_Should
    {
        [TestMethod]
        public async Task FindLogbookIfExists()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "FindLogbookIfExists")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var logbook = new Logbook { Name = "logbook1" };
                context.Logbooks.Add(logbook);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var logbook =await context.Logbooks.FirstOrDefaultAsync(l=>l.Name=="logbook1");
                var id = logbook.Id;
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook =await sut.FindLogbookById(id);

                Assert.IsTrue(foundLogbook == logbook);

            }
        }


        [TestMethod]
        public async Task ReturnNullIfLogbookDoesNotExist()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnNullIfLogbookDoesNotExist")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.FindLogbookById(1);

                Assert.IsTrue(foundLogbook == null);

            }
        }
    }
}
