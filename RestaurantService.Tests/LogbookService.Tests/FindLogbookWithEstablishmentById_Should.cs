﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class FindLogbookWithEstablishmentById_Should
    {
        [TestMethod]
        public async Task FindLogbookWithAddressIfExists()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "FindLogbookWithAddressIfExists")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                context.Cities.Add(new City { Name = "TestCity" });
                context.SaveChanges();
                var city = await context.Cities.FirstOrDefaultAsync(c => c.Name == "TestCity");
                context.Addresses.Add(new Address { CityId = city.Id, Street = "TestStreet" });
                context.SaveChanges();
                var address = await context.Addresses.FirstOrDefaultAsync(a => a.Street == "TestStreet");
                context.Establishments.Add(new Establishment { Name="TestEstablishment", AddressId=address.Id });
                context.SaveChanges();
                var establishment=await context.Establishments.FirstOrDefaultAsync(e => e.Name == "TestEstablishment");
                var logbook = new Logbook { Name = "logbook1", EstablishmentId=establishment.Id };
                context.Logbooks.Add(logbook);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var logbook = await context.Logbooks.FirstOrDefaultAsync(l => l.Name == "logbook1");
                var id = logbook.Id;
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.FindLogbookWithEstablishmentById(id);

                Assert.IsTrue(foundLogbook == logbook);
                Assert.IsTrue(foundLogbook.Establishment.Name== "TestEstablishment");
                Assert.IsTrue(foundLogbook.Establishment.Address.City.Name == "TestCity");
                Assert.IsTrue(foundLogbook.Establishment.Address.Street == "TestStreet");
            }
        }


        [TestMethod]
        public async Task ReturnNullIfLogbookDoesNotExist()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnNullIfLogbookDoesNotExist")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.FindLogbookWithEstablishmentById(1);

                Assert.IsTrue(foundLogbook == null);
            }
        }

        [TestMethod]
        public async Task ReturnNullIfLogbookIsDeleted()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnNullIfLogbookIsDeleted")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var logbook = new Logbook { Name = "logbook1", IsDeleted=true };
                context.Logbooks.Add(logbook);
                context.SaveChanges();
            }

            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var logbook = await context.Logbooks.FirstOrDefaultAsync(l => l.Name == "logbook1");
                var foundLogbook = await sut.FindLogbookWithEstablishmentById(logbook.Id);

                Assert.IsTrue(foundLogbook == null);
            }
        }
    }
}

