﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class GetLogbookByName_Should_Should
    {
        [TestMethod]
        public async Task ReturnALogbookWithThePassedName()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnALogbookWithThePassedName")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                context.Logbooks.Add(new Logbook { Name = "logbook1" });
                context.Logbooks.Add(new Logbook { Name = "logbook2", IsDeleted = true });
                context.SaveChanges();
                var lb1Id = context.Logbooks.FirstOrDefault(lb => lb.Name == "logbook1").Id;
                var lb2Id = context.Logbooks.FirstOrDefault(lb => lb.Name == "logbook2").Id;
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);

                var foundLogbooks = await sut.ListAllLogBooks();
                Assert.IsTrue(foundLogbooks.Count == 1);
                Assert.IsTrue(foundLogbooks.Any(l => l.Name == "logbook1"));

            }
        }
    }
}





