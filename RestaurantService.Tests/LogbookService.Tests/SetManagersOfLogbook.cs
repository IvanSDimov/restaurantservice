﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class SetManagersOfLogbook_Should
    {

        //public async Task<Logbook> SetManagersOfLogbook(Logbook logbook)
        //{
        //    IQueryable<ManagersLogbooks> managerIds = null;
        //    if (logbook != null)
        //    {
        //        managerIds = this.context.ManagersLogbooks.Where(ml => ml.LogbookId == logbook.Id);

        //        var managers = await this.context.Users.Where(u => !u.IsDeleted && managerIds.Any(ml => ml.ManagerId == u.Id)).ToListAsync();
        //        logbook.Managers = managers;

        //    }
        //    return logbook;
        //}


        [TestMethod]
        public async Task ReturnThePassedLogbookWithSetManagers()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnThePassedLogbookWithSetManagers")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                context.Users.Add(new ApplicationUser { UserName = "User1" });
                context.Users.Add(new ApplicationUser { UserName = "User2" });
                context.Users.Add(new ApplicationUser { UserName = "User3", IsDeleted=true});
                context.Logbooks.Add(new Logbook { Name = "logbook1" });
                context.SaveChanges();
                var lId = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1").Id;
                var manager_logbooks = context.Users.Select(u => new ManagersLogbooks
                {
                    ManagerId = u.Id,
                    LogbookId = lId
                });
                context.ManagersLogbooks.AddRange(manager_logbooks);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var logbook = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);

                var foundLogbook = await sut.SetManagersOfLogbook(logbook);
                Assert.IsTrue(foundLogbook.Managers.Count == 2);
                Assert.IsTrue(!foundLogbook.Managers.Any(l => l.UserName == "User3"));

            }
        }
    }
}





