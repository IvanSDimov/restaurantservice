﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class GetCount_Should
    {
        [TestMethod]
        public async Task ReturnTheCountOfNotDeletedLogbooks()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnTheCountOfNotDeletedLogbooks")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var lb1 = new Logbook();
                var lb2 = new Logbook();
                var lb3 = new Logbook { IsDeleted=true};

                context.Logbooks.AddRange(new List<Logbook> { lb1, lb2, lb3 });
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var returned = await sut.GetCount();

                Assert.IsTrue(returned ==2);
            }
        }
    }
}




