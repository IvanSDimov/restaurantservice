﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class GetNumberOfCommentsOfAnLogbook_Should
    {
        //public async Task<int> GetNumberOfCommentsOfAnLogbook(Logbook logbook)
        //{
        //    return await context.ManagerComments
        //       .Where(c => c.LogbookId == logbook.Id && c.IsDeleted == false).CountAsync();
        //}
        [TestMethod]
        public async Task ReturnTheCountOfCommentsInALogbook()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnTheCountOfCommentsInALogbook")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                context.Logbooks.Add(new Logbook { Name = "logbook1" });
                context.Logbooks.Add(new Logbook { Name = "logbook2" });
                context.SaveChanges();
                var lb1Id = context.Logbooks.FirstOrDefault(lb => lb.Name == "logbook1").Id;
                var lb2Id = context.Logbooks.FirstOrDefault(lb => lb.Name == "logbook2").Id;
                var comment1 = new ManagerComment
                {
                    LogbookId = lb1Id,
                };
                var comment2 = new ManagerComment
                {
                    LogbookId = lb1Id,
                };
                var comment3 = new ManagerComment
                {
                    LogbookId = lb2Id,
                };
                context.ManagerComments.AddRange(new List<ManagerComment> { comment1, comment2, comment3 });
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var logbook = context.Logbooks.FirstOrDefault(lb => lb.Name == "logbook1");
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var returned = await sut.GetNumberOfCommentsOfAnLogbook(logbook);

                Assert.IsTrue(returned == 2);
            }
        }
    }
}



