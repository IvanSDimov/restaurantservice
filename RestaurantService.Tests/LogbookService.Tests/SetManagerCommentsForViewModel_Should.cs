﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogbookService.Tests
{
    [TestClass]
    public class SetManagerCommentsForViewModel_Should
    {
        //public async Task<Logbook> SetManagerCommentsForViewModel(Logbook logbook)
        //{
        //    var comments = await context.ManagerComments
        //        .Where(c => c.LogbookId == logbook.Id && !c.IsDeleted).Include(c => c.Author)
        //        .OrderBy(c => c.IsDone)
        //        .ThenBy(c => c.DateCreated)
        //        .ToListAsync();

        //    logbook.managerComments = comments;
        //    return logbook;
        //}
        [TestMethod]
        public async Task FindAllNotDeletedManagerCommentsOfLogbookAndIncludeAuthor()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "FindAllNotDeletedManagerCommentsOfLogbookAndIncludeAuthor")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                context.Users.Add(new ApplicationUser { UserName = "User1" });
                context.Users.Add(new ApplicationUser { UserName = "User2" });
                context.Users.Add(new ApplicationUser { UserName = "User3" });
                context.Logbooks.Add(new Logbook { Name = "logbook1" });
                context.Logbooks.Add(new Logbook { Name = "logbook2" });
                context.SaveChanges();
                var lb1Id = context.Logbooks.FirstOrDefault(lb => lb.Name == "logbook1").Id;
                var lb2Id = context.Logbooks.FirstOrDefault(lb => lb.Name == "logbook2").Id;
                var comment1 = new ManagerComment
                {
                    LogbookId = lb1Id,
                    AuthorId = context.Users.FirstOrDefault(u => u.UserName == "User1").Id,
                    Text = "comment1"
                };
                var comment2 = new ManagerComment
                {
                    LogbookId = lb1Id,
                    AuthorId = context.Users.FirstOrDefault(u => u.UserName == "User2").Id,
                    Text = "comment2"
                };
                var comment3 = new ManagerComment
                {
                    LogbookId = lb2Id,
                    AuthorId = context.Users.FirstOrDefault(u => u.UserName == "User3").Id,
                    Text = "comment3"
                };
                context.ManagerComments.AddRange(new List<ManagerComment> { comment1, comment2, comment3 });
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var logbook = context.Logbooks.FirstOrDefault(lb => lb.Name == "logbook1");
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.SetManagerCommentsForViewModel(logbook);

                Assert.IsTrue(foundLogbook.ManagerComments.Count == 2);
                Assert.IsTrue(foundLogbook.ManagerComments.Any(c => c.Text == "comment1"));
                Assert.IsTrue(foundLogbook.ManagerComments.Any(c => c.Text == "comment2"));
                Assert.IsTrue(foundLogbook.ManagerComments.Any(c => c.Author.UserName == "User1"));
                Assert.IsTrue(foundLogbook.ManagerComments.Any(c => c.Author.UserName == "User2"));
                Assert.IsTrue(!foundLogbook.ManagerComments.Any(c => c.Text == "comment3"));
            }
        }


        [TestMethod]
        public async Task OrderCommentsByIsDoneTnenByDate()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "OrderCommentsByIsDoneTnenByDate")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                context.Users.Add(new ApplicationUser { UserName = "User1" });
                context.Users.Add(new ApplicationUser { UserName = "User2" });
                context.Users.Add(new ApplicationUser { UserName = "User3" });
                context.Logbooks.Add(new Logbook { Name = "logbook1" });
                context.SaveChanges();
                var lb1Id = context.Logbooks.FirstOrDefault(lb => lb.Name == "logbook1").Id;
                var comment1 = new ManagerComment
                {
                    LogbookId = lb1Id,
                    AuthorId = context.Users.FirstOrDefault(u => u.UserName == "User1").Id,
                    IsDone = true,
                    DateCreated = DateTime.Now.AddMinutes(1),
                    Text = "comment1"
                };
                var comment2 = new ManagerComment
                {
                    LogbookId = lb1Id,
                    AuthorId = context.Users.FirstOrDefault(u => u.UserName == "User2").Id,
                    DateCreated = DateTime.Now.AddMinutes(3),
                    Text = "comment2"
                };
                var comment3 = new ManagerComment
                {
                    LogbookId = lb1Id,
                    AuthorId = context.Users.FirstOrDefault(u => u.UserName == "User3").Id,
                    DateCreated = DateTime.Now.AddMinutes(2),
                    Text = "comment3"
                };
                context.ManagerComments.AddRange(new List<ManagerComment> { comment1, comment2, comment3 });
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var logbook = context.Logbooks.FirstOrDefault(lb => lb.Name == "logbook1");
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.SetManagerCommentsForViewModel(logbook);

                Assert.IsTrue(foundLogbook.ManagerComments.Count == 3);
                Assert.IsTrue(foundLogbook.ManagerComments.First().Text == "comment3");
                Assert.IsTrue(foundLogbook.ManagerComments.Last().Text == "comment1");
            }
        }

        [TestMethod]
        public async Task NotIncludeDeletedComments()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "NotIncludeDeletedComments")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                context.Users.Add(new ApplicationUser { UserName = "User1" });
                context.Users.Add(new ApplicationUser { UserName = "User2" });
                context.Users.Add(new ApplicationUser { UserName = "User3" });
                context.Logbooks.Add(new Logbook { Name = "logbook1" });
                context.SaveChanges();
                var lb1Id = context.Logbooks.FirstOrDefault(lb => lb.Name == "logbook1").Id;
                var comment1 = new ManagerComment
                {
                    LogbookId = lb1Id,
                    AuthorId = context.Users.FirstOrDefault(u => u.UserName == "User1").Id,
                    Text = "comment1"
                };
                var comment2 = new ManagerComment
                {
                    LogbookId = lb1Id,
                    AuthorId = context.Users.FirstOrDefault(u => u.UserName == "User2").Id,
                    Text = "comment2"
                };
                var comment3 = new ManagerComment
                {
                    LogbookId = lb1Id,
                    AuthorId = context.Users.FirstOrDefault(u => u.UserName == "User3").Id,
                    Text = "comment3",
                    IsDeleted=true
                };
                context.ManagerComments.AddRange(new List<ManagerComment> { comment1, comment2, comment3 });
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var logbook = context.Logbooks.FirstOrDefault(lb => lb.Name == "logbook1");
                var fakeUserService = new Mock<IUserService>();
                var sut = new RestaurantService.Services.LogbookService(context, fakeUserService.Object);
                var foundLogbook = await sut.SetManagerCommentsForViewModel(logbook);

                Assert.IsTrue(foundLogbook.ManagerComments.Count == 2);
                Assert.IsTrue(!foundLogbook.ManagerComments.Any(c => c.Text == "comment3"));
            }
        }
    }
}



