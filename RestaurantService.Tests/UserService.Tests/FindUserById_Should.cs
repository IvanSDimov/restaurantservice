﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Threading.Tasks;

namespace UserService.Tests
{
    [TestClass]
    public class FindUserById_Should
    {
        [TestMethod]
        public async Task FindUserIfExists()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "FindUserIfExists")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var user = new ApplicationUser { UserName = "user1" };
                context.Users.Add(user);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var user = await context.Users.FirstOrDefaultAsync(l => l.UserName == "user1");
                var id = user.Id;
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null,null,null,null,null,null,fakeServiceProvider.Object,null);
                
                var sut = new RestaurantService.Services.UserService(context, fakeServiceProvider.Object, userManager);
                var foundUser = await sut.FindUserById(id);

                Assert.IsTrue(foundUser == user);

            }
        }


        [TestMethod]
        public async Task ReturnNullIfUserDoesNotExist()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnNullIfUserDoesNotExist")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var sut = new RestaurantService.Services.UserService(context, fakeServiceProvider.Object, userManager);
                var foundUser = await sut.FindUserById("a");

                Assert.IsTrue(foundUser == null);

            }
        }
    }
}
