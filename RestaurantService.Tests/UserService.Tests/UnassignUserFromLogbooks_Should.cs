﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserService.Tests
{
    [TestClass]
    public class UnassignUserFromLogbooks_Should_Should
    {
        [TestMethod]
        public async Task UnassignUserFromPointedLogbooks()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "UnassignUserFromPointedLogbooks")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {

                var user1 = new ApplicationUser { UserName = "user1" };
                var logbook1 = new Logbook { Name = "logbook2" };
                var logbook2 = new Logbook { Name = "logbook1" };
                var logbook3 = new Logbook { Name = "logbook3" };

                context.Logbooks.AddRange(new List<Logbook> { logbook1, logbook2, logbook3 });
                context.Users.Add(user1);
                context.SaveChanges();
                user1 = context.Users.FirstOrDefault(u => u.UserName == "user1");
                logbook1 = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");
                logbook2 = context.Logbooks.FirstOrDefault(l => l.Name == "logbook2");
                logbook3 = context.Logbooks.FirstOrDefault(l => l.Name == "logbook3");

                foreach (var item in new List<int> { logbook1.Id, logbook2.Id, logbook3.Id})
                {
                    context.ManagersLogbooks.Add(new ManagersLogbooks { ManagerId = user1.Id, LogbookId = item});
                }
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var logbook1 = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");
                var logbook2 = context.Logbooks.FirstOrDefault(l => l.Name == "logbook2");
                var user1 = context.Users.FirstOrDefault(u => u.UserName == "user1");

                var sut = new RestaurantService.Services.UserService(context, fakeServiceProvider.Object, userManager);
                await sut.UnassignUserFromLogbooks(user1.Id, new List<int> {logbook1.Id,logbook2.Id });

                Assert.IsTrue(context.ManagersLogbooks.Count() == 2);
                Assert.IsTrue(context.ManagersLogbooks.Any(ul => ul.LogbookId == logbook1.Id&&ul.ManagerId==user1.Id));
                Assert.IsTrue(context.ManagersLogbooks.Any(ul => ul.LogbookId == logbook2.Id && ul.ManagerId == user1.Id));
            }
        }
    }
}


