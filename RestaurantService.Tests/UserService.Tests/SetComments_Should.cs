﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserService.Tests
{
    [TestClass]
    public class SetComment_Should
    {
        [TestMethod]
        public async Task SetCommentsToThePassedUser()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "SetCommentsToThePassedUser")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var user1 = new ApplicationUser { UserName = "user1" };
                context.Users.Add(user1);
                context.SaveChanges();
                user1 = context.Users.FirstOrDefault(u => u.UserName == "user1");
                var comment1 = new ManagerComment {Text= "comment1" ,AuthorId=user1.Id};
                var comment2 = new ManagerComment { Text = "comment2", AuthorId = user1.Id };
                var comment3 = new ManagerComment { Text = "comment3", AuthorId = "a" };
                context.ManagerComments.AddRange(new List<ManagerComment> {comment1, comment2, comment3});
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var user = context.Users.FirstOrDefault(u => u.UserName == "user1");
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var sut = new RestaurantService.Services.UserService(context, fakeServiceProvider.Object, userManager);
                await sut.SetComments(user);
                var foundUser = context.Users.FirstOrDefault(u => u.UserName == "user1");

                Assert.IsTrue(foundUser.ManagerComments.Count == 2);
                Assert.IsTrue(foundUser.ManagerComments.Any(c=>c.Text =="comment1"));
                Assert.IsTrue(foundUser.ManagerComments.Any(c => c.Text == "comment2"));
            }
        }
    }
}
