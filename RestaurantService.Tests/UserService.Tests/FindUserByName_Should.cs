﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Threading.Tasks;

namespace UserService.Tests
{
    [TestClass]
    public class FindUserByName_Should
    {
        [TestMethod]
        public async Task FindUserByNameIfExists()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "FindUserByNameIfExists")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var user = new ApplicationUser { UserName = "user1" };
                context.Users.Add(user);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var sut = new RestaurantService.Services.UserService(context, fakeServiceProvider.Object, userManager);
                var foundUser = await sut.FindUserByName("user1");

                Assert.IsTrue(foundUser.UserName == "user1");

            }
        }


        [TestMethod]
        public async Task ReturnNullIfUserWithThePassedNameDoesNotExist()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnNullIfUserWithThePassedNameDoesNotExist")
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var sut = new RestaurantService.Services.UserService(context, fakeServiceProvider.Object, userManager);
                var foundUser = await sut.FindUserByName("a");

                Assert.IsTrue(foundUser == null);

            }
        }
    }
}
