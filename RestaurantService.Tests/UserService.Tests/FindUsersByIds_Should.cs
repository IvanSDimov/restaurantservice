﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserService.Tests
{
    [TestClass]
    public class FindUsersByIds_Should
    {
        [TestMethod]
        public async Task FindUsersByIdsIfExist()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "FindUsersByIdsIfExist")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var user1 = new ApplicationUser { UserName = "user1" };
                var user2 = new ApplicationUser { UserName = "user2" };
                context.Users.Add(user1);
                context.Users.Add(user2);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var user1 = context.Users.FirstOrDefault(u => u.UserName == "user1");
                var user2 = context.Users.FirstOrDefault(u => u.UserName == "user2");
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var sut = new RestaurantService.Services.UserService(context, fakeServiceProvider.Object, userManager);
                var foundUsers = await sut.FindUsersByIds(new List<string> { user1.Id, user2.Id, "user3" });

                Assert.IsTrue(foundUsers.Count == 2);
                Assert.IsTrue(foundUsers.Any(u => u.UserName == "user1"));
                Assert.IsTrue(foundUsers.Any(u => u.UserName == "user2"));

            }
        }
    }
}


