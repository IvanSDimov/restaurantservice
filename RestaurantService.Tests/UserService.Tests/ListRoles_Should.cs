﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserService.Tests
{
    [TestClass]
    public class ListRoles_Should
    {
        [TestMethod]
        public async Task ListAllRolesInTheDatabase()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ListAllRolesInTheDatabase")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var role1 = new IdentityRole { Name = "role1"};
                var role2 = new IdentityRole { Name = "role2" };
                var role3 = new IdentityRole { Name = "role3" };

                context.Roles.AddRange(new List<IdentityRole> { role1, role2, role3 });
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var sut = new RestaurantService.Services.UserService(context, fakeServiceProvider.Object, userManager);
                var roles = await sut.ListRoles();

                Assert.IsTrue(roles.Count == 3);
                Assert.IsTrue(roles.Any(r => r== "role1"));
                Assert.IsTrue(roles.Any(r => r == "role2"));
                Assert.IsTrue(roles.Any(r => r == "role3"));
            }
        }
    }
}

