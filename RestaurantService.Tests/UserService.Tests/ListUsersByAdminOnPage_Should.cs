﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserService.Tests
{
    [TestClass]
    public class ListUsersByAdminOnPage_Should
    {
        [TestMethod]
        public async Task ReturnUsersByAdminOnPageAndCountOfAllUsersByAdmin()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnUsersByAdminOnPageAndCountOfAllUsersByAdmin")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var user1 = new ApplicationUser { UserName = "user1", AdminId="a1" };
                var user2 = new ApplicationUser { UserName = "user2", AdminId="a2" };
                var user3 = new ApplicationUser { UserName = "user3", AdminId = "a1" };
                var user4 = new ApplicationUser { UserName = "user4", AdminId = "a1" };
                var user5 = new ApplicationUser { UserName = "user5", AdminId = "a1", IsDeleted=true };
                context.Users.Add(user1);
                context.Users.Add(user2);
                context.Users.Add(user3);
                context.Users.Add(user4);
                context.Users.Add(user5);
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var sut = new RestaurantService.Services.UserService(context, fakeServiceProvider.Object, userManager);
                var foundUsers = await sut.ListUsersByAdminOnPage(2, 0, "a1" );
                var foundUsers1 = await sut.ListUsersByAdminOnPage(2, 1, "a1");

                Assert.IsTrue(foundUsers.Item1 == 3);
                Assert.IsTrue(foundUsers.Item2.Count == 2);
                Assert.IsTrue(foundUsers.Item2.Any(u=>u.UserName == "user1"));
                Assert.IsTrue(foundUsers.Item2.Any(u => u.UserName == "user3"));
                Assert.IsTrue(foundUsers1.Item1 == 3);
                Assert.IsTrue(foundUsers1.Item2.Count == 1);
                Assert.IsTrue(foundUsers1.Item2.Any(u => u.UserName == "user4"));


            }
        }
    }
}
