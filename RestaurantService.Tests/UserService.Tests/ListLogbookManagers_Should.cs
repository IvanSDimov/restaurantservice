﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserService.Tests
{
    [TestClass]
    public class ListlogbookManagers_Should
    {
        [TestMethod]
        public async Task ReturnActiveManagersOfLogbook()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ReturnActiveManagersOfLogbook")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var logbook1 = new Logbook { Name = "logbook1" };
                context.SaveChanges();
                
                var user1 = new ApplicationUser { UserName = "user1"};
                var user2 = new ApplicationUser { UserName = "user2" };
                var user3 = new ApplicationUser { UserName = "user3"};
                var user4 = new ApplicationUser { UserName = "user4"};
                var user5 = new ApplicationUser { UserName = "user5", IsDeleted = true };
                context.Roles.Add(new IdentityRole { Name = "Manager" });
                context.Logbooks.Add(logbook1);
                context.Users.Add(user1);
                context.Users.Add(user2);
                context.Users.Add(user3);
                context.Users.Add(user4);
                context.Users.Add(user5);
                context.SaveChanges();
                user1 = context.Users.FirstOrDefault(u => u.UserName == "user1");
                user2 = context.Users.FirstOrDefault(u => u.UserName == "user2");
                user3 = context.Users.FirstOrDefault(u => u.UserName == "user3");
                user4 = context.Users.FirstOrDefault(u => u.UserName == "user4");
                user5 = context.Users.FirstOrDefault(u => u.UserName == "user5");
                logbook1 = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");
                var role = context.Roles.FirstOrDefault(r => r.Name == "Manager");
                foreach (var item in new List<string> { user1.Id, user2.Id, user4.Id, user5.Id })
                {
                    context.ManagersLogbooks.Add(new ManagersLogbooks { ManagerId = item, LogbookId =logbook1.Id });
                }
                foreach (var item in new List<string> { user1.Id, user2.Id, user3.Id, user5.Id })
                {
                    context.UserRoles.Add(new IdentityUserRole<string> { UserId = item, RoleId = role.Id });
                }
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var logbook1 = context.Logbooks.FirstOrDefault(l => l.Name == "logbook1");

                var sut = new RestaurantService.Services.UserService(context, fakeServiceProvider.Object, userManager);
                var foundUsers = await sut.ListlogbookManagers(logbook1.Id);

                Assert.IsTrue(foundUsers.Count == 2);
                Assert.IsTrue(foundUsers.Any(u => u.UserName == "user1"));
                Assert.IsTrue(foundUsers.Any(u => u.UserName == "user2"));
            }
        }
    }
}


