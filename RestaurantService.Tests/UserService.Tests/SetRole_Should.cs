﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserService.Tests
{
    [TestClass]
    public class SetRole_Should
    {
        [TestMethod]
        public async Task SetRoleToThePassedUser()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "SetRoleToThePassedUser")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                var user1 = new ApplicationUser { UserName = "user1"};
                var role1 = new IdentityRole("role1");
                context.Users.Add(user1);
                context.Roles.Add(role1);
                context.SaveChanges();
                user1=context.Users.FirstOrDefault(u => u.UserName == "user1");
                role1 = context.Roles.FirstOrDefault(r => r.Name == "role1");
                context.UserRoles.Add(new IdentityUserRole<string> { UserId = user1.Id, RoleId = role1.Id });
                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                var user = context.Users.FirstOrDefault(u => u.UserName == "user1");
                var fakeUserStore = new Mock<IUserStore<ApplicationUser>>();
                var fakeServiceProvider = new Mock<IServiceProvider>();
                var userManager = new UserManager<ApplicationUser>(fakeUserStore.Object,
                        null, null, null, null, null, null, fakeServiceProvider.Object, null);

                var sut = new RestaurantService.Services.UserService(context, fakeServiceProvider.Object, userManager);
                await sut.SetRole(user);
                var foundUser = context.Users.FirstOrDefault(u => u.UserName == "user1");

                Assert.IsTrue(foundUser.Role == "role1");
            }
        }
    }
}
