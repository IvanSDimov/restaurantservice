﻿using Microsoft.EntityFrameworkCore;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services
{
    public class KindService: IKindService
    {
        private readonly ApplicationDbContext context;

        public KindService(ApplicationDbContext context)
        {
            this.context = context;
        }

        public async Task<List<Kind>> ListKinds()
        {
            return await context.Kinds.ToListAsync();
        }

        public async Task<Kind> GetKindByName(string name)
        {
            return await this.context.Kinds.
                FirstOrDefaultAsync(e => e.Name == name);
        }
    }
}
