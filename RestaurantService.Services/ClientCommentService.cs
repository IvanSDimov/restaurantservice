﻿using Microsoft.EntityFrameworkCore;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services
{
    public class ClientCommentService: IClientCommentService
    {
        private readonly ApplicationDbContext context;

        public ClientCommentService(ApplicationDbContext context)
        {
            this.context = context;
        }

        public async Task<ClientComment> AddComment(int establishmentId, string text, string name, Moderation moderation=null)
        {
            if (string.IsNullOrEmpty(text)||text.Length<3)
            {
                throw new ArgumentException("Comment should be at least 3 characters long!");
            }
            if (string.IsNullOrEmpty(name)) name = "Anonymous";
            var comment = new ClientComment
            {
                Text = text,
                Author = name,
                DateCreated = DateTime.Now,
                EstablishmentId=establishmentId,
            };
            if (moderation != null)
            {
                comment.ModerationId = moderation.Id;
            }
            context.ClientComments.Add(comment);
            await context.SaveChangesAsync();
            return comment;
        }

        public async Task<List<string>> CheckForSwearWords(List<string> words)
        {
            var lowerWords = words.Select(w => w.ToLower());
            var uniqueLowerWords = new HashSet<string>(lowerWords).ToList();
            var found= await this.context.SwearDictionaries.Where(w => uniqueLowerWords.Contains(w.Words)).ToListAsync();
            if (found.Count > 0)
                return found.Select(w => w.Words).ToList();
            else return new List<string>();
        }

        public async Task<ClientComment> FindCommentById(int id)
        {
            return await this.context.ClientComments.FindAsync(id);
        }

        public async Task<ClientComment> UpdateComment(int id, string text, int? moderationId=null)
        {
            var comment = await this.context.ClientComments.FindAsync(id);
            comment.Text = text;
            comment.ModerationId = moderationId;
            this.context.ClientComments.Update(comment);
            this.context.SaveChanges();
            return comment;
        }
    }

}
