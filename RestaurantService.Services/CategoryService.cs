﻿using Microsoft.EntityFrameworkCore;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ApplicationDbContext context;
        

        public CategoryService(ApplicationDbContext context)
        {
            this.context = context;
            
        }
        public async Task<List<Category>> ListAllCategories()
        {
            return await context.Categories.ToListAsync();
        }
        public async Task<Category> FindCategoryById(int id)
        {
            return await context.Categories.FindAsync(id);
        }
    }
}
