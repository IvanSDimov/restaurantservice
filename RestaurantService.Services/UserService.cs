﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext context;
        private IServiceProvider serviceProvider;
        private readonly UserManager<ApplicationUser> userManager;

        public UserService(ApplicationDbContext context, IServiceProvider serviceProvider, UserManager<ApplicationUser> userManager)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.serviceProvider = serviceProvider;
            this.userManager = userManager;
        }

        public async Task<ApplicationUser> FindUserById(string Id)
        {
            return await context.Users.FindAsync(Id);
        }

        public async Task<ApplicationUser> FindUserByName(string name)
        {
            return await context.Users.FirstOrDefaultAsync(u => u.UserName == name);
        }

        public async Task<List<ApplicationUser>> FindUsersByNames(List<string> names)
        {
            return await context.Users.Where(u =>names.Contains(u.UserName)).ToListAsync();
        }

        public async Task<List<ApplicationUser>> FindUsersByIds(List<string> ids)
        {
            return await context.Users.Where(u => ids.Contains(u.Id)).ToListAsync();
        }

        public async Task< Tuple < int, IList<ApplicationUser>>> ListUsersByAdminOnPage(int onPage, int page, string adminId)
        {
            var adminRole = await context.Roles.FirstOrDefaultAsync(r => r.Name == "Admin");
            var users = context.Users
                .Where(u => !u.IsDeleted
                    && u.AdminId == adminId);
            var count = await users.CountAsync();
            var usersOnPage=await users.Skip(onPage * page).Take(onPage).ToListAsync();
            return new Tuple<int, IList<ApplicationUser>>(count, usersOnPage);
        }

        public async Task<Tuple<int, IList<ApplicationUser>>> ListDeletedUsersByAdminOnPage(int onPage, int page, string adminId)
        {
            var adminRole = await context.Roles.FirstOrDefaultAsync(r => r.Name == "Admin");
            var users = context.Users
                .Where(u => u.IsDeleted
                    && u.AdminId == adminId
                    );
            var count = await users.CountAsync();
            var usersOnPage = await users.Skip(onPage * page).Take(onPage).ToListAsync();
            return new Tuple<int, IList<ApplicationUser>>(count, usersOnPage);
        }

        public async Task<int> GetCount()
        {
            return await context.Users.Where(u => !u.IsDeleted).CountAsync();
        }

        public async Task DeleteUser(ApplicationUser user)
        {
            user.IsDeleted = true;
            context.Users.Update(user);
            await context.SaveChangesAsync();
        }

        public async Task UndeleteUser(ApplicationUser user)
        {
            user.IsDeleted = false;
            context.Users.Update(user);
            await context.SaveChangesAsync();
        }

        public async Task<ApplicationUser> SetRole(ApplicationUser user)
        {
            var roleId = await context.UserRoles.Where(u => u.UserId == user.Id).Select(ur => ur.RoleId).FirstOrDefaultAsync();
            var role = await context.Roles.FindAsync(roleId);
            if (role != null) user.Role = role.Name;
            return user;
        }

        public async Task<ApplicationUser> SetComments(ApplicationUser user)
        {
            var comments = await context.ManagerComments
                            .Where(c => c.AuthorId == user.Id && c.IsDeleted != true)
                            .ToListAsync();
            user.ManagerComments = comments;
            return user;
        }

        public async Task<List<string>> ListRoles()
        {
            return await this.context.Roles.Select(r => r.Name).ToListAsync();
        }


        public async Task<List<ApplicationUser>>ListManagersCreatedByAdmin(string AdminId)
        {
            var managerRole = await this.context.Roles.FirstOrDefaultAsync(r => r.Name == "Manager");
            var managerIds = this.context.UserRoles
                .Where(ur => ur.RoleId == managerRole.Id)
                .Select(ur => ur.UserId);
            var managers =await this .context.Users  
                .Where(u => u.AdminId == AdminId&&!u.IsDeleted)
                .Where(u=>managerIds.Contains(u.Id))
                .ToListAsync();
            return managers;
        }

        public async Task<List<ApplicationUser>> ListlogbookManagers(int logbookId)
        {
            var managerRole = await context.Roles.FirstOrDefaultAsync(r => r.Name == "Manager");
            var Id = managerRole.Id;
            var managerIds = context.UserRoles.Where(ur => ur.RoleId == Id).Select(ur => ur.UserId);

            var managers =await context.Users
                .Where(u => managerIds.Any(m=>m==u.Id))
                .Include(u => u.ManagerLogbooks)
                .Where(u => u.ManagerLogbooks.Any(ml=>ml.LogbookId == logbookId)&&!u.IsDeleted)
                .ToListAsync();
            return managers;
        }

        public async Task<List<ApplicationUser>> ListAllManagersByAdminExceptSpecifiedManagers(string adminId, List<ApplicationUser>users)
        {
            var managerRole = await context.Roles.FirstOrDefaultAsync(r => r.Name == "Manager");
            var Id = managerRole.Id;
            var managerIds = context.UserRoles.Where(ur => ur.RoleId == Id).Select(ur => ur.UserId).ToList();
            var managers = context.Users.Where(u => managerIds.Contains(u.Id) 
                                                    && u.AdminId==adminId
                                                    && !users.Contains(u)
                                                    && !u.IsDeleted).ToList();
            return managers;
        }

        public async Task UnassignUserFromLogbooks(string userId, List<int> logbooksToLeave)
        {
            var logbookManagersToRemove = this.context.ManagersLogbooks
                        .Where(ml => ml.ManagerId == userId && !logbooksToLeave.Contains(ml.LogbookId));
            this.context.ManagersLogbooks.RemoveRange(logbookManagersToRemove);
            await this.context.SaveChangesAsync();

        }
    }           
}
