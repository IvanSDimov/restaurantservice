﻿using Microsoft.EntityFrameworkCore;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services
{
    public class CityService:ICityService
    {
        private readonly ApplicationDbContext context;

        public CityService(ApplicationDbContext context)
        {
            this.context = context;
        }
        public async Task<List<City>> ListCities()
        {
            return await context.Cities.ToListAsync();
        }
    }
}
