﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services
{
    public class JsonService : IJsonService
    {
        private readonly ApplicationDbContext context;
        private IServiceProvider serviceProvider;

        public JsonService(ApplicationDbContext context, IServiceProvider serviceProvider)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.serviceProvider = serviceProvider;
        }

        public async Task DatabaseSeed()
        {
            var ManagerRole = context.Roles.FirstOrDefault(r => r.Name == "Manager");
            if (!context.UserRoles.Any(ur => ur.RoleId == ManagerRole.Id))
            {
                var userManeger = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                var managerUser = new ApplicationUser { UserName = "Manager", Email = "manager@gmail.com" };
                await userManeger.CreateAsync(managerUser, "Manager123@");
                await userManeger.AddToRoleAsync(managerUser, "Manager");
            }

            var establishmentJson = File.ReadAllText(@".\wwwroot\JSons\establishment.json");
            var establishment = JsonConvert.DeserializeObject<Establishment[]>(establishmentJson);
            var adminId = context.Users.FirstOrDefault(u => u.UserName == "Admin").Id;
            context.Set<Establishment>().AddRange(establishment.Where(x => !context.Establishments.Any(y => y.Name == x.Name || y.Image == x.Image && y.AdminId== adminId)));

            var ManagerCommentsJSon = File.ReadAllText(@".\wwwroot\JSons\ManagerComments.json");
            var ManagerComments = JsonConvert.DeserializeObject<ManagerComment[]>(ManagerCommentsJSon);
            context.Set<ManagerComment>().AddRange(ManagerComments.Where(x => !context.ManagerComments.Any(y => y.Text == x.Text)));

            var clientCommentJSon = File.ReadAllText(@".\wwwroot\JSons\ClientComments.json");
            var clientComments = JsonConvert.DeserializeObject<ClientComment[]>(clientCommentJSon);
            context.Set<ClientComment>().AddRange(clientComments.Where(cc => !context.ClientComments.Any(y => y.Text == cc.Text)));

            var cityJson = File.ReadAllText(@".\wwwroot\JSons\City.json");
            var city = JsonConvert.DeserializeObject<City[]>(cityJson);
            context.Set<City>().AddRange(city.Where(x => !context.Cities.Any(y => y.Name == x.Name)));

            var addressJson = File.ReadAllText(@".\wwwroot\JSons\address.json");
            var address = JsonConvert.DeserializeObject<Address[]>(addressJson);
            context.Set<Address>().AddRange(address.Where(x => !context.Addresses.Any(y => y.Street == x.Street)));

            var LogbookJson = File.ReadAllText(@".\wwwroot\JSons\LogBook.json");
            var logbooks = JsonConvert.DeserializeObject<Logbook[]>(LogbookJson);
            context.Set<Logbook>().AddRange(logbooks.Where(lg => !context.Logbooks.Any(l => l.Name == lg.Name)));

            var KindsJson = File.ReadAllText(@".\wwwroot\JSons\Kind.json");
            var kinds = JsonConvert.DeserializeObject<Kind[]>(KindsJson);
            context.Set<Kind>().AddRange(kinds.Where(k => !context.Kinds.Any(kn => kn.Name == k.Name)));

            var SwearJson = File.ReadAllText(@".\wwwroot\JSons\SwearDictionary.json");
            var swears = JsonConvert.DeserializeObject<SwearDictionary[]>(SwearJson);
            context.Set<SwearDictionary>().AddRange(swears.Where(s => !context.SwearDictionaries.Any(sd => sd.Words == s.Words)));

            var CategoryJson = File.ReadAllText(@".\wwwroot\JSons\Categories.json");
            var categories = JsonConvert.DeserializeObject<Category[]>(CategoryJson);
            context.Set<Category>().AddRange(categories.Where(c => !context.Categories.Any(ca => ca.Name == c.Name)));
             

            context.SaveChanges();
            //Set mangerComments to Logbooks
            var newLogbooks = context.Logbooks.Where(x => x.ManagerComments.Count == 0);
            var newManegerComments = context.ManagerComments.Where(x => ManagerComments.Any(y => y.Text == x.Text)).ToList();
            foreach (var newLogbook in newLogbooks)
            {
                var managerCommnets = FindRandomCollection(newManegerComments, 3);
                newLogbook.ManagerComments = managerCommnets.ToList();
            }
            //Seting Manager <-> Logbook:
            var managerRoleId = context.Roles.FirstOrDefault(r => r.Name == "Manager").Id;
            var managerIds = context.UserRoles.Where(ur => ur.RoleId == managerRoleId).Select(ur=>ur.UserId).ToList();
            var managers = context.Users.Where(u =>managerIds.Contains(u.Id)).ToList();
            var logbookIds = context.ManagersLogbooks.Select(mlg => mlg.LogbookId).ToList();            
            var newlogbooks = context.Logbooks.Where(lg => !logbookIds.Contains(lg.Id)).ToList();

            SeedManagersLogbooks(managers, newlogbooks);

            //foreach(var manager in managers)
            //{
            //    var logBooks = FindRandomCollection(newlogbooks, 3);
            //    manager.ManagerLogbooks = logBooks
            //        .Select(lg => new ManagersLogbooks() { LogbookId = lg.Id, ManagerId = manager.Id }).ToList(); 
            //}
            //Seting Establishment -> Addresses:
            var establishmentsToAddAddressTo = context.Establishments.Where(x => x.Address == null).ToList();
            var allAdresses = context.Addresses.ToList();
            SeedAddressesToEstablishment(establishmentsToAddAddressTo, allAdresses);
            
            //Seting Establishment -> Admin:
            var adminID = context.Users.FirstOrDefault(u => u.UserName == "Admin").Id;
            var establishmentsToAddAdminTo = context.Establishments.Where(x => x.AdminId == null).ToList();
            SeedAdminToEstablishment(establishmentsToAddAdminTo, adminID);

            //Seting Establisment -> Kind:
            var allKinds = context.Kinds.Select(k=>k.Id).ToList();
            var establishmentsToAddKindTo = context.Establishments.Where(e => e.KindId == null).ToList();
            SeedKindToEstablishment(establishmentsToAddKindTo, allKinds);

            //Seting Adress -> City:
            var addressesToAddCirtyTo = context.Addresses.Where(a => a.CityId == null).ToList();
            var allCities = context.Cities.Select(c=>c.Id).ToList();
            SeedCityToAddress(addressesToAddCirtyTo, allCities);

            //Seting Category <-> ManagersComments:
            var managerCommentsToAddCategoryTo = context.ManagerComments
                .Where(mc => !context.CategoryManagerComment.Any(cmc =>cmc.ManagerCommentId == mc.Id)).ToList();
            var categoryToAddManagerCommentTo = context.Categories
                .Where(c => !c.CategoryManagerComment.Any(cmc => cmc.CategoryId==c.Id)).ToList();
            SeedCategoryManager(categoryToAddManagerCommentTo, managerCommentsToAddCategoryTo);

            //Seting ClientComment -> Establishment:
            var clientCommentsToAddEstablishmentTo = context.ClientComments
                .Where(c => c.EstablishmentId == null).ToList();
            var allEstablishmentIds = context.Establishments.Select(e => e.Id).ToList();
            SeedEstablishmentToClientComment(clientCommentsToAddEstablishmentTo ,allEstablishmentIds);

            //Seting Logbook -> Establishment:
            var LogbooksToAddEstablishmentTo = context.Logbooks.Where(l => l.EstablishmentId == null).ToList();
            SeedEstablishmentToLogbook(LogbooksToAddEstablishmentTo, allEstablishmentIds);

            //Seting ManagerComment -> Logbook:
            var managerCommentToAddLogbookTo = context.ManagerComments.Where(mc => mc.LogbookId == null).ToList();
            var allLogbooks = context.Logbooks.Select(l=>l.Id).ToList();
            SeedLogbookToManagerComments(managerCommentToAddLogbookTo, allLogbooks);

            //Seting ManagerComments -> Author:
            var managerCommentToAddmanagerTo = context.ManagerComments.Where(mc => mc.AuthorId == null).ToList();
            var allmanagers = context.Users.Select(u => u.Id).ToList();
            SeedManagerToManagerComments(managerCommentToAddmanagerTo, allmanagers);


            context.SaveChanges();

        }

        private IEnumerable<U> FindRandomCollection<U>(IList<U> objectsToBeAdded, int counter)
        {
            var rnd = new Random();
            var indexes = new List<int>();
            while (counter > 0)
            {
                int n = rnd.Next(0, objectsToBeAdded.Count());
                if (!indexes.Contains(n))
                {
                    indexes.Add(n);
                    counter--;
                }
            }
            return indexes.Select(i => objectsToBeAdded[i]);
        }

        private void SeedManagersLogbooks(List<ApplicationUser> managers, List<Logbook> logbooks)
        {
            if(managers.Count >= logbooks.Count)
            {
                foreach (var manager in managers)
                {
                    int p = Math.Min(3, logbooks.Count);
                    var logBooks = FindRandomCollection(logbooks, p);
                    manager.ManagerLogbooks = logBooks
                        .Select(lg => new ManagersLogbooks() { LogbookId = lg.Id, ManagerId = manager.Id }).ToList();
                    context.Users.Update(manager);
                }
                
                context.SaveChanges();

            }
            else
            {
                foreach (var logbook in logbooks)
                {
                    int p = Math.Min(3, managers.Count);
                    var manager = FindRandomCollection(managers, p);
                    logbook.ManagersLogbooks = manager
                        .Select(m => new ManagersLogbooks() { LogbookId = logbook.Id, ManagerId = m.Id }).ToList();
                    context.Logbooks.Update(logbook);
                }
                context.SaveChanges();
            }
        }

        private void SeedAddressesToEstablishment(List<Establishment> establishmentsToAddTo, List<Address> allAdresses)
        {
            foreach (var establishmentToAddTo in establishmentsToAddTo)
            {
                var adress = FindRandomCollection(allAdresses, 1).ToList();
                establishmentToAddTo.Address = adress[0];
                context.Establishments.Update(establishmentToAddTo);
            }
            context.SaveChanges();
        }

        private void SeedAdminToEstablishment(List<Establishment> establishmentsToAddTo, string adminId)
        {
            establishmentsToAddTo.Select(e => e.AdminId = adminId);
            foreach(var establishment in establishmentsToAddTo)
            {
                context.Establishments.Update(establishment);
            }
            
            context.SaveChanges();
        }
        
        private void SeedKindToEstablishment(List<Establishment> establishmentsToAddTo, List<int> kindIds)
        {
            foreach (var establishmentToAddTo in establishmentsToAddTo)
            {
                var kindId = FindRandomCollection(kindIds, 1).ToList();
                establishmentToAddTo.KindId = kindId[0];
                context.Establishments.Update(establishmentToAddTo);
            }
            context.SaveChanges();
        }

        private void SeedCityToAddress(List<Address> addresses, List<int> allCities)
        {
            foreach (var address in addresses)
            {
                var cities = FindRandomCollection(allCities, 1).ToList();
                address.CityId = cities[0];
                context.Addresses.Update(address);
            }
            context.SaveChanges();
        }

        private void SeedCategoryManager(List<Category> allCategories, List<ManagerComment> allManagerComments)
        {
            
            if(allCategories.Count >= allManagerComments.Count)
            {
                foreach(var category in allCategories)
                {
                   
                    var managerComments = FindRandomCollection(allManagerComments, 3);
                    category.CategoryManagerComment = managerComments
                        .Select(mc => new CategoryManagerComment() { ManagerCommentId = mc.Id, CategoryId = category.Id }).ToList();
                    context.Categories.Update(category);
                    //context.CategoryManagerComment.AddRange(category.CategoryManagerComment);            
                }
                context.SaveChanges();
            }
            else
            {
                foreach (var managerComment in allManagerComments)
                {
                 
                    var category = FindRandomCollection(allCategories, 2);
                    managerComment.CategoryManagerComment = category
                        .Select(mc => new CategoryManagerComment() { ManagerCommentId = managerComment.Id, CategoryId = mc.Id }).ToList();
                    context.ManagerComments.Update(managerComment);
                    //context.CategoryManagerComment.AddRange(managerComment.CategoryManagerComment);
                }
                context.SaveChanges();
            }            
        }

        private void SeedEstablishmentToClientComment(List<ClientComment> clientComments, List<int> allEstablishmentIds)
        {
            foreach (var clientComment in clientComments)
            {
                context.ClientComments.Update(clientComment);
                var establishmentId = FindRandomCollection(allEstablishmentIds, 1).ToList();
                clientComment.EstablishmentId = establishmentId[0];                
            }
            context.SaveChanges();
        }

        private void SeedEstablishmentToLogbook(List<Logbook> logbooks, List<int> allEstablishmentIds)
        {
            foreach (var logbook in logbooks)
            {
                var establishmentId = FindRandomCollection(allEstablishmentIds, 1).ToList();
                logbook.EstablishmentId = establishmentId[0];
                context.Logbooks.Update(logbook);
            }
            context.SaveChanges();
        }

        private void SeedLogbookToManagerComments(List<ManagerComment> managerComments, List<int> allLogbooksIds)
        {
            foreach (var managerComment in managerComments)
            {
                var logbookId = FindRandomCollection(allLogbooksIds, 1).ToList();
                managerComment.LogbookId = logbookId[0];
                context.ManagerComments.Update(managerComment);
            }
            context.SaveChanges();
        }
        
        private void SeedManagerToManagerComments(List<ManagerComment> managerComments,List<string> allManagersIds)
        {
            foreach (var managerComment in managerComments)
            {
                var managerId = FindRandomCollection(allManagersIds, 1).ToList();
                managerComment.AuthorId = managerId[0];
                context.ManagerComments.Update(managerComment);
            }
            context.SaveChanges();
        }
    }
}
