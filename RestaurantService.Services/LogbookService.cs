﻿using Microsoft.EntityFrameworkCore;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services
{
    public class LogbookService : ILogbookService
    {
        private readonly ApplicationDbContext context;
        private readonly IUserService userService;

        public LogbookService(ApplicationDbContext context, IUserService userService)
        {
            this.context = context;
            this.userService = userService;
        }
        public async Task<Logbook> FindLogbookById(int? id)
        {
            return await context.Logbooks.FindAsync(id);
        }

        public async Task<Logbook> FindLogbookWithEstablishmentById(int id)
        {
            var logbooks = this.context
                    .Logbooks
                    .Where(l => !l.IsDeleted)
                    .Include(l => l.Establishment)
                    .ThenInclude(e => e.Address)
                    .ThenInclude(a => a.City);
            var logbook= await logbooks.FirstOrDefaultAsync(l => l.Id == id);
            return logbook;
        }

        public async Task<Logbook> FindLogbookByNameAndEstablishment(string name, int establishmentId)
        {
            return await this.context.Logbooks.Where(l => l.Name == name && l.EstablishmentId == establishmentId).FirstOrDefaultAsync();
        }


        public async Task<List<Logbook>> FindLogbooksByEstablishment(int establishmentId)
        {
            return await this.context.Logbooks.Where(l => l.EstablishmentId == establishmentId&&!l.IsDeleted).ToListAsync();
        }


        public async Task<List<Logbook>> FindDeletedLogbooksByEstablishment(int establishmentId)
        {
            return await this.context.Logbooks.Where(l => l.EstablishmentId == establishmentId && l.IsDeleted).ToListAsync();
        }


        public async Task<Logbook> UpdateLogbook(string name, int establishmentId, List<string> managerIds)
        {
            var existing = await this.FindLogbookByNameAndEstablishment(name, establishmentId);
            bool shouldSaveChanges = false;
            if (existing != null)
            {
                if (existing.Managers == null||existing.IsDeleted) existing.Managers = new List<ApplicationUser>();
                if (existing.IsDeleted)
                {
                    var managersToRemove =await this.context.ManagersLogbooks.Where(ml => ml.LogbookId == existing.Id).ToListAsync();
                    this.context.ManagersLogbooks.RemoveRange(managersToRemove);
                    shouldSaveChanges = true;
                }
                else
                {
                    var currManagerIds = this.context.ManagersLogbooks?
                        .Where(ml => ml.LogbookId == existing.Id)?
                        .Select(ml=>ml.ManagerId).ToList();
                    if(currManagerIds!=null)
                    managerIds = managerIds?.Where(mid => !currManagerIds.Any(cmid => cmid == mid)).ToList();
                }
                existing.IsDeleted = false;
                if (managerIds != null && managerIds.Count > 0)
                {
                    var managerLogbooks = managerIds.Select(m => new ManagersLogbooks { ManagerId = m, LogbookId = existing.Id }).ToList();
                    existing.ManagersLogbooks = managerLogbooks;
                    this.context.ManagersLogbooks.AddRange(managerLogbooks);
                    shouldSaveChanges = true;
                }
                if(shouldSaveChanges) await this.context.SaveChangesAsync();
            }
            return existing;
        }


        public async Task<Logbook> UpdateLogbook(int logbookId, 
                string name, 
                List<string> newManagerIds, 
                List<string> existingManagerIds)
        {
            var logbook = await this.context.Logbooks
                    .Include(l=>l.Establishment)
                    .ThenInclude(e=>e.Address)
                    .ThenInclude(a=>a.City)
                    .FirstOrDefaultAsync(l=>l.Id==logbookId);
            if (logbook.Name != name )
            {
                int establishmentId = logbook.EstablishmentId ?? 0;
                var existing = await this.FindLogbookByNameAndEstablishment(name, establishmentId);
                if (existing != null) return logbook;
            }

            logbook.Name = name;
            List<ManagersLogbooks> managersToDelete = null;
            List<ManagersLogbooks> managersToAdd = null;
            if (existingManagerIds != null)
            {
                 managersToDelete =await  this.context.ManagersLogbooks.Where(ml => ml.LogbookId == logbookId
                                    && !existingManagerIds.Contains(ml.ManagerId)).ToListAsync();
            }
            else
            {
                managersToDelete = await this.context.ManagersLogbooks.Where(ml => ml.LogbookId == logbookId).ToListAsync();
            }
            if (managersToDelete != null)
                this.context.ManagersLogbooks.RemoveRange(managersToDelete);
            if (newManagerIds != null)
            {
                managersToAdd = newManagerIds.Select(m => new ManagersLogbooks { ManagerId = m, LogbookId = logbookId }).ToList();
                this.context.ManagersLogbooks.AddRange(managersToAdd);
            }
            this.context.Logbooks.Update(logbook);
            await this.context.SaveChangesAsync();
            logbook = await this.SetManagersOfLogbook(logbook);

            return logbook;
        }


        public async Task DeleteLogbook(int id)
        {
            var logbook = await this.context.Logbooks.FindAsync(id);
            logbook.IsDeleted = true;
            //var logbookManagersToDelete = this.context.ManagersLogbooks.Where(ml => ml.LogbookId == id);
            //this.context.ManagersLogbooks.RemoveRange(logbookManagersToDelete);
            this.context.Logbooks.Update(logbook);
            await this.context.SaveChangesAsync();
        }

        public async Task UndeleteLogbook(int id)
        {
            var logbook = await this.context.Logbooks.FindAsync(id);
            logbook.IsDeleted = false;
            this.context.Logbooks.Update(logbook);
            await this.context.SaveChangesAsync();
        }


        public async Task<Logbook> CreateLogbookUsers(string name, int establishmentId, List<string>managerIds)
        {
            //var logbook = await this.FindLogbookByNameAndEstablishment(name, establishmentId);
            //if (logbook != null) return null;
            var logbook=new Logbook
            {
                Name = name,
                EstablishmentId = establishmentId
            };
            this.context.Logbooks.Add(logbook);
            await this.context.SaveChangesAsync();
            if (managerIds != null && managerIds.Count > 0)
            {
                var managerLogbooks = managerIds.Select(m => new ManagersLogbooks { ManagerId = m, LogbookId = logbook.Id }).ToList();
                this.context.ManagersLogbooks.AddRange(managerLogbooks);
                await this.context.SaveChangesAsync();
            }
            var created = await this.context.Logbooks.FirstOrDefaultAsync(l=>l.Name==name&&l.EstablishmentId==establishmentId);
            return logbook;
        }


        public async Task<List<Logbook>> GetAllManagerLogbooks(string managerId)
        {
            var logbookIds = await context.ManagersLogbooks
                .Where(ml => ml.ManagerId == managerId)
                .Select(ml => ml.LogbookId)
                .ToListAsync();
            return await context.Logbooks.Where(l => logbookIds.Contains(l.Id)&&!l.IsDeleted).ToListAsync();
        }


        //public async Task<Logbook> SetManagerCommentsForViewModel(Logbook logbook)
        //{
        //    var comments =  await context.ManagerComments
        //        .Where(c => c.LogbookId == logbook.Id && c.IsDeleted == false).Include(c=>c.Author)
        //        .OrderBy(c=>c.IsDone)
        //        .ThenBy(c=>c.DateCreated)
        //        .ToListAsync();
  
        //    logbook.managerComments = comments;
        //    return logbook;
        //}

        public async Task<int> GetNumberOfCommentsOfAnLogbook(Logbook logbook)
        {
            return await context.ManagerComments
               .Where(c => c.LogbookId == logbook.Id && c.IsDeleted == false ).CountAsync();
        }


        public async Task<int> GetCount()
        {
            return await context.Logbooks.Where(l=>!l.IsDeleted).CountAsync();
        }


        public async Task<Logbook> SetCommentsOnPageToLogbook(int onPage,int page,Logbook logbook)
        {
            var commentsOnPage = await context.ManagerComments
                .Where(c => c.LogbookId == logbook.Id && c.IsDeleted == false)
                .Include(c => c.Author)
                .Include(c=>c.CategoryManagerComment)
                .OrderBy(c => c.IsDone)
                .ThenByDescending(c => c.DateCreated).Skip(onPage * page).Take(onPage)
                .ToListAsync();
            if (commentsOnPage != null && commentsOnPage.Count > 0)
            {
                foreach (var item in commentsOnPage)
                {
                    item.Category = item.CategoryManagerComment
                        .Select(cm => cm.CategoryId)
                        .Select(c => context.Categories.Find(c))
                        .ToList();
                }
                logbook.ManagerComments = commentsOnPage;
            }

            return logbook;
        }


        public async Task<ICollection<Logbook>> ListAllLogBooks()
        {
            return await context.Logbooks.Where(l=>!l.IsDeleted).ToListAsync();
        }


        public async Task<Logbook> SetManagersOfLogbook(Logbook logbook)
        {
            IQueryable<ManagersLogbooks>managerIds=null;
            if (logbook != null)
            {
                managerIds = this.context.ManagersLogbooks.Where(ml => ml.LogbookId == logbook.Id);

                var managers =await this.context.Users.Where(u =>!u.IsDeleted && managerIds.Any(ml => ml.ManagerId == u.Id)).ToListAsync();
                logbook.Managers = managers;

            }
            return logbook;
        }

        public async Task<Dictionary<Tuple<string,int>,List<Logbook>>> DivideLogbooksByEstablishments(List<Logbook> logbooks)
        {
            var result = new Dictionary<Tuple<string, int>, List<Logbook>>();
            var establishmentIds = logbooks.Select(l => l.EstablishmentId);
            var establishments = await this.context.Establishments.Where(e => establishmentIds.Contains(e.Id)).ToListAsync();
            foreach (var est in establishments)
            {
                var logbooksForResult = logbooks.Where(l => l.EstablishmentId == est.Id).ToList();
                var key = new Tuple<string, int>(est.Name, est.Id);
                result[key] = logbooksForResult;
            }
            return result;
        }
    }
}
