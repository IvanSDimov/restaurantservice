﻿using Microsoft.EntityFrameworkCore;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services
{
    public class ModerationService:IModerationService
    {
        private readonly ApplicationDbContext context;

        public ModerationService(ApplicationDbContext context)
        {
            this.context = context;
        }

        public async Task<ModerationReason> GetReasonById(int id)
        {
            return await this.context.ModerationReasons.FindAsync(id);       
        }

        public async Task<ModerationReason> GetReasonByName(string name)
        {
            return await this.context.ModerationReasons.
                FirstOrDefaultAsync(e => e.Name == name);
        }

        public async Task<Moderation> CreateModeration(int? reasonId, string authorId)
        {
            var moderation = new Moderation
            {
                ModerationReasonId = reasonId,
                AuthorId = authorId,
                Date = DateTime.Now
            };
            this.context.Moderations.Add(moderation);
            await this.context.SaveChangesAsync();
            return moderation;
        }

        public async Task<List<ModerationReason>> ListModerationReasons()
        {
            return await this.context.ModerationReasons.ToListAsync();
        }
    }
}
