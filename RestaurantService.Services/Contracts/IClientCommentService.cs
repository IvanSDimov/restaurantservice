﻿using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services.Contracts
{
    public interface IClientCommentService
    {
        Task<ClientComment> AddComment(int establishmentId, string text, string name, Moderation moderation = null);
        Task<List<string>> CheckForSwearWords(List<string> words);
        Task<ClientComment> FindCommentById(int id);
        Task<ClientComment> UpdateComment(int id, string text, int? moderationId=null);
    }
}
