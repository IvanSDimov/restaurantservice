﻿using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services.Contracts
{
    public interface IManagerCommentService
    {
        Task<ManagerComment> AddComment(string text, ApplicationUser manager, Logbook logbook, byte[] image = null);
        Task EditComment(string newtext, ManagerComment comment);
        Task<ManagerComment> FindCommentById(int commentid);
        Task<Tuple<List<ManagerComment>, int>> ManagerCommentsOfManager(string managerId, int logbookId, int page, int onPage);
        Task<Tuple<List<ManagerComment>, int>> ManagerCommentsByCategory(int categoryId, int logbookId, int page, int onPage);
        Task<Tuple<IList<ManagerComment>, int>> SearchManagerComments(int onPage, int page, int LogbookId, DateTime FromDate, DateTime ToDate, List<string> ManagerIds, List<int> CategoryIds);
        Task EditCategories(List<int> categoryIds, int commentId);
        Task EditState(int commentId, bool state);
        Task<ManagerComment> DeleteImage(int id);

    }
}
