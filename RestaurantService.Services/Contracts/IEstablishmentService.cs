﻿using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services.Contracts
{
    public interface IEstablishmentService : IService
    {

        Task<IList<Establishment>> GetEstablishmentsOnPage(int onPage, int page, string AdminId = null);

        Task<Tuple<IList<Establishment>, int>> SearchEstablishments(int onPage, int page,
                    List<int> kindIds,
                    List<int> townIds,
                    string pattern,
                    string adminId = null);

        Task<Establishment> GetEstablishmentById(int Id);

        Task<Establishment> FindEstablishmentByNameAndAddress(string name, int addressId);

        Task<Establishment> CreateEstablishment(string name, Address address, string newFileName, Kind kind,string userId);

        Task<Establishment> SetAllPropsOfEstablishment(int establishmentId);

        Task<Establishment> SetKindAndAddressOfEstablishment(int establishmentId);

        Task<Establishment> UpdateEstablishment(Establishment establishment);

        Task<Tuple<List<ClientComment>, int>> GetCommentsOnPageOfEstablishment(int establishmentId, int page, int onPage);

    }
}
