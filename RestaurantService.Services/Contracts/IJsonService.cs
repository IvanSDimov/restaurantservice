﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services.Contracts
{
    public interface IJsonService
    {
        Task DatabaseSeed();
    }
}
