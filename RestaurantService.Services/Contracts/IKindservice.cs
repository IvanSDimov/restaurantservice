﻿using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services.Contracts
{
    public interface IKindService
    {
        Task<List<Kind>> ListKinds();
        Task<Kind> GetKindByName(string name);
    }
}
