﻿using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services.Contracts
{
    public interface ICategoryService
    {
        Task<List<Category>> ListAllCategories();
        Task<Category> FindCategoryById(int id);
    }
}
