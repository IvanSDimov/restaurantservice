﻿using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services.Contracts
{
    public interface IModerationService
    {
        Task<Moderation> CreateModeration(int? reasonId, string authorId);
        Task<ModerationReason> GetReasonByName(string name);
        Task<ModerationReason> GetReasonById(int id);
        Task<List<ModerationReason>> ListModerationReasons();
    }
}
