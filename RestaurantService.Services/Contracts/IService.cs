﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services
{
    public interface IService
    {
        Task<int> GetCount();
    }
}
