﻿using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services
{
    public interface IUserService:IService
    {
        Task<ApplicationUser> FindUserById(string Id);
        Task<List<ApplicationUser>> FindUsersByIds(List<string> ids);

        Task<ApplicationUser> FindUserByName(string name);
        Task<List<ApplicationUser>> FindUsersByNames(List<string> names);

        Task<Tuple<int, IList<ApplicationUser>>> ListUsersByAdminOnPage(int onPage, int page, string adminId);
        Task<Tuple<int, IList<ApplicationUser>>> ListDeletedUsersByAdminOnPage(int onPage, int page, string adminId);

        Task DeleteUser(ApplicationUser user);
        Task UndeleteUser(ApplicationUser user);

        Task<ApplicationUser> SetRole(ApplicationUser user);
        Task<ApplicationUser> SetComments(ApplicationUser user);

        Task<List<string>> ListRoles();
        Task<List<ApplicationUser>> ListManagersCreatedByAdmin(string AdminId);
        Task<List<ApplicationUser>> ListlogbookManagers(int logbookId);
        Task<List<ApplicationUser>> ListAllManagersByAdminExceptSpecifiedManagers(string adminId, List<ApplicationUser> users);

        Task UnassignUserFromLogbooks(string userId, List<int> logbooksToLeave);

        }
}
