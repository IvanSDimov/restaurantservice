﻿using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services.Contracts
{
    public interface IAddressService
    {
        Task<Address> FindAddress(string city, string street);
        Task<Address> CreateAddress(string city, string street);
    }
}
