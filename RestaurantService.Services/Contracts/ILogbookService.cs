﻿using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services.Contracts
{
    public interface ILogbookService :IService
    {
        Task<Logbook> FindLogbookById(int? id);

        Task<List<Logbook>> GetAllManagerLogbooks(string managerId);
       // Task<Logbook> SetManagerCommentsForViewModel(Logbook logbook);
        Task<int> GetNumberOfCommentsOfAnLogbook(Logbook logbook);
        Task<Logbook> SetCommentsOnPageToLogbook(int onPage, int page, Logbook logbook);


        Task<ICollection<Logbook>> ListAllLogBooks();
        Task<Logbook> FindLogbookByNameAndEstablishment(string name, int establishmentId);
        Task<List<Logbook>> FindLogbooksByEstablishment(int establishmentId);
        Task<List<Logbook>> FindDeletedLogbooksByEstablishment(int establishmentId);
        Task<Logbook> FindLogbookWithEstablishmentById(int id);
        Task<Logbook> CreateLogbookUsers(string name, int establishmentId, List<string> managerIds);
        Task<Logbook> UpdateLogbook(string name, int establishmentId, List<string> managerIds);
        Task<Logbook> UpdateLogbook(int logbookId,
                string name,
                List<string> newManagerIds,
                List<string> existingManagerIds);
        Task<Logbook> SetManagersOfLogbook(Logbook logbook);
        Task DeleteLogbook(int id);
        Task UndeleteLogbook(int id);
        Task<Dictionary<Tuple<string, int>, List<Logbook>>> DivideLogbooksByEstablishments(List<Logbook> logbooks);
    }
}
