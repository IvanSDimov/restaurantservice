﻿using Microsoft.EntityFrameworkCore;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services
{
    public class AddressService : IAddressService
    {
        private readonly ApplicationDbContext context;

        public AddressService(ApplicationDbContext context)
        {
            this.context = context;
        }

        public async Task<Address> FindAddress(string city, string street)
        {
            var address = await this.context.Addresses.Include(a=>a.City).FirstOrDefaultAsync(a => a.City.Name == city && a.Street.ToUpper() == street.ToUpper());
            return address;
        }

        public async Task<Address> CreateAddress(string city, string street)
        {
            var estCity = await this.context.Cities.FirstOrDefaultAsync(c => c.Name == city);
            var address = new Address
            {
                CityId = estCity.Id,
                Street = street.ToUpper()
            };
            this.context.Addresses.Add(address);
            await context.SaveChangesAsync();
            return address;
        }

    }
}
