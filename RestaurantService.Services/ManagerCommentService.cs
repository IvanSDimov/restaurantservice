﻿using Microsoft.EntityFrameworkCore;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services
{
    public class ManagerCommentService : IManagerCommentService
    {
        private readonly ApplicationDbContext context;

        public ManagerCommentService(ApplicationDbContext context)
        {
            this.context = context;
        }
        public async Task<ManagerComment> AddComment(string text, ApplicationUser manager, Logbook logbook, byte[] image = null)
        {
            if (string.IsNullOrEmpty(text))
            {
                throw new ArgumentException("Comment must not be empty!");
            }
            var comment = new ManagerComment()
            {
                AuthorId = manager.Id,
                Author = manager,
                Text = text,
                LogbookId = logbook.Id,
                Logbook = logbook,
                IsDeleted = false,
                IsDone = false,
                DateCreated = DateTime.Now,
                ByteImage = image
            };
            await context.ManagerComments.AddAsync(comment);
            await context.SaveChangesAsync();
            return comment;
        }
        public async Task<ManagerComment> FindCommentById(int commentid)
        {
            var comment = await context.ManagerComments.FindAsync(commentid);
            if (comment == null || comment.IsDeleted == true)
            {
                throw new ArgumentException($"No comment whith ID {commentid} exists.");
            }

            return comment;
        }


        public async Task EditComment(string newtext, ManagerComment comment)
        {
            if (comment == null || comment.IsDeleted == true)
            {
                throw new ArgumentException($"Comment does not exists.");
            }
            comment.Text = newtext;
            context.ManagerComments.Update(comment);
            await context.SaveChangesAsync();
        }

        public async Task<ManagerComment> DeleteImage(int id)
        {
            var comment = await this.context.ManagerComments.FindAsync(id);
            comment.ByteImage = null;
            this.context.ManagerComments.Update(comment);
            await this.context.SaveChangesAsync();
            return comment;

        }

        private IQueryable<ManagerComment> OrderCommentsWithAuthorAndLogbook(IQueryable<ManagerComment> comments)
        {
            return comments
                .Include(m => m.Author)
                .Include(m => m.Logbook)
                .Include(m => m.CategoryManagerComment)
                .OrderBy(m => m.IsDone)
                .ThenByDescending(m => m.DateCreated);
        }

        private async Task<List<ManagerComment>> ListCommentsOnPage(IQueryable<ManagerComment> allComments, int page, int onPage)
        {
            return await allComments
                .Skip(page * onPage)
                .Take(onPage)
                .ToListAsync();
        }

        private List<ManagerComment> SetCategoriesToComments(List<ManagerComment> comments)
        {
            foreach (var item in comments)
            {
                item.Category = item.CategoryManagerComment?
                    .Select(cm => cm.CategoryId)
                    .Select(c => context.Categories.Find(c))
                    .ToList();
            }
            return comments;
        }

        public async Task<Tuple<List<ManagerComment>, int>> ManagerCommentsByCategory(int categoryId, int logbookId, int page, int onPage)
        {

            var allComments = context.ManagerComments.Where(m => m.CategoryManagerComment.Any(cm => cm.CategoryId == categoryId)
                    && m.LogbookId == logbookId
                    && !m.IsDeleted);
            allComments = OrderCommentsWithAuthorAndLogbook(allComments);

            var count = await allComments.CountAsync();
            var comments = await ListCommentsOnPage(allComments, page, onPage);
            comments = SetCategoriesToComments(comments);

            if (comments == null || comments.Count == 0)
            {
                throw new ArgumentException($"No comments was found.");
            }
            return new Tuple<List<ManagerComment>, int>(comments, count);
        }

        public async Task<Tuple<List<ManagerComment>, int>> ManagerCommentsOfManager(string managerId, int logbookId, int page, int onPage)
        {
            var allComments = context.ManagerComments
                .Where(mc => mc.AuthorId == managerId
                    && mc.IsDeleted == false
                    && mc.LogbookId == logbookId);
            allComments = OrderCommentsWithAuthorAndLogbook(allComments);
            var count = await allComments.CountAsync();
            var comments = await ListCommentsOnPage(allComments, page, onPage);
            comments = SetCategoriesToComments(comments);
            if (comments == null || comments.Count == 0)
            {
                throw new ArgumentException($"No comments was found.");
            }
            return new Tuple<List<ManagerComment>, int>(comments, count);
        }

        public async Task<Tuple<IList<ManagerComment>, int>> SearchManagerComments
            (int onPage,
            int page,
            int LogbookId,
            DateTime FromDate,
            DateTime ToDate,
            List<string> ManagerIds,
            List<int> CategoryIds)
        {
            var numberOfCategories = await context.Categories.CountAsync();

            var comments = context.ManagerComments
                .Include(mc => mc.Author)
                .Include(mc => mc.CategoryManagerComment)
                .Where(mc => mc.LogbookId == LogbookId);
            if (CategoryIds != null)
            {
                if ( CategoryIds.Count > 0)
                {
                    var foundCommentsIds = context.CategoryManagerComment
                            .Where(cm => CategoryIds.Contains(cm.CategoryId))
                            .Select(cm => cm.ManagerCommentId);
                    comments = comments.Where(c => foundCommentsIds.Contains(c.Id));
                    //comments = comments
                    //    .Include(mc => mc.CategoryManagerComment)
                    //    .Where(mc =>CategoryIds
                    //        .Any(cId => mc.CategoryManagerComment
                    //            .Any(cmc => cmc.CategoryId == cId)));              
                }
            }
            if (ManagerIds != null)
            {
                if (ManagerIds.Count > 0)
                {
                    comments = comments.Where(c => ManagerIds.Contains(c.AuthorId));
                }
            }

            if (FromDate != DateTime.MinValue)
            {
                comments = comments.Where(c => c.DateCreated.Year >= FromDate.Year && c.DateCreated.Month >= FromDate.Month && c.DateCreated.Day >= FromDate.Day);
            }
            if (ToDate != DateTime.MinValue)
            {
                comments = comments.Where(c => c.DateCreated.Year <= ToDate.Year && c.DateCreated.Month <= ToDate.Month && c.DateCreated.Day <= ToDate.Day);
            }
            var commentsToReturn = await comments.OrderBy(c => c.IsDone)
                .ThenByDescending(c => c.DateCreated).Skip(onPage * page).Take(onPage).ToListAsync();
            commentsToReturn = SetCategoriesToComments(commentsToReturn);
            var count = await comments.CountAsync();
            return new Tuple<IList<ManagerComment>, int>(commentsToReturn, count);
        }

        public async Task EditCategories(List<int> categoryIds, int commentId)
        {
            var categoryManagerComments = new List<CategoryManagerComment>();
            var categoriesToDelete = await context.CategoryManagerComment.Where(cmc => cmc.ManagerCommentId == commentId).ToListAsync();
            context.CategoryManagerComment.RemoveRange(categoriesToDelete);

            foreach (var categoryId in categoryIds)
            {
                categoryManagerComments.Add(new CategoryManagerComment { CategoryId = categoryId, ManagerCommentId = commentId });
            }
            context.CategoryManagerComment.AddRange(categoryManagerComments);
            await context.SaveChangesAsync();
        }


        public async Task EditState(int commentId, bool state)
        {
            var comment = await context.ManagerComments.FindAsync(commentId);
            if (comment == null || comment.IsDeleted == true)
            {
                throw new ArgumentException($"Comment does not exsist.");
            }
            comment.IsDone = state;
            context.ManagerComments.Update(comment);
            await context.SaveChangesAsync();
        }

    }
}

