﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantService.Services
{
    public class EstablishmentService : IEstablishmentService
    {
        private readonly ApplicationDbContext context;
        private readonly ILogbookService logbookService;

        public EstablishmentService(ApplicationDbContext context, ILogbookService logbookService)
        {
            this.context = context;
            this.logbookService = logbookService;
        }

        public async Task<int> GetCount()
        {
            return await context.Establishments.Where(u => !u.IsDeleted).CountAsync();
        }

        public async Task<IList<Establishment>> GetEstablishmentsOnPage(int onPage, int page, string аdminId = null)
        {
            var establishmentsQuery = context.Establishments
                   .Include(e => e.Address)
                    .ThenInclude(a => a.City)
                   .Include(e => e.Kind);
            IQueryable<Establishment> establishments = establishmentsQuery;
            if (!string.IsNullOrEmpty(аdminId))
            {
                establishments = establishmentsQuery.Where(e => e.AdminId == аdminId);   
            }
            return await establishments.Skip(onPage * page).Take(onPage).ToListAsync(); 
        }

        public async Task<Tuple<IList<Establishment>, int>> SearchEstablishments(int onPage, int page,
                    List<int> kindIds,
                    List<int> townIds,
                    string pattern,
                    string adminId = null
                    )
        {
            var allkinds = await this.context.Kinds.CountAsync();
            var alltowns = await this.context.Cities.CountAsync();
            
            IQueryable<Establishment> found = this.context.Establishments
                .Include(x => x.Address)
                .ThenInclude(a => a.City)
                .Include(x => x.Kind);

            if (!string.IsNullOrEmpty(adminId)) found = found.Where(e => e.AdminId == adminId);
            if (townIds!=null) {
                if (townIds.Count > 0 && townIds.Count < alltowns)
                    found = found
                        .Where(e => e.Address.CityId is int
                        && townIds.Contains((int)e.Address.CityId));
            }
            if (kindIds != null)
            {
                if (kindIds.Count > 0 && kindIds.Count < allkinds)
                    found = found
                        .Where(e => e.KindId is int
                        && kindIds.Contains((int)e.KindId));
            }
            if (!string.IsNullOrEmpty(pattern))
                found = found.Where(e => e.Name.ToLower().Contains(pattern.ToLower()));
            var establishments = found.Skip(onPage * page).Take(onPage).ToList();
            var count = await found.CountAsync();
            return new Tuple<IList<Establishment>, int>(establishments, count);
        }

        public async Task<Establishment> CreateEstablishment(string name,
            Address address,
            string newFileName,
            Kind kind,
            string userId)
        {
            var establishment = new Establishment
            {
                Name = name,
                Address = address,
                Image = newFileName,
                KindId = kind.Id,
                AdminId = userId
            };
            await this.context.AddAsync(establishment);
            await this.context.SaveChangesAsync();
            return establishment;
        }

        public async Task<Establishment> GetEstablishmentById(int Id)
        {
            return await context.Establishments.FindAsync(Id);
        }


        public async Task<Establishment> FindEstablishmentByNameAndAddress(string name, int addressId)
        {
            var est = await this.context.Establishments.
                FirstOrDefaultAsync(e => e.Name == name && e.AddressId == addressId);
            return est;
        }

        private async Task<Establishment> SetLogbooksOfEstablishment(Establishment establishment)
        {
            if (establishment != null)
            {
                var logbooks = await this.context.Logbooks
                                .Where(l => l.EstablishmentId == establishment.Id && l.IsDeleted!=true)
                                .ToListAsync();
                var logbooksWithManagers = new List<Logbook>();
                foreach (var lb in logbooks)
                {
                    var setLogbook = await this.logbookService.SetManagersOfLogbook(lb);
                    logbooksWithManagers.Add(setLogbook);
                }
                establishment.Logbooks = logbooksWithManagers;
            }
            return establishment;
        }

        public async Task<Establishment> SetAllPropsOfEstablishment(int establishmentId)
        {
            var establishment =await this.SetKindAndAddressOfEstablishment(establishmentId);
            establishment = await this.SetLogbooksOfEstablishment(establishment);
            return establishment;
        }

        public async Task<Establishment> SetKindAndAddressOfEstablishment(int establishmentId)
        {
            var establishment = await this.context.Establishments
                .Include(e => e.Address)
                .ThenInclude(a => a.City)
                .Include(e => e.Kind)
                .FirstOrDefaultAsync(e => e.Id == establishmentId);
            return establishment;
        }

        public async Task<Establishment> UpdateEstablishment(Establishment establishment)
        {
            this.context.Establishments.Update(establishment);
            await this.context.SaveChangesAsync();
            return establishment;
        }

        public async Task<Tuple<List<ClientComment>,int>> GetCommentsOnPageOfEstablishment(int establishmentId, int page,int onPage)
        {
            var comments = this.context.ClientComments
                .Include(c => c.Moderation)
                .ThenInclude(m => m.Author)
                .Include(c => c.Moderation)
                .ThenInclude(m => m.ModerationReason)
                .Where(c => c.EstablishmentId == establishmentId);
            var count = await comments.CountAsync();
            var commentsOnPage=await comments
                .OrderByDescending(c=>c.DateCreated)
                .Skip(page*onPage)
                .Take(onPage)
                .ToListAsync();
            return new Tuple<List<ClientComment>, int>(commentsOnPage, count);
        }

    }
}
