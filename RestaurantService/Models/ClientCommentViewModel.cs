﻿using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Models
{
    public class ClientCommentViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Text { get; set; }
        public string Author { get; set; }
        public List<string> CommentWords { get; set; }
        public DateTime Date { get; set; }
        public int EstablishmentId { get; set; }
        public Moderation Moderation { get; set; }
        public string ModerationReason { get; set; }
        public string ModerationAuthor { get; set; }
        public DateTime ModerationDate { get; set; }
        public int Page { get; set; }
        //public string IsModerated { get; set; }
    }
}
