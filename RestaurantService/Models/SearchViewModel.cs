﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Models
{
    public class SearchViewModel
    {
        public List<string> AllKinds { get; set; }
        public List<string> Towns { get; set; }
        public List<string> Kinds { get; set; }
        public string Pattern { get; set; }
        public string IsChanged { get; set; }
        public string SearchAction { get; set; }
    }
}
