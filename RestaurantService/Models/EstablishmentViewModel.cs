﻿using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Models
{
    public class EstablishmentViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Kind { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public Address Address { get; set; }
        public string Image { get; set; }
        //public string AdminId { get; set; }

        public List<ClientCommentViewModel> ClientComments { get; set; }
        public PagingViewModel PagingModel { get; set; }

        public string Href { get; set; }
    }
}
