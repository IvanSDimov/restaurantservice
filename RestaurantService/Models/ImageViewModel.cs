﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Models
{
    public class ImageViewModel
    {
        public int Id { get; set; }
        public string Image { get; set; }

        public string Default { get; set; }
        public string StyleClass { get; set; }

        public IFormFile FormImage { get; set; }
        public byte[] ByteImage { get; set; }
    }
}
