﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Models
{
    public class CustomErrorViewModel
    {
        public string ExceptionMessage { get; set; }
        public string ExceptionCode { get; set; }

        public CustomErrorViewModel()
        {
            ExceptionCode ="500";
        }
    }
}
