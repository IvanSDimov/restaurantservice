﻿using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Models
{
    public class AllEstablishmentsViewModels
    {
       public ICollection<EstablishmentViewModel> establishmentViewModels { get; set; }
        public PagingViewModel PagingModel { get; set; }
        //public SearchViewModel Search { get; set; }
        public string Href { get; set; }
    }
}
