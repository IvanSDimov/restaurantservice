﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Models
{
    public class CommentSearchViewModel
    {
        public string LogbookId { get; set; }
        public List<string> Categories { get; set; }
        public List<string> Managers { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
