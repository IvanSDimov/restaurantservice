﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using RestaurantService.Data.Models;
using RestaurantService.Extensions;
using RestaurantService.Models;
using RestaurantService.Services.Contracts;

namespace RestaurantService.Controllers
{
    public class HomeController : Controller
    {
        private const int onPage = 8;

        private IMemoryCache cache;
        private readonly IServiceProvider serviceProvider;
        private readonly IEstablishmentService establishmentService;
        private readonly IKindService kindService;
        private readonly ICityService cityService;
        private readonly IMapper<AllEstablishmentsViewModels, List<Establishment>> allEstablishmentsMapper;
        private readonly UserManager<ApplicationUser> userManager;


        public HomeController(
            IMemoryCache cache,
            IEstablishmentService establishmentService,
            IKindService kindService,
            ICityService cityService,
            IServiceProvider serviceProvider,
            IMapper<AllEstablishmentsViewModels, List<Establishment>> allEstablishmentsMapper,
            UserManager<ApplicationUser> userManager)
        {
            this.cache = cache;
            this.establishmentService = establishmentService;
            this.kindService = kindService;
            this.serviceProvider = serviceProvider;
            this.allEstablishmentsMapper = allEstablishmentsMapper;
            this.cityService = cityService;
            this.userManager = userManager;
        }
        public async Task<IActionResult> Index(int page=1)
        {
             var kinds= await kindService.ListKinds();
             var towns= await cityService.ListCities();
            cache.Set("Kinds", kinds);
            cache.Set("Towns", towns);
            ViewBag.MenuBar = "SearchEsrablishment";
            ViewBag.Kinds = kinds;
            ViewBag.Cities = towns;
            if (User.IsInRole("Admin"))return RedirectToAction("Index","Home", new {area="Admin"});
            if (User.IsInRole("Manager")) return RedirectToAction("Index", "Home", new { area = "Manager" });
            var establishments = await establishmentService.GetEstablishmentsOnPage(onPage, page - 1);
            var pages = await establishmentService.GetPages(onPage);
            var estModel = allEstablishmentsMapper.Map(establishments.ToList());
            estModel.Href = "/Establishment/Details/";
            estModel.PagingModel = new PagingViewModel()
            {
                CurrPage = page,
                Pages = pages,
                Area = "",
                Controller = "Home",
                Action = "Index"
            };
            
            return View(estModel);
        }


        public async Task<IActionResult> Establishments(SearchViewModel model, int page = 1)
        {
            ViewData["Message"] = "Your contact page.";
            bool isInitial = false;
            ViewBag.MenuBar = "SearchEsrablishment";
            if (model.IsChanged=="changed")
            {
                if (string.IsNullOrEmpty(HttpContext.Session.GetString("Session")))
                {
                    var uniqueId = new Guid();
                    HttpContext.Session.SetString("Session", uniqueId.ToString());
                }
                cache.Set(HttpContext.Session.GetString("Session"), model);//"SearchModel"
                isInitial = true;
            }
            if (string.IsNullOrEmpty(HttpContext.Session.GetString("Session")))
            {
                var uniqueId = new Guid();
                HttpContext.Session.SetString("Session", uniqueId.ToString());
                cache.Set(HttpContext.Session.GetString("Session"), model);
            }
            model = cache.Get<SearchViewModel>(HttpContext.Session.GetString("Session"));
            ViewBag.MenuBar = "SearchEsrablishment";
            //var test= await establishmentService.SearchEstablishments(new List<string> { "hotel" }, new List<string> { "Pleven" }, "h", onPage, id-1);
            if (model.Kinds == null) model.Kinds = new List<string>();
            if (model.Towns == null) model.Towns = new List<string>();
            var found = await establishmentService.SearchEstablishments(onPage, page - 1,
                        model.Kinds.Select(int.Parse).ToList(), 
                        model.Towns.Select(int.Parse).ToList(), 
                        model.Pattern,
                        userManager.GetUserId(User));
            int pages = 0;
            var estModel = allEstablishmentsMapper.Map(found.Item1.ToList());
            if (User.IsInRole("Admin")) estModel.Href = "/Admin/CreateEstablishment/Created/";            
            else estModel.Href = "/Establishment/Details/";
            if (found.Item1.Count > 0)
            {
                pages = await establishmentService.GetPages(onPage, found.Item2); 
            }
            estModel.PagingModel = new PagingViewModel()
            {
                CurrPage = page,
                Pages = pages,
                Area = "",
                Controller = "Home",
                Action = "Establishments"
            };
            //estModel.Search = model;
            //model.SearchAction = "/Home/Establishments";
            if (!isInitial)
            {
                ViewBag.Kinds = cache.Get("Kinds");
                ViewBag.Cities = cache.Get("Towns");

                return View("_EstablishmentsPartial", estModel);
            }
            return PartialView("_EstablishmentsPartial", estModel);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
