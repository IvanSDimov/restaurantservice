﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using RestaurantService.Models;

namespace RestaurantService.Controllers
{
    public class ErrorController : Controller
    {
        [Route("Error/{statusCode}")]
        [AllowAnonymous]
        public IActionResult HandleErrorCode(int statusCode)
        {
            var statusCodeData = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            var exceptionFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            switch (statusCode)
            {
                case 404:
                    return View("_CustomError", new CustomErrorViewModel { ExceptionMessage = "Page Not Found",ExceptionCode="404" });
                case 500:
                    return View("_CustomError", new CustomErrorViewModel { ExceptionMessage = "Something Went Wrong"});
                default:
                    return View("_CustomError", new CustomErrorViewModel { ExceptionMessage = "Something Went Wrong" });
            }
        }
    }
}
