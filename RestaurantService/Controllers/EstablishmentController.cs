﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RestaurantService.Data.Models;
using RestaurantService.Extensions;
using RestaurantService.Models;
using RestaurantService.Services.Contracts;

namespace RestaurantService.Controllers
{
    public class EstablishmentController : Controller
    {
        private readonly IEstablishmentService establishmentService;
        private readonly IMapper<EstablishmentViewModel, Establishment> establishmentMapper;
        private readonly IClientCommentService commentService;
        private readonly IModerationService moderationService;
        private readonly UserManager<ApplicationUser> userManager;
        private int onPage = 2;

        public EstablishmentController(IEstablishmentService establishmentService, 
            IMapper<EstablishmentViewModel, Establishment> establishmentMapper,
            IClientCommentService commentService,
            IModerationService moderationService,
            UserManager<ApplicationUser> userManager)
        {
            this.establishmentService = establishmentService;
            this.establishmentMapper = establishmentMapper;
            this.commentService = commentService;
            this.moderationService = moderationService;
            this.userManager = userManager;
        }

        public async Task<IActionResult> Details(int id, int page=1)
        {
            ViewData["Message"] = "Establishment Details";
            if (User.IsInRole("Moderator")) onPage = 4;
            ViewBag.ModerationReasons = await this.moderationService.ListModerationReasons();
            var establishment = await this.establishmentService.SetKindAndAddressOfEstablishment(id);
            var commentsOnPage_count= await this.establishmentService.GetCommentsOnPageOfEstablishment(id, page - 1, onPage);
            establishment.ClientComments = commentsOnPage_count.Item1;
            var establishmentModel = establishmentMapper.Map(establishment);
            foreach (var item in establishmentModel.ClientComments)
            {
                item.Page = page;
            }

            int pages = 0;
            if (User.IsInRole("Admin")) establishmentModel.Href = "/Admin/CreateEstablishment/Created/";
            else establishmentModel.Href = "/Establishment/Details/";
            if (commentsOnPage_count.Item1.Count > 0)
            {
                pages = await establishmentService.GetPages(onPage, commentsOnPage_count.Item2);
            }
            establishmentModel.PagingModel = new PagingViewModel()
            {
                CurrPage = page,
                Pages = pages,
                Area = "",
                Controller = "Establishment",
                Action = "Details"
            };

            return View(establishmentModel);
        }

    }
}