﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using RestaurantService.Areas.Manager.Models;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services.Contracts;

namespace RestaurantService.Controllers
{
    public class SearchController : Controller
    {
        private IMemoryCache _cache;


        public SearchController(IMemoryCache cache)
        {
            _cache = cache;
        }

        [HttpGet]
        public IActionResult EstablishmentSearch()
        {
            _cache.Remove(HttpContext.Session.GetString("Session"));
            return View();
        }

    }
}