﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services.Contracts;

namespace RestaurantService.Controllers
{
    public class CommentController : Controller
    {
        private readonly IEstablishmentService establishmentService;
        private readonly IMapper<EstablishmentViewModel, Establishment> establishmentMapper;
        private readonly IClientCommentService commentService;
        private readonly IModerationService moderationService;
        private readonly UserManager<ApplicationUser> userManager;

        public CommentController(IEstablishmentService establishmentService,
            IMapper<EstablishmentViewModel, Establishment> establishmentMapper,
            IClientCommentService commentService,
            IModerationService moderationService,
            UserManager<ApplicationUser> userManager)
        {
            this.establishmentService = establishmentService;
            this.establishmentMapper = establishmentMapper;
            this.commentService = commentService;
            this.moderationService = moderationService;
            this.userManager = userManager;
        }

        public async Task<IActionResult> SaveComment(ClientCommentViewModel model)
        {
            ClientComment comment = null;
            if (string.IsNullOrEmpty(model.Author)) model.Author = "Anonymous";
            try
            {
                if (!string.IsNullOrEmpty(model.Text))
                {
                    var totalWords = model.Text.Split(' ', ',', '.').Select(w => w.Replace(" ", "")
                                                                                  .Replace("!", "")
                                                                                  .Replace("?", "")
                                                                                  .Replace(",", "")
                                                                                  .Replace(";", "")
                                                                                  .Replace(".", ""))
                                                                                  .ToList();
                    var uniqueWords = new HashSet<string>(totalWords);
                    model.CommentWords = uniqueWords.ToList();
                    model.Date = DateTime.Now;
                    var swearWords = await this.commentService.CheckForSwearWords(model.CommentWords);
                    var swearAll = totalWords.Where(w => swearWords.Contains(w.ToLower())).Count();
                    if (swearWords.Count > 0)
                    {
                        foreach (var word in model.CommentWords)
                        {
                            if (swearWords.Contains(word.ToLower()))
                            {
                                model.Text = (model.Text).Replace(word, "***");
                            }
                        }
                    }
                    if (swearAll > totalWords.Count / 5)
                    {
                        model.Text = "";
                        model.ModerationReason = "inappropriate language";
                        model.ModerationDate = DateTime.Now;
                    }
                    else
                    {
                        if (swearWords.Count > 0)
                        {
                            var reason = await this.moderationService.GetReasonByName("inappropriate language");
                            var moderation = await this.moderationService.CreateModeration(reason.Id, null);
                            model.Moderation = moderation;
                            model.ModerationReason = "inappropriate language";
                            model.ModerationDate = DateTime.Now;
                        }
                        comment = await this.commentService.AddComment(model.EstablishmentId, model.Text, model.Author, model.Moderation);
                    }
                }
                return PartialView("_ClientCommentPartial", model);
            }
            catch (ArgumentException ex)
            {
                return PartialView("_CustomError", new CustomErrorViewModel { ExceptionMessage = ex.Message });
            }
        }



        [HttpPost]
        public async Task<IActionResult> EditComment(string text, int id, int moderation, int page)
        {
            var reason = await this.moderationService.GetReasonById(moderation);
            var moderationEntity = await this.moderationService.CreateModeration(reason?.Id, userManager.GetUserId(User));
            var comment = await this.commentService.UpdateComment(id,text,moderationEntity?.Id);
            return RedirectToAction("Details", "Establishment", new {id= comment.EstablishmentId, page=page});
        }

        public IActionResult InvalidComment(ClientCommentViewModel model)
        {
            return PartialView("_LeaveClientCommentPartial", model);
        }
    }
}