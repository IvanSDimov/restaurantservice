﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using RestaurantService.Areas.Manager.Models;
using RestaurantService.Models;
using RestaurantService.Models.AccountViewModels;

namespace RestaurantService.Controllers
{
    public class InputController : Controller
    {
        private IMemoryCache _cache;
        private readonly UserManager<ApplicationUser> _userManager;

        public InputController(IMemoryCache cache, UserManager<ApplicationUser> userManager)
        {
            _cache = cache;
            _userManager = userManager;
        }

        [HttpPost("UploadImage")]
        public async Task<IActionResult> UploadImage(ImageViewModel imagemodel)
        {
            var parentmodel = _cache.Get<IImigable>(_userManager.GetUserName(User));
            imagemodel.StyleClass = parentmodel.ImageModel.StyleClass;
            if (imagemodel.FormImage != null && Path.GetExtension(imagemodel.FormImage.FileName).ToLower() == ".jpg")
            {

                using (var memoryStream = new MemoryStream())
                {
                    if (imagemodel.FormImage != null)
                    {
                        await imagemodel.FormImage.CopyToAsync(memoryStream);
                        imagemodel.ByteImage = memoryStream.ToArray();
                    }
                }
            }
            parentmodel.ImageModel = imagemodel;
            _cache.Set(_userManager.GetUserName(User), parentmodel);
            return PartialView("_UploadImagePartial", imagemodel);
        }
    }
}