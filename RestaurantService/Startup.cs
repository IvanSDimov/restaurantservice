﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RestaurantService.Data;
using RestaurantService.Models;
using RestaurantService.Services;
using RestaurantService.Areas.Admin.Models;
using RestaurantService.Areas.Admin.Mappers;
using RestaurantService.Services.Contracts;
using RestaurantService.Data.Models;
using RestaurantService.Areas.Manager.Models;
using RestaurantService.Areas.Manager.Mappers;
using RestaurantService.Mappers;
using RestaurantService.Models.AccountViewModels;
using RestaurantService.Hubs;

namespace RestaurantService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IEstablishmentService, EstablishmentService>();
            services.AddScoped<IJsonService, JsonService>();
            services.AddScoped<IManagerCommentService, ManagerCommentService>();
            services.AddScoped<ILogbookService, LogbookService>();
            services.AddScoped<IKindService, KindService>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IClientCommentService, ClientCommentService>();
            services.AddScoped<IModerationService, ModerationService>();

            services.AddSingleton<IMapper<AllUsersViewModel, IList<ApplicationUser>>, AllUsersMapper>();
            services.AddSingleton<IMapper<ManagerViewModel, ApplicationUser>, ManagerMapper>();
            services.AddSingleton<IMapper<UserViewModel, ApplicationUser>, UserMapper>();
            services.AddSingleton<IMapper<EstablishmentViewModel, Establishment>, EstablishmentMapper>();
            services.AddSingleton<IMapper<AllEstablishmentsViewModels, List<Establishment>>, AllEstablishmentsMapper>();
            services.AddSingleton<IMapper<LogbookViewModel, Logbook>, LogbookMapper>();
            services.AddSingleton<IMapper<AllLogbookViewModel, List<Logbook>>, AllLogbookMapper>();
            services.AddSingleton<IMapper<ManagerCommentViewModel, ManagerComment>, ManagerCommentMapper>();
            services.AddSingleton<IMapper<AllManagerCommentsViewModel, List<ManagerComment>>, AllManagerCommentsMapper>();
            services.AddSingleton<IMapper<EstablishmentDetailsViewModel, Establishment>, EstablishmentDetailsMapper>();
            services.AddSingleton<IMapper<CreateLogbookViewModel, Logbook>, CreateLogbookMapper>();
            services.AddSingleton<IMapper<CreateEstablishmentViewModel, Establishment>, CreateEstablishmentMapper>();
            services.AddSingleton<IMapper<CreateUserViewModel, ApplicationUser>,CreateUserMapper>();
            services.AddSingleton<IMapper<LogbookViewModelForManager, Logbook>, LogbookForManagerMapper>();
            services.AddSingleton<IMapper<ClientCommentViewModel, ClientComment>, ClientCommentMapper>();

            //!!!!!!!!!!!!!!!!!!!!!!!!
            services.AddDistributedMemoryCache();
            
            services.AddSession(options =>
            {
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });
            //!!!!!!!!!!!!!!!!!!!!!!!!

            services.AddMvc().SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_2_2);

            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
                app.UseStatusCodePagesWithReExecute("/Error/{0}");
                app.UseExceptionHandler("/Error/{0}");
            }
            else
            {
                app.UseStatusCodePagesWithReExecute("/Error/{0}");
                app.UseExceptionHandler("/Error/{0}");
            }

            app.UseStaticFiles();

            //!!!!!!!!!!!!!!!!!!!!
            app.UseSession();
            //!!!!!!!!!!!!!!!!!!!!

            app.UseAuthentication();

            app.UseSignalR(routes =>
            {
                routes.MapHub<ApplicationHub>("/ApplicationHub");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areas",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");



                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
