﻿
$("#uploadForm").submit(function (ev) {
    ev.preventDefault();
    var $this = $(this);

    var formAction = $(this).attr("action");
    var fdata = new FormData();

    var fileInput = $('#ImageModel_FormImage')[0];
    var file = fileInput.files[0];
    fdata.append(fileInput.name, file);
    $.ajax({
        type: 'post',
        url: formAction,
        data: fdata,
        processData: false,
        contentType: false
    }).done(function (result) {
       $("#partial").html(result)
    });

});

$("#establishment-search").submit(function (ev) {
            caches.delete("SearchModel")
            ev.preventDefault();
            var $this = $(this);
            var dataToSend = $('#establishment-search').find('.inputToSelect').serialize();

            $.ajax({
                type: 'get',
                url:'/Home/Establishments',
                data: dataToSend,
                processData: false,
                contentType: false
            }).done(function (result) {
                $("#theBody").empty();
                $("#theBody").append(result);
            });
});

$("#managerCommnet-search").submit(function (ev) {
    caches.delete("CommentSerchModel")
    ev.preventDefault();
    var $this = $(this);
    var dataToSend = $('#managerCommnet-search').find('.inputToSelect').serialize();
    console.log(dataToSend);
    $.ajax({
        type: 'get',
        url: '/Manager/Logbook/LogbookCommets',
        data: dataToSend,
        processData: false,
        contentType: false
    }).done(function (result) {
        $("#theBody").empty();
        $("#theBody").append(result);
        $('.user-multiselect').multiselect({
            includeSelectAllOption: true,
        });
        
    });
});

$("#client-comment-form").submit(function (ev) {
    ev.preventDefault();
    var $this = $(this);
    var dataToSend = $('#client-comment-form').find('.client-comment-input').serialize();
    
    var commentText = $('#client-comment-text').val();
    console.log($('#client-comment-text'));
    console.log(commentText);
    if (commentText!= undefined && commentText!= null && commentText.length > 3) {
        $.ajax({
            type: 'get',
            url: '/Comment/SaveComment',
            data: dataToSend,
            processData: false,
            contentType: false
        }).done(function (result) {
            console.log(result);
            var currUrl1 = window.location.href;
            if (!currUrl1.includes("="))
            {
                var currUrl = currUrl1;
            }
            else
            {
                var index = currUrl1.indexOf("=");
                currUrl = currUrl1.substring(0, index+1) + "1";
                console.log(index);
                console.log(currUrl1);
                console.log(currUrl);
            }
            //var currUrl = currUrl1.substring(currUrl1.length - 2, currUrl1.length);
            var div1 = $(result).filter('.card.px-3.align-left.col-12')
            if (div1.length > 0) {
                div1 = $(result).filter('.card.px-3.align-left.col-12')[0];
                div1 = $(div1).find('.panel-item.p-4.d-flex.align-items-start')[0];
                div1 = $(div1).find('.card-text')[0];
                div1 = $(div1).find('.client-comment-returned-text')[0];
                div1 = $(div1).find('.moderation-edit-paragraph')[0];
                div1 = $(div1).find('p.mbr-content-text.mbr-fonts-style.display-7')[0];
                    var commentToSave = div1.innerHTML.trim();
                //    if (currUrl === "=1" || !currUrl1.includes("=")) {
                //    $("#client-comment-display").prepend(result);
                //    $("#client-comment-form-display").load(self);
                //}

                if (commentToSave.length < 3) {
                    alert('Your comment has been moderated for inappropriate language!');
                } else if (commentToSave != commentText.trim()) {
                    alert('Your comment has been moderated for inappropriate language: "' + commentToSave + '"!');
                } else
                    alert('Thank you for your comment: "' + commentToSave + '"!');
                window.location.href = currUrl;
            } else {
                $("#body-1").empty();
                $("#body-1").append(result);
            }
            
        });
    }
    else
    {
        alert('Invalid comment!');
    }
});


$(document).ready(function () {
    $('.user-multiselect').multiselect({
        includeSelectAllOption: true,
    });
    $('#multiple-checkboxes1').multiselect({
        includeSelectAllOption: true,
    });
    $('#multiple-checkboxes2').multiselect({
        includeSelectAllOption: true,
    });
    $('#multiple-checkboxes3').multiselect({
        includeSelectAllOption: false,
    });
    $('#multiple-checkboxes4').multiselect({
        includeSelectAllOption: true,
    });
    $('#multiple-checkboxes5').multiselect({
        includeSelectAllOption: false,
    });
    $('#multiple-checkboxes8').multiselect({
        includeSelectAllOption: true,
    });
    $('#multiple-checkboxes6').multiselect({
        includeSelectAllOption: true,
    });
    $('#multiple-checkboxes7').multiselect({
        includeSelectAllOption: true,
    });
    $('#search-list').css({
        opacity: 1
    });
});

$('#add-managers-button').click(function (ev) {
    var dataToSend = $('#add-managers-data-to-send').find('.inputToSelect').serialize();
    $.ajax({
        type: 'get',
        url: '/Admin/CreateEstablishment/AddManagersToLogbooks',
        data: dataToSend,
        processData: false,
        contentType: false
    }).done(function (result) {
        $("#add-managers-div").empty();
        $("#add-managers-div").append(result);
            var multies = $('.user-multiselect');

        multies.each(function () {
            $(this).multiselect({
                includeSelectAllOption: true,
            });
        });
    });
})


$('.moderation-edit-link').click(function (ev) {
    var myDiv = $(ev.delegateTarget).parent().parent().parent().parent().parent().find('.moderation-edit-paragraph');
    var myP = myDiv.find('p');
    var mySpan = myDiv.find('span');
    var myText = myP[0].innerHTML;
    var myId = mySpan.innerHTML;
    var parentDiv = myDiv.parent();
    var formDiv = parentDiv.parent().find('.moderation-edit-form');
    console.log(myDiv);
    console.log(formDiv);
    $(myDiv).css({
        display: 'none'
    });
    $(this).css({
        display: 'none'
    });
    $(formDiv).css({
        display: 'block',
    });
    var textArea = formDiv.find('textarea');
    textArea.val(myText.trim());
});


$('.btn.btn-primary.edit-comment-button').click(function (ev) {
  
    var commentDIV = $(this).parent().parent().parent().parent().parent();
    var parentDIV = commentDIV.parent();
    var formDIV = parentDIV.find('.comment-edit-form');
    $(commentDIV).css({
        display: 'none'
    });
    $(formDIV).css({
        display: 'block',
    });
});



$('#create-establishment-form').submit(function (ev) {
    ev.preventDefault()
    var dataToSend = $('#add-managers-div').find('.user-multiselect');
    

    var logbookViewModels = [];
    for (var i = 0; i < dataToSend.length; i++) {
        var currId = '#' + dataToSend[i].id;
        var selected = $(currId).val();
        var logbookViewModel = {
            name: dataToSend[i].id,
            managers: selected
        };
        logbookViewModels.push(logbookViewModel);
    }


    dataToSend = $('input.existing-logbooks[type="checkbox"]');

    var logbooksToDelete = [];
    for (var i = 0; i < dataToSend.length; i++) {
        if (dataToSend[i].checked === false)
        {
            logbooksToDelete.push(dataToSend[i].value);
        }
    }

    dataToSend = $('input.deleted-logbooks[type="checkbox"]');

    var logbooksToUndelete = [];
    for (var i = 0; i < dataToSend.length; i++) {
        if (dataToSend[i].checked === true) {
            logbooksToUndelete.push(dataToSend[i].value);
        }
    }

    var establishmentName = $('#establishment-name').val();
    var establishmentCity = $('#multiple-checkboxes5').val();
    var establishmentStreet = $('#establishment-street').val();
    var establishmentKind = $('#multiple-checkboxes3').val();
    var createupdate = $('#create-update').val();
    var logbookNames = $('#logbooks-to-send').val();


    var requestData = {
        name: establishmentName,
        city: establishmentCity,
        street: establishmentStreet,
        kind: establishmentKind,
        logbookViewModels: logbookViewModels,
        createOrUpdate: createupdate,
        deletedLogbooks: logbooksToDelete,
        logbooksToUndelete: logbooksToUndelete,
        logbookNames: logbookNames
    };

    var sourceUrl;
    if (createupdate === "update")
    {
        sourceUrl = '/Admin/CreateEstablishment/UpdateEstablishment';
    }
    else
    {
        sourceUrl = '/Admin/CreateEstablishment/CreateEstablishment';
    }


    $.ajax({
        type: 'POST',
        url: sourceUrl,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(requestData),
        success: function (result) {
            console.log(result.Url);
            console.log(result);
            window.location.href = result.url;
        },
        error: function (p1, p2, p3) {
            console.log(p1);
            console.log(p2);
            console.log(p3);
        }
    })
})

$('.form-check-input').click(function (ev) {
    var state = $(ev.delegateTarget)[0].checked;
    var commentID = $(ev.delegateTarget).parent().parent().find('.comment-id').val();
    console.log(state);
    var dataToSend = {
        State: state,
        CommentId: commentID
    };

    $.ajax({
        type: 'post',
        url: '/Logbook/StateEdit',
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(dataToSend),
        processData: false,
        success: function (result) {
            console.log(result);
        },
    });

})

// hasClass
function hasClass(elem, className) {
    return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}

// toggleClass
function toggleClass(elem, className) {
    var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(' ' + className + ' ') >= 0) {
            newClass = newClass.replace(' ' + className + ' ', ' ');
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    } else {
        elem.className += ' ' + className;
    }
}

var mobile = document.createElement('div');
mobile.className = 'nav-mobile';
document.querySelector('.nav').appendChild(mobile);

var mobileNav = document.querySelector('.nav-mobile');
var toggle = document.querySelector('.nav-list');
mobileNav.onclick = function () {
    toggleClass(this, 'nav-mobile-open');
    toggleClass(toggle, 'nav-active');
}