﻿using RestaurantService.Areas.Admin.Models;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using RestaurantService.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Extensions
{
    public static class ServiceExtensions
    {
        public static async Task<int> GetPages(this IService service, int onPage, int count=0)
        {
            if (count==0) count =await service.GetCount();
            var pages = count / onPage;
            if (pages * onPage < count) pages++;
            return pages;
        }

    }
}
