﻿using Microsoft.AspNetCore.Hosting;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Extensions
{
    public static class SaveImage
    {
        public static void SaveByteImageInWwwroot(this ImageViewModel imgModel,
            string defaultImage,
            string path,
            ref string newFileName,
            IHostingEnvironment _hostingEnvironment)
        {
            if (imgModel.FormImage != null)
            {
                var fileName = Path.GetFileName(imgModel.FormImage.FileName);
                var fullName = newFileName + Path.GetExtension(fileName);
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, path);//"images/establishments/"
                var filePath = Path.Combine(uploads, fullName);
                using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(imgModel.ByteImage, 0, imgModel.ByteImage.Length);
                }
            }
            else newFileName = defaultImage;
        }
    }
}
