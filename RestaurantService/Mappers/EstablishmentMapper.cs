﻿
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Mappers
{
    public class EstablishmentMapper : IMapper<EstablishmentViewModel, Establishment>
       
    {
        private readonly IMapper<ClientCommentViewModel, ClientComment> commentMapper;

        public EstablishmentMapper(IMapper<ClientCommentViewModel, ClientComment> commentMapper)
        {
            this.commentMapper = commentMapper;
        }

        public EstablishmentViewModel Map(Establishment entity)
        {
            var model = new EstablishmentViewModel();
            model.Id = entity.Id;
            model.Name = entity.Name;
            model.Kind = entity.Kind?.Name;
            model.City = entity.Address?.City?.Name;
            model.Street = entity.Address?.Street;
            model.Address = entity.Address;
            model.Image = "../../../images/establishments/" + entity.Image + ".jpg";
            if (entity.ClientComments != null)
                model.ClientComments = entity.ClientComments.Select(this.commentMapper.Map).ToList();
                //AdminId=entity.AdminId
                

            return model;
        }
    }
}
