﻿
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Mappers
{
    public class AllEstablishmentsMapper : IMapper<AllEstablishmentsViewModels,List<Establishment>>
    {
        private IMapper<EstablishmentViewModel, Establishment> mapper;
        public AllEstablishmentsMapper(IMapper<EstablishmentViewModel, Establishment> mapper)
        {
            this.mapper = mapper;
        }

        public AllEstablishmentsViewModels Map(List<Establishment> entity)
        {
            return new AllEstablishmentsViewModels
            {
               establishmentViewModels = entity.Select(e => mapper.Map(e)).ToList()
            };
        }
    }
}
