﻿using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Mappers
{
    public class ClientCommentMapper : IMapper<ClientCommentViewModel, ClientComment>
    {
        public ClientCommentViewModel Map(ClientComment entity)
        {
            var model = new ClientCommentViewModel();
            model.Id=entity.Id;
            model.Text=entity.Text;
            model.Date=entity.DateCreated;
            model.Author=entity.Author;
            model.EstablishmentId = entity.EstablishmentId??0;
            model.ModerationReason = entity.Moderation?.ModerationReason?.Name;
            model.ModerationAuthor = entity.Moderation?.Author?.UserName;
            if(entity.Moderation!=null)
                model.ModerationDate = entity.Moderation.Date;
            //entity.Establishment?.Name;
            //entity.Establishment?.Address?.City?.Name;
            //entity.Establishment?.Address?.Street;
            //entity.Moderation?.ModerationReason?.Name;
            //entity.Moderation?.Autor?.UserName;
            //entity.Moderation?.Date;
            return model;
        }
    }
}
