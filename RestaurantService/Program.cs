﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RestaurantService.Data;
using RestaurantService.Data.Models;
using RestaurantService.Models;

namespace RestaurantService
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = BuildWebHost(args);

            await SeedDatabase(host);

            host.Run();
        }

        private static async Task SeedDatabase(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                var userManeger = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                var roleManeger = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                foreach (var item in new List<string> { "Admin", "Manager","Moderator" })
                {
                    if (!dbContext.Roles.Any(r => r.Name == item))
                        await roleManeger.CreateAsync(new IdentityRole { Name = item });
                }
                var adminRole = await dbContext.Roles.FirstOrDefaultAsync(r => r.Name == "Admin");
                if (!dbContext.UserRoles.Any(ur => ur.RoleId == adminRole.Id))
                {
                    var adminUser = new ApplicationUser { UserName = "Admin", Email = "admin@gmail.com" };

                    await userManeger.CreateAsync(adminUser, "Admin123@");

                    await userManeger.AddToRoleAsync(adminUser, "Admin");
                }
            }
        }
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
