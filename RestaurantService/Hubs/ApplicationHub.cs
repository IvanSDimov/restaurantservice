﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Hubs
{
    public class ApplicationHub : Hub
    {
        
        public async Task CommentAdded(int id, string CommentText)
        {
            await Clients.All.SendAsync("ReceiveComment", id, CommentText);
        }
    }
}
