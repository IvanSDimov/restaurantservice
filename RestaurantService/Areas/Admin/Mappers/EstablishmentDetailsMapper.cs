﻿using RestaurantService.Areas.Admin.Models;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Mappers
{
    public class EstablishmentDetailsMapper: IMapper<EstablishmentDetailsViewModel, Establishment>
    {
        private readonly IMapper<CreateLogbookViewModel, Logbook> _logbookMapper;

        public EstablishmentDetailsMapper(IMapper<CreateLogbookViewModel, Logbook> logbookMapper)
        {
            _logbookMapper = logbookMapper;
        }

        public EstablishmentDetailsViewModel Map(Establishment establishment)
        {
            var establishmentModel = new EstablishmentDetailsViewModel
            {
                Id = establishment.Id,
                Name = establishment.Name,
                Street = establishment.Address.Street,
                City=establishment.Address.City.Name,
                Kind=establishment.Kind.Name,
                Image= "../../../images/establishments/" + establishment.Image+ ".jpg",                
                Logbooks=establishment.Logbooks.Select(l => this._logbookMapper.Map(l)).ToList()

        };
            return establishmentModel;
        }
    }
}
