﻿using RestaurantService.Areas.Admin.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Mappers
{
    public class ManagerMapper : IMapper<ManagerViewModel, ApplicationUser>
    {
        public ManagerViewModel Map(ApplicationUser user)
        {
            var managerModel = new ManagerViewModel()
            {
                Id = user.Id,
                UserName = user.UserName,
                Avatar = "../../../users/managers/" + user.Avatar + ".jpg",
                Role = user.Role,
                IsDeleted = false,
                ManagerComments = user.ManagerComments,
                Establishment = user.Establishment
            };
            return managerModel;
        }
    }
}
