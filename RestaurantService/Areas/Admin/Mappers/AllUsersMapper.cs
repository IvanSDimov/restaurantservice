﻿using RestaurantService.Areas.Admin.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Mappers
{
    public class AllUsersMapper : IMapper<AllUsersViewModel, IList<ApplicationUser>>
    {
        private IMapper<UserViewModel, ApplicationUser> userMapper;

        public AllUsersMapper(IMapper<UserViewModel, ApplicationUser> userMapper)
        {
            this.userMapper = userMapper;
        }
        public AllUsersViewModel Map(IList<ApplicationUser> users)
        {
            return new AllUsersViewModel
            {
                UserModels = users.Select(u => userMapper.Map(u)).ToList(),
            };
        }
    }
}
