﻿using RestaurantService.Areas.Admin.Models;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Mappers
{
    public class CreateEstablishmentMapper : IMapper<CreateEstablishmentViewModel, Establishment>
    {
        private readonly IMapper<CreateLogbookViewModel, Logbook> _logbookMapper;

        public CreateEstablishmentMapper(IMapper<CreateLogbookViewModel, Logbook> logbookMapper)
        {
            _logbookMapper = logbookMapper;
        }


        public CreateEstablishmentViewModel Map(Establishment establishment)
        {
            var establishmentModel = new CreateEstablishmentViewModel
            {
                Id = establishment.Id,
                Name = establishment.Name,
                Street = establishment.Address.Street,
                City = establishment.Address.City.Name,
                Kind = establishment.Kind.Name,
                Address = establishment.Address,
                ImageModel = new ImageViewModel
                {
                    Image = "../../../images/establishments/" + establishment.Image + ".jpg",   
                },
                LogbookViewModels = establishment.Logbooks?.Select(lb => _logbookMapper.Map(lb)).ToList(),
                DeletedLogbookViewModels = establishment.DeletedLogbooks?.Select(lb => _logbookMapper.Map(lb)).ToList(),
                IsDeleted = establishment.IsDeleted
            };
            return establishmentModel;
        }
    }
}
