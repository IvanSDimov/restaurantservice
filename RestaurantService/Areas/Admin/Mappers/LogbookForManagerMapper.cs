﻿using RestaurantService.Areas.Admin.Models;
using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Mappers
{
    public class LogbookForManagerMapper : IMapper<LogbookViewModelForManager, Logbook>
    {
        public LogbookViewModelForManager Map(Logbook logbook)
        {
            var model = new LogbookViewModelForManager()
            {
                Id = logbook.Id,
                Name = logbook.Name,
                EstablishmentId = logbook.Establishment?.Id ?? 0
            };
            return model;
        }
    }
}
