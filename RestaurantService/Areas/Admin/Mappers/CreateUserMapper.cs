﻿using RestaurantService.Models;
using RestaurantService.Models.AccountViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Mappers
{
    public class CreateUserMapper : IMapper<CreateUserViewModel, ApplicationUser>
    {
        public CreateUserViewModel Map(ApplicationUser user)
        {
            var userModel = new CreateUserViewModel()
            {
                Id=user.Id,
                UserName = user.UserName,
                Email = user.Email,
                ImageModel=new ImageViewModel
                { 
                    Image = "../../../images/avatars/" + user.Avatar + ".jpg",
                    StyleClass= "border-radius:50%;width:300px;padding-top:10px;padding-bottom:10px;"
                },
                Role = user.Role,
            };
            return userModel;
        }
    }
}
