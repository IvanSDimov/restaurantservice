﻿using RestaurantService.Areas.Admin.Models;
using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Mappers
{
    public class CreateLogbookMapper : IMapper<CreateLogbookViewModel, Logbook>
    {
        public CreateLogbookViewModel Map(Logbook logbook)
        {
            var model = new CreateLogbookViewModel
            {
                Id = logbook.Id,
                Name = logbook.Name,
                Managers = logbook.Managers?.Select(u => u.UserName).ToList(),
                EntityUsers = logbook.Managers?.ToList(),
                EstablishmentName=logbook.Establishment?.Name,
                EstablishmentAddress=logbook.Establishment?.Address?.City?.Name+' '+ logbook.Establishment?.Address?.Street,
                EstablishmentId=logbook.Establishment?.Id ?? 0
            };
            return model;
        }
    }
}
