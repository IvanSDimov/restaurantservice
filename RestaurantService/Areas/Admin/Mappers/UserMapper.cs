﻿using RestaurantService.Areas.Admin.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Mappers
{
    public class UserMapper : IMapper<UserViewModel, ApplicationUser>
    {
        public UserViewModel Map(ApplicationUser user)
        {
            var userModel = new UserViewModel()
            {
                Id = user.Id,
                UserName = user.UserName,
                Email=user.Email,
                Avatar = "../../../images/avatars/" + user.Avatar + ".jpg",
                Role = user.Role,
                AdminId=user.AdminId,
                IsDeleted = user.IsDeleted
            };
            return userModel;
        }
    }
}
