﻿using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Models
{
    public class AllUsersViewModel
    {
        public int Pages { get; set; }
        public PagingViewModel PagingModel { get; set; }
        public List<string> Roles { get; set; }
        public ICollection<UserViewModel> UserModels { get; set; }
    }
}
