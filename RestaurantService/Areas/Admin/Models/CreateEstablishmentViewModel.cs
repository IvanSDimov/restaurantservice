﻿using RestaurantService.Areas.Manager.Models;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Models
{
    public class CreateEstablishmentViewModel : IImigable
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Address")]
        public Address Address { get; set; }

        [Required]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Street")]
        public string Street { get; set; }

        public ImageViewModel ImageModel { get; set; }

        public string Kind { get; set; }

        //public List<ApplicationUser> Managers { get; set; }

        public string LogbookNames { get; set; }

        //public List<string> Logbooks { get; set; }

        //public Dictionary<string, List<string>> LogbookUsers { get; set; }

        public List<CreateLogbookViewModel> LogbookViewModels { get; set; }
        public List<CreateLogbookViewModel> DeletedLogbookViewModels { get; set; }

        public bool IsDeleted { get; set; }

        public string CreateOrUpdate { get; set; }

        public List<string> DeletedLogbooks { get; set; }
        public List<string> LogbooksToUndelete { get; set; }
    }
}

