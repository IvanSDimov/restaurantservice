﻿using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Models
{
    public class ManagerViewModel:UserViewModel
    {
        public IList<ManagerComment> ManagerComments { get; set; }
        public IList<Establishment> Establishment { get; set; }
    }
}
