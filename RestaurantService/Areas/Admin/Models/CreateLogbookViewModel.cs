﻿using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Models
{
    public class CreateLogbookViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string EstablishmentName { get; set; }
        public string EstablishmentAddress { get; set; }
        public int EstablishmentId { get; set; }
        public List<string>Managers { get; set; }
        public List<ApplicationUser> EntityUsers { get; set; }
        public List<string>ManagersToLeave { get; set; }
    }
}
