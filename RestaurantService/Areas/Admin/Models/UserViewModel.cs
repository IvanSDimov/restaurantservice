﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Models
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
        public string Role { get; set; }
        public string AdminId { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsLastAdmin { get; set; }
        public Dictionary<Tuple<string, int>, List<LogbookViewModelForManager>> Logbooks { get; set; }
    }
}
