﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Models
{
    public class LogbookViewModelForManager
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int EstablishmentId { get; set; }
    }
}
