﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Admin.Models
{
    public class EstablishmentDetailsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Kind { get; set; }
        public string Image { get; set; }
        public List<CreateLogbookViewModel> Logbooks { get; set; }
        
    }
}
