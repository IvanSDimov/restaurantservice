﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using RestaurantService.Areas.Admin.Models;
using RestaurantService.Areas.Manager.Models;
using RestaurantService.Data.Models;
using RestaurantService.Extensions;
using RestaurantService.Models;
using RestaurantService.Services;
using RestaurantService.Services.Contracts;

namespace RestaurantService.Areas.Admin.Controllers
{
    public class CreateEstablishmentController : Controller
    {
        private const string defaultImage = "_default";

        private IMemoryCache _cache;
        private readonly IEstablishmentService _establishmentService;
        private readonly ILogbookService _logbookService;
        private readonly IKindService _kindService;
        private readonly IAddressService _addressService;
        private readonly ICityService _cityService;
        private readonly IUserService _userService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IMapper<EstablishmentDetailsViewModel, Establishment> _establishmentMapper;
        private readonly IMapper<CreateEstablishmentViewModel, Establishment> _createEstablishmentMapper;

        private readonly UserManager<ApplicationUser> _userManager;

        public CreateEstablishmentController(
            IMemoryCache cache,
            IEstablishmentService establishmentService,
            IHostingEnvironment hostingEnvironment,
            IMapper<EstablishmentDetailsViewModel, Establishment> establishmentMapper,
            IMapper<CreateEstablishmentViewModel, Establishment> createEstablishmentMapper,
            UserManager<ApplicationUser> userManager,
            ILogbookService logbookService,
            IKindService kindService,
            IAddressService addressService,
            ICityService cityService,
            IUserService userService
            )
        {
            _establishmentService = establishmentService;
            _cache = cache;
            _hostingEnvironment = hostingEnvironment;
            _establishmentMapper = establishmentMapper;
            _createEstablishmentMapper = createEstablishmentMapper;
            _userManager = userManager;
            _logbookService = logbookService;
            _kindService = kindService;
            _addressService = addressService;
            _cityService = cityService;
            _userService = userService;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [Area("Admin")]
        [HttpGet]
        [AllowAnonymous]
        public IActionResult CreateEstablishment(string returnUrl = null)
        {
            var model = new CreateEstablishmentViewModel()
            {
                ImageModel = new ImageViewModel()
                {
                    StyleClass = "width:300px;padding-top:10px;padding-bottom:10px;",
                    Default = "../../../images/establishments/_default.jpg"
                }
            };
            model.CreateOrUpdate = "create";
            _cache.Set(_userManager.GetUserName(User), model);
            ViewData["ReturnUrl"] = returnUrl;

            var kinds = _cache.Get("Kinds");
            var towns = _cache.Get("Towns");


            ViewBag.Kinds = kinds;
            ViewBag.Towns = towns;

            var cacheModel = _cache.Get<CreateEstablishmentViewModel>(_userManager.GetUserName(User));
            return View(cacheModel);
        }

        [Area("Admin")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> CreateEstablishment([FromBody] CreateEstablishmentViewModel model)
        {
            var cacheModel = _cache.Get<CreateEstablishmentViewModel>(_userManager.GetUserName(User));
            var imgModel = cacheModel.ImageModel;
            model.ImageModel = imgModel;
            Establishment establishment = null;
            if (!string.IsNullOrEmpty(model.Name)
                && !string.IsNullOrEmpty(model.City)
                && !string.IsNullOrEmpty(model.Street))
            {
                var address = await _addressService.FindAddress(model.City, model.Street);
                if(address==null) address = await _addressService.CreateAddress(model.City, model.Street);
                model.Address = address;
                establishment = await _establishmentService.FindEstablishmentByNameAndAddress(model.Name, 
                                                                                               address.Id);
            }
            var kind= await _kindService.GetKindByName(model.Kind);
            if (!string.IsNullOrEmpty(model.Name)
                && model.Address != null
                && (establishment == null || establishment.IsDeleted))
            {
                var newFileName = model.Name.Replace(" ","")+'_'+model.City.Replace(" ", "") + '_'+model.Street.Replace(" ", "");
                imgModel.SaveByteImageInWwwroot(defaultImage,
                                      "images/establishments/",
                                       ref newFileName,
                                      _hostingEnvironment);
                string webRootPath = _hostingEnvironment.WebRootPath;
                if (!System.IO.File.Exists(webRootPath + "/images/establishments/" + newFileName + ".jpg")) newFileName = defaultImage;
                if (establishment != null && establishment.IsDeleted)
                    {
                        establishment.Address = model.Address;
                        establishment.Name = model.Name;
                        establishment.IsDeleted = false;
                        establishment.Image = newFileName;
                        establishment.Kind = kind;
                        establishment.AdminId = _userManager.GetUserId(User);
                    }
                    else
                    {
                        establishment = await _establishmentService.CreateEstablishment(model.Name,
                            model.Address,
                            newFileName,
                            kind,
                            _userManager.GetUserId(User));
                            await _establishmentService.UpdateEstablishment(establishment);
                }
                foreach (var lb in model.LogbookViewModels)
                {
                    var existing =await this._logbookService.UpdateLogbook(lb.Name, establishment.Id, lb.Managers);
                    if(existing==null)
                        await this._logbookService.CreateLogbookUsers(lb.Name, establishment.Id, lb.Managers);
                }
                if ((model.LogbookViewModels == null
                    || model.LogbookViewModels.Count == 0)
                    && !string.IsNullOrEmpty(model.LogbookNames))
                {
                    var toCreate = model.LogbookNames.Split(',').Select(s => s.Trim());
                    foreach (var lb in toCreate)
                    {
                        var existing = await this._logbookService.UpdateLogbook(lb, establishment.Id, null);
                        if (existing == null)
                            await this._logbookService.CreateLogbookUsers(lb, establishment.Id, null);
                    }
                }

                if (establishment!=null&&establishment.IsDeleted!=true)
                {
                    model.Id = establishment.Id;
                    _cache.Set(_userManager.GetUserName(User), model);
                    var url = "/Admin/CreateEstablishment/Created/" + establishment.Id.ToString();
                    return Json(new { Url = url });
                }
                //AddErrors(result);
            }
            ViewBag.Kinds = _cache.Get<List<Kind>>("Kinds");
            ViewBag.Towns = _cache.Get<List<City>>("Towns");
            return View(model);
        }

        [Area("Admin")]
        public async Task<IActionResult> AddManagersToLogbooks(CreateEstablishmentViewModel establishmentModel)
        {
            if (string.IsNullOrEmpty(establishmentModel.LogbookNames)) {
                return PartialView("_MoreInfo");
            };
            var logbooks = establishmentModel.LogbookNames
                        .Split(',')
                        .Select(n=>n.Trim())
                        .ToList();
            establishmentModel.LogbookViewModels = new List<CreateLogbookViewModel>();
            foreach (var item in logbooks)
            {
                establishmentModel.LogbookViewModels.Add(new CreateLogbookViewModel {Name=item });
            }
            var managers = await _userService.ListManagersCreatedByAdmin(_userManager.GetUserId(User));
            if(managers!=null)
                ViewBag.Managers = managers;
            return PartialView("_AddManagers", establishmentModel);
        }

        [Area("Admin")]
        public async Task<IActionResult> Created(int id)
        {
            var est = await this._establishmentService.GetEstablishmentById(id);
            est = await this._establishmentService.SetAllPropsOfEstablishment(est.Id);
            
            var model = this._establishmentMapper.Map(est);

            return View("Created",model);
        }

        [Area("Admin")]
        [HttpGet]
        public async Task<IActionResult> UpdateEstablishment(int id)
        {
            var est = await this._establishmentService.SetAllPropsOfEstablishment(id);
            est.DeletedLogbooks = await this._logbookService.FindDeletedLogbooksByEstablishment(id);

            var model = this._createEstablishmentMapper.Map(est);
            model.CreateOrUpdate = "update";
            _cache.Set(_userManager.GetUserName(User), model);

            var kinds = _cache.Get("Kinds");
            var towns = _cache.Get("Towns");

            ViewBag.Kinds = kinds;
            ViewBag.Towns = towns;

            return View("CreateEstablishment",model);
        }

        [Area("Admin")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> UpdateEstablishment([FromBody] CreateEstablishmentViewModel model)
        {
            var cacheModel = _cache.Get<CreateEstablishmentViewModel>(_userManager.GetUserName(User));
            var imgModel = cacheModel.ImageModel;
            model.ImageModel = imgModel;
            var establishment = await _establishmentService.SetAllPropsOfEstablishment(cacheModel.Id);
            Establishment foundEstablishment = null;
            if (!string.IsNullOrEmpty(model.Name)
                && !string.IsNullOrEmpty(model.City)
                && !string.IsNullOrEmpty(model.Street)
                && (model.Name!=establishment.Name
                || model.City!=establishment.Address?.City?.Name
                || model.Street!=establishment.Address?.Street))
            {
                var address = await _addressService.FindAddress(model.City, model.Street);
                if (address == null) address = await _addressService.CreateAddress(model.City, model.Street);
                model.Address = address;
                foundEstablishment = await _establishmentService.FindEstablishmentByNameAndAddress(model.Name,                                                                                              address.Id);
            }else 
            if (model.City == establishment.Address?.City?.Name&& model.Street == establishment.Address?.Street)
            {
                model.Address = establishment.Address;
            }
            var kind = await _kindService.GetKindByName(model.Kind);
            if (!string.IsNullOrEmpty(model.Name)
                && model.Address != null
                && (foundEstablishment == null || foundEstablishment.IsDeleted))
            {
                IdentityResult result = null;
                var newFileName = "";
                if (string.IsNullOrEmpty(establishment.Image)||establishment.Image==defaultImage)
                {
                    newFileName = model.Name.Replace(" ", "") + '_' + model.City.Replace(" ", "") + '_' + model.Street.Replace(" ", "");
                }
                else
                {
                    newFileName = establishment.Image;
                }
                imgModel.SaveByteImageInWwwroot(newFileName,
                                    "images/establishments/",
                                    ref newFileName,
                                    _hostingEnvironment);
                string webRootPath = _hostingEnvironment.WebRootPath;
                if (!System.IO.File.Exists(webRootPath+"/images/establishments/" + newFileName + ".jpg")) newFileName = defaultImage;

                establishment.Address = model.Address;
                    establishment.Name = model.Name;
                    establishment.IsDeleted = false;
                    establishment.Image = newFileName;
                    establishment.Kind = kind;
                    establishment.AdminId = _userManager.GetUserId(User);


                foreach (var id in model.DeletedLogbooks)
                {
                    await this._logbookService.DeleteLogbook(int.Parse(id));
                }

                foreach (var lb in model.LogbookViewModels)
                {
                    var existing = await this._logbookService.UpdateLogbook(lb.Name, establishment.Id, lb.Managers);
                    if (existing == null)
                        await this._logbookService.CreateLogbookUsers(lb.Name, establishment.Id, lb.Managers);
                }

                foreach (var id in model.LogbooksToUndelete)
                {
                    await this._logbookService.UndeleteLogbook(int.Parse(id));
                }

                if ((model.LogbookViewModels == null
                    || model.LogbookViewModels.Count == 0)
                    && !string.IsNullOrEmpty(model.LogbookNames))
                {
                    var toCreate = model.LogbookNames.Split(',').Select(s=>s.Trim());
                    foreach (var lb in toCreate)
                    {
                        var existing = await this._logbookService.UpdateLogbook(lb, establishment.Id, null);
                        if (existing == null)
                            await this._logbookService.CreateLogbookUsers(lb, establishment.Id, null);
                    }
                }

                if (establishment != null && establishment.IsDeleted != true)
                {
                    model.Id = establishment.Id;
                    _cache.Set(_userManager.GetUserName(User), model);
                    await _establishmentService.UpdateEstablishment(establishment);
                    var url = "/Admin/CreateEstablishment/Created/" + establishment.Id.ToString();
                    return Json(new { Url = url });
                }
                AddErrors(result);
            }
            ViewBag.Kinds = _cache.Get<List<Kind>>("Kinds");
            ViewBag.Towns = _cache.Get<List<City>>("Towns");
            return View(model);
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.AdminPanel), "Home");
            }
        }

        #endregion
    }
}
    
