﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using RestaurantService.Areas.Admin.Models;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services;
using RestaurantService.Services.Contracts;

namespace RestaurantService.Areas.Admin.Controllers
{
    public class LogbookController : Controller
    {
        private const string defaultImage = "_default";

        private IMemoryCache _cache;
        private readonly IEstablishmentService _establishmentService;
        private readonly ILogbookService _logbookService;
        private readonly IKindService _kindService;
        private readonly IAddressService _addressService;
        private readonly ICityService _cityService;
        private readonly IUserService _userService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IMapper<CreateLogbookViewModel, Logbook> _logbookMapper;

        private readonly UserManager<ApplicationUser> _userManager;

        public LogbookController(
            IMemoryCache cache,
            IEstablishmentService establishmentService,
            IHostingEnvironment hostingEnvironment,
            IMapper<CreateLogbookViewModel, Logbook> logbookMapper,
            UserManager<ApplicationUser> userManager,
            ILogbookService logbookService,
            IKindService kindService,
            IAddressService addressService,
            ICityService cityService,
            IUserService userService
            )
        {
            _establishmentService = establishmentService;
            _cache = cache;
            _hostingEnvironment = hostingEnvironment;
            _logbookMapper = logbookMapper;
            _userManager = userManager;
            _logbookService = logbookService;
            _kindService = kindService;
            _addressService = addressService;
            _cityService = cityService;
            _userService = userService;
        }

        [Area("Admin")]
        [HttpGet]
        public async Task<IActionResult> UpdateLogbook(int id)
        {
            var lb = await this._logbookService.FindLogbookWithEstablishmentById(id);
            lb = await this._logbookService.SetManagersOfLogbook(lb);
            ViewBag.Managers = await this._userService.ListAllManagersByAdminExceptSpecifiedManagers(_userManager.GetUserId(User), lb.Managers.ToList());
            var model = this._logbookMapper.Map(lb);
            return View(model);
        }

        [Area("Admin")]
        [HttpPost]
        public async Task<IActionResult> UpdateLogbook(CreateLogbookViewModel lb, int id)
        {
            var logbook = await this._logbookService.UpdateLogbook(id,lb.Name,lb.Managers,lb.ManagersToLeave);
            var model = this._logbookMapper.Map(logbook);
            ViewBag.Managers = await this._userService.ListAllManagersByAdminExceptSpecifiedManagers(_userManager.GetUserId(User), logbook.Managers.ToList());
            return View(model);
        }
    }
}