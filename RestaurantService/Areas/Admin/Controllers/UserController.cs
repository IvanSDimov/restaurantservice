﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RestaurantService.Areas.Admin.Models;
using RestaurantService.Data.Models;
using RestaurantService.Extensions;
using RestaurantService.Models;
using RestaurantService.Models.AccountViewModels;
using RestaurantService.Services;
using RestaurantService.Services.Contracts;

namespace RestaurantService.Areas.Admin.Controllers
{
    public class UserController : Controller
    {
        private const string defaultImage = "_default";

        private IMemoryCache _cache;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        //private readonly IEmailSender _emailSender;
        private readonly IUserService _userService;
        private readonly ILogbookService _logbookService;
        private readonly ILogger _logger;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IMapper<UserViewModel, ApplicationUser> _userMapper;
        private readonly IMapper<CreateUserViewModel, ApplicationUser> _createUserMapper;
        private readonly IMapper<LogbookViewModelForManager, Logbook> _logbookMapper;
        private readonly IServiceProvider _serviceProvider;

        public UserController(
            IMemoryCache cache,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            //IEmailSender emailSender,
            IUserService userService,
            ILogbookService logbookService,
            ILogger<UserController> logger,
            IHostingEnvironment hostingEnvironment,
            IMapper<UserViewModel, ApplicationUser> userMapper,
            IMapper<CreateUserViewModel, ApplicationUser> createUserMapper,
            IMapper<LogbookViewModelForManager, Logbook> logbookMapper,
            IServiceProvider serviceProvider)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _userService = userService;
            _logbookService = logbookService;
            _logger = logger;
            _cache = cache;
            _hostingEnvironment = hostingEnvironment;
            _userMapper = userMapper;
            _createUserMapper = createUserMapper;
            _logbookMapper = logbookMapper;
            _serviceProvider = serviceProvider;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [Area("Admin")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> CreateUser(string returnUrl = null)
        {
            var model = new CreateUserViewModel()
            {
                ImageModel = new ImageViewModel()
                {
                    StyleClass= "border-radius:50%;width:300px;padding-top:10px;padding-bottom:10px;",
                    Default= "../../../images/avatars/_default.jpg"
                }
            };
            _cache.Set(_userManager.GetUserName(User), model);
            ViewData["ReturnUrl"] = returnUrl;
            var roles = await _userService.ListRoles();
            _cache.Set("Roles", roles);
            ViewBag.Roles = roles;
            var cacheModel=_cache.Get<CreateUserViewModel>(_userManager.GetUserName(User));
            return View(cacheModel);
        }

        [Area("Admin")]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateUser(CreateUserViewModel model,string role, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            var cacheModel = _cache.Get<CreateUserViewModel>(_userManager.GetUserName(User));
            var imgModel = cacheModel.ImageModel;
            
            ApplicationUser user = null;
            if (!string.IsNullOrEmpty(model.UserName))
            {
                user = await _userService.FindUserByName(model.UserName);
                if (user != null && user.IsDeleted != true)
                {
                ModelState.AddModelError(string.Empty, "UserName already exists!");
                }
            }
            if (!string.IsNullOrEmpty(model.UserName)
                && !string.IsNullOrEmpty(model.Email)
                && !string.IsNullOrEmpty(model.Password)
                && model.Password==model.ConfirmPassword
                && (user==null|| user.IsDeleted))
            {
                IdentityResult result = null;
                var newFileName = model.UserName;

                imgModel.SaveByteImageInWwwroot(defaultImage,
                                    "images/avatars/",
                                    ref newFileName,
                                    _hostingEnvironment);
                string webRootPath = _hostingEnvironment.WebRootPath;
                if (!System.IO.File.Exists(webRootPath + "/images/avatars/" + newFileName + ".jpg")) newFileName = defaultImage;

                if (user != null && user.IsDeleted)
                {
                    user.Email = model.Email;
                    user.UserName = model.UserName;
                    user.IsDeleted = false;
                    user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, model.Password);
                    user.Avatar = newFileName;
                    user.AdminId = _userManager.GetUserId(User);
                    user.Role = role;
                    result = await _userManager.UpdateAsync(user);
                }
                else
                {
                    user = new ApplicationUser {
                        UserName = model.UserName,
                        Email = model.Email,
                        Avatar = newFileName,
                        Role = role,
                        AdminId= _userManager.GetUserId(User)
                };
                    result = await _userManager.CreateAsync(user, model.Password);
                }
                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");
                    await _userManager.AddToRoleAsync(user, role);
                    _cache.Remove(_userManager.GetUserName(User));
                    _cache.Remove("Roles");
                    return View("Details", _userMapper.Map(user));
                }
                AddErrors(result);
            }

            model.ImageModel = imgModel;
            ViewBag.Roles = _cache.Get<List<string>>("Roles");
            return View(model);
        }

        [Area("Admin")]
        [HttpGet]
        public async Task<IActionResult> UpdateUser(string id)
        {
            var user = await this._userService.FindUserById(id);
            user= await this._userService.SetRole(user);

            var model = this._createUserMapper.Map(user);
            model.CreateOrUpdate = "update";
            _cache.Set(_userManager.GetUserName(User), model);

            if (user.Role == "Manager")
            {
                model.Logbooks = await this.DivideLogbooksModelsByEstablishments(user);
            }

            return View("CreateUser", model);
        }

        [Area("Admin")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> UpdateUser(CreateUserViewModel model)
        {
            var cacheModel = _cache.Get<CreateUserViewModel>(_userManager.GetUserName(User));
            var imgModel = cacheModel.ImageModel;
            model.ImageModel = imgModel;
            var user = await this._userService.FindUserById(model.Id);
            user = await this._userService.SetRole(user);
            ApplicationUser foundUser = null;
            if (!string.IsNullOrEmpty(model.UserName)&&model.UserName!=user.UserName)
            {
                foundUser = await this._userService.FindUserByName(model.UserName);
            }

            if (!string.IsNullOrEmpty(model.UserName)
                && !string.IsNullOrEmpty(model.Email)
                && (foundUser == null || foundUser.IsDeleted))
            {
                IdentityResult result = null;
                var newFileName = "";
                if (string.IsNullOrEmpty(user.Avatar) || user.Avatar == defaultImage)
                    newFileName = model.UserName;
                else
                    newFileName = user.Avatar;
                    imgModel.SaveByteImageInWwwroot(newFileName,
                                        "images/avatars/",
                                        ref newFileName,
                                        _hostingEnvironment);
                string webRootPath = _hostingEnvironment.WebRootPath;
                if (!System.IO.File.Exists(webRootPath + "/images/avatars/" + newFileName + ".jpg")) newFileName = defaultImage;

                user.UserName = model.UserName;
                user.IsDeleted = false;
                user.Email = model.Email;
                //user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, model.Password);
                user.Avatar = newFileName;
                user.AdminId = _userManager.GetUserId(User);
                if(user.Role=="Manager" && model?.LogbooksToLeave!=null)
                {
                    await this._userService.UnassignUserFromLogbooks(user.Id, model.LogbooksToLeave);
                }
                    result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");
                    _cache.Remove(_userManager.GetUserName(User));
                    return RedirectToAction("Details", new { id=user.Id});
                }
                AddErrors(result);
            }
            model.ImageModel = imgModel;
            ViewBag.Roles = _cache.Get<List<string>>("Roles");
            return View("CreateUser",model);
        }

        [Area("Admin")]
        [AllowAnonymous]
        public async Task<IActionResult> Details(string id)
        {
            var user =await _userService.FindUserById(id);
            user = await _userService.SetRole(user);
            var userModel = _userMapper.Map(user);
            if (user.Role == "Manager")
            {
                userModel.Logbooks = await this.DivideLogbooksModelsByEstablishments(user);
            }
            return View(userModel);
        }



        [Area("Admin")]
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteUser(UserViewModel model)
        {
            var user = await _userService.FindUserById(model.Id);
            await _userService.DeleteUser(user);
            return RedirectToAction("AdminPanel", "Home");
        }

        [Area("Admin")]
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UndeleteUser(UserViewModel model)
        {
            var user = await _userService.FindUserById(model.Id);
            await _userService.UndeleteUser(user);
            return RedirectToAction("AdminPanel", "Home");
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.AdminPanel), "Home");
            }
        }

        private async Task<Dictionary<Tuple<string, int>, List<LogbookViewModelForManager>>> DivideLogbooksModelsByEstablishments(ApplicationUser user)
        {
            var logbooks = await this._logbookService.GetAllManagerLogbooks(user.Id);
            var dictionary = await this._logbookService.DivideLogbooksByEstablishments(logbooks);
            var newDictionary = new Dictionary<Tuple<string, int>, List<LogbookViewModelForManager>>();
            foreach (var item in dictionary)
            {
                newDictionary[item.Key] = item.Value.Select(l => this._logbookMapper.Map(l)).ToList();
            }
            return newDictionary;
        }
        #endregion
    }
}