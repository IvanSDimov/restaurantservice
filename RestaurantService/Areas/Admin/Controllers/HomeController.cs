﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using RestaurantService.Areas.Admin.Models;
using RestaurantService.Areas.Manager.Models;
using RestaurantService.Data.Models;
using RestaurantService.Extensions;
using RestaurantService.Models;
using RestaurantService.Services;
using RestaurantService.Services.Contracts;

namespace RestaurantService.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class HomeController : Controller
    {
        private readonly IJsonService jsonService;
        private readonly IUserService userService;
        private readonly IEstablishmentService establishmentService;
        private readonly UserManager<ApplicationUser> userManager;
        private IMemoryCache cache;
        //private readonly IPdfService pdfService;


        private readonly IMapper<AllUsersViewModel, IList<ApplicationUser>> allUserMapper;
        private readonly IMapper<ManagerViewModel, ApplicationUser> userMapper;
        private readonly IMapper<AllEstablishmentsViewModels, List<Establishment>> allEstablishmentsMapper;

        

        private readonly int onPage = 6;


        public HomeController(
            IJsonService jsonService,
            IUserService userService,
            IEstablishmentService establishmentService,
            UserManager<ApplicationUser> userManager,
            IMemoryCache cache,
        //IPdfService pdfService,
        IMapper<AllUsersViewModel, IList<ApplicationUser>> allUserMapper,
        IMapper<AllEstablishmentsViewModels, List<Establishment>> allEstablishmentsMapper,
        IMapper<ManagerViewModel, ApplicationUser> userMapper)
        {

            this.jsonService = jsonService;
            //this.pdfService = pdfService;
            this.userService = userService;
            this.establishmentService = establishmentService;
            this.userManager = userManager;
            this.cache = cache;
            this.userMapper = userMapper;
            this.allEstablishmentsMapper = allEstablishmentsMapper;
            this.allUserMapper = allUserMapper;

        }

        [Area("Admin")]
        public  IActionResult Index()
        {
            ViewBag.MenuBar = "AdminMenu";
            cache.Set(userManager.GetUserId(User), new SearchViewModel());
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("Session")))
            {
                cache.Set(HttpContext.Session.GetString("Session"), new SearchViewModel());
            }
            return View();
        }


        [Area("Admin")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AdminPanel(int page=1)
        {
            var users_count = await userService.ListUsersByAdminOnPage(onPage, page - 1,userManager.GetUserId(User));
            foreach (var item in users_count.Item2)
            {
                await userService.SetRole(item);
            }

            var mappedUsers = allUserMapper.Map(users_count.Item2);
            mappedUsers.Roles = await userService.ListRoles();
            var pages = 1;
            if (users_count.Item1>0)
                 pages = await userService.GetPages(onPage, users_count.Item1);
            mappedUsers.PagingModel = new PagingViewModel
            {
                CurrPage=page,
                Pages = pages,
                Area = "Admin",
                Controller = "Home",
                Action = "AdminPanel"
            };
            return View(mappedUsers);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeletedUsers(int page = 1)
        {
            var users_count = await userService.ListDeletedUsersByAdminOnPage(onPage, page - 1, userManager.GetUserId(User));
            foreach (var item in users_count.Item2)
            {
                await userService.SetRole(item);
            }

            var mappedUsers = allUserMapper.Map(users_count.Item2);
            mappedUsers.Roles = await userService.ListRoles();
            var pages = 1;
            if (users_count.Item1 > 0)
                pages = await userService.GetPages(onPage, users_count.Item1);
            mappedUsers.PagingModel = new PagingViewModel
            {
                CurrPage = page,
                Pages = pages,
                Area = "Admin",
                Controller = "Home",
                Action = "DeletedUsers"
            };
            return View("AdminPanel", mappedUsers);
        }





        [Area("Admin")]
        [Authorize(Roles = "Admin")]
        public IActionResult Seed()
        {
            ViewData["Message"] = "Seed";
            jsonService.DatabaseSeed();
            return View();
        }
    }
}