﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using RestaurantService.Areas.Manager.Models;
using RestaurantService.Data.Models;
using RestaurantService.Models;
using RestaurantService.Services.Contracts;

namespace RestaurantService.Areas.Manager.Controllers
{
    [Area("Manager")]
    [Authorize(Roles = "Manager")]
    public class HomeController : Controller
    {
        private readonly IEstablishmentService establishmentService;
        private readonly IMapper<AllEstablishmentsViewModels, List<Establishment>> allEstablishmentsMapper;
        private readonly ILogbookService logbookService;
        private readonly IServiceProvider serviceProvider;
        private readonly IMapper<LogbookViewModel, Logbook> LogbookMapper;
        private readonly IMapper<AllLogbookViewModel, List<Logbook>> AllLogbookMapper;


        public HomeController(IEstablishmentService establishmentService,
            IMapper<AllEstablishmentsViewModels, List<Establishment>> allEstablishmentsMapper,
            ILogbookService logbookService,
            IServiceProvider serviceProvider,
            IMapper<LogbookViewModel, Logbook> LogbookMapper,
            IMapper<AllLogbookViewModel, List<Logbook>> AllLogbookMapper)
        {
            this.allEstablishmentsMapper = allEstablishmentsMapper;
            this.establishmentService = establishmentService;
            this.logbookService = logbookService;
            this.serviceProvider = serviceProvider;
            this.LogbookMapper = LogbookMapper;
            this.AllLogbookMapper = AllLogbookMapper;
        }
        [Area("Manager")]
        [Route("Home/Index")]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> Index()
        {
            var userManeger = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var userid = userManeger.GetUserId(User);

            var logbooksToMap = await logbookService.GetAllManagerLogbooks(userid);
            if (logbooksToMap.Count == 1) return RedirectToAction("Details", "Logbook", new { id = logbooksToMap.First().Id });
            var dictionary = await logbookService.DivideLogbooksByEstablishments(logbooksToMap);
            var dictionaryForModel = new Dictionary<Tuple<string, int>, List<LogbookViewModel>>();
            int maxCount = 0;
            foreach (var item in dictionary)
            {
                dictionaryForModel[item.Key] = item.Value.Select(LogbookMapper.Map).ToList();
                if (item.Value.Count > maxCount) maxCount = item.Value.Count;
            }

            var AllLogbookViewModels = AllLogbookMapper.Map(logbooksToMap);
            AllLogbookViewModels.logbookViewModelsWithEstablishments = dictionaryForModel;
            AllLogbookViewModels.MaxLogbooksInEstablishment = maxCount;

            return View(AllLogbookViewModels);
        }
    }
}
