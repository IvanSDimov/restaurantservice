﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using RestaurantService.Areas.Manager.Models;
using RestaurantService.Data.Models;
using RestaurantService.Extensions;
using RestaurantService.Hubs;
using RestaurantService.Models;
using RestaurantService.Services;
using RestaurantService.Services.Contracts;

namespace RestaurantService.Areas.Manager.Controllers
{
    public class LogbookController : Controller
    {
        private IMemoryCache cache;
        private readonly IEstablishmentService establishmentService;
        private readonly IMapper<EstablishmentViewModel, Establishment> eastablishmentMapper;
        private IServiceProvider serviceProvider;
        private readonly IUserService userService;
        private readonly IManagerCommentService managerCommentService;
        private readonly ILogbookService logbookService;
        private readonly ICategoryService categoryService;
        private readonly IMapper<LogbookViewModel, Logbook> logbookMapper;
        private readonly IMapper<AllManagerCommentsViewModel, List<ManagerComment>> allCommentsMapper;
        private readonly IMapper<ManagerCommentViewModel, ManagerComment> commentsMapper;
        private readonly UserManager<ApplicationUser> userManager;
        private int onPage = 5;
        private readonly IHubContext<ApplicationHub> hubContext;

        public LogbookController(
            IMemoryCache cache,
            IEstablishmentService establishmentService,
            IMapper<EstablishmentViewModel, Establishment> eastablishmentMapper,
            IServiceProvider serviceProvider,
            IManagerCommentService managerCommentService,
            ICategoryService categoryService,
            ILogbookService logbookService,
            IMapper<LogbookViewModel, Logbook> logbookMapper,
            IUserService userService,
            IMapper<AllManagerCommentsViewModel, List<ManagerComment>> allCommentsMapper,
            IMapper<ManagerCommentViewModel, ManagerComment> commentsMapper,
             IHubContext<ApplicationHub> hubContext,
             UserManager<ApplicationUser> userManager)
        {
            this.cache = cache;
            this.eastablishmentMapper = eastablishmentMapper;
            this.establishmentService = establishmentService;
            this.serviceProvider = serviceProvider;
            this.managerCommentService = managerCommentService;
            this.logbookService = logbookService;
            this.logbookMapper = logbookMapper;
            this.userService = userService;
            this.allCommentsMapper = allCommentsMapper;
            this.commentsMapper = commentsMapper;
            this.categoryService = categoryService;
            this.hubContext = hubContext;
            this.userManager = userManager;
        }
        [Area("Manager")]
        [Route("Logbook/Details/{id}")]
        [Authorize(Roles = "Manager")]
        [HttpGet]
        public async Task<IActionResult> Details(int id, int page = 1)
        {
            var categories = await categoryService.ListAllCategories();
            var managers = await userService.ListlogbookManagers(id);
            cache.Set("Categories", categories);
            cache.Set("Managers", managers);
            ViewBag.Categories = categories;
            ViewBag.Managers = managers;
            ViewBag.LogbookId = id;

            var logbookToMap = await logbookService.FindLogbookById(id);

            var count = await logbookService.GetNumberOfCommentsOfAnLogbook(logbookToMap);

            await logbookService.SetCommentsOnPageToLogbook(onPage, page - 1, logbookToMap);
            int pages = 1;
            if (count > 0)
            {
                pages = await logbookService.GetPages(onPage, count);
            }
            var logbookViewModel = logbookMapper.Map(logbookToMap);

            logbookViewModel.ImageModel = new ImageViewModel()
            {
                StyleClass = "height:80px;padding:0;margin:0 auto",
                Default = null
            };
            logbookViewModel.PagingModel = new PagingViewModel()
            {
                CurrPage = page,
                Pages = pages,
                Area = "Manager",
                Controller = "Logbook",
                Action = "Details"
            };
            cache.Set(userManager.GetUserName(User), logbookViewModel);
            ViewBag.MenuBar = "SerchComments";
            return View(logbookViewModel);
        }
        [Area("Manager")]
        [Authorize(Roles = "Manager")]
        [HttpGet]
        public async Task<IActionResult> LogbookCommets(CommentSearchViewModel model, string IsChanged, int id, int page = 1)
        {
            var categories = cache.Get("Categories");
            var managers = cache.Get("Managers");
            ViewBag.Categories = categories;
            ViewBag.Managers = managers;
            var savedModel = cache.Get<CommentSearchViewModel>(userManager.GetUserId(User));
            if (model.LogbookId == null && savedModel != null) model.LogbookId = savedModel.LogbookId;
            ViewBag.LogbookId = model.LogbookId;
            bool IsInitial = false;
            ViewBag.MenuBar = "SerchComments";
            if (IsChanged == "changed")
            {
                cache.Set(userManager.GetUserId(User), model);
                IsInitial = true;
            }
            model = cache.Get<CommentSearchViewModel>(userManager.GetUserId(User));
            if (model.Categories == null)
            {
                model.Categories = new List<string>();
            }
            if (model.Managers == null)
            {
                model.Managers = new List<string>();
            }
            int pages = 0;
            var comments = await managerCommentService.SearchManagerComments(
                onPage,
                page - 1,
                int.Parse(model.LogbookId),
                model.FromDate,
                model.ToDate,
                model.Managers,
                model.Categories.Select(int.Parse).ToList());
            var logbook = new Logbook();

            logbook = await logbookService.FindLogbookById(int.Parse(model.LogbookId));

            logbook.ManagerComments = comments.Item1.ToList();
            var logbookViewModel = logbookMapper.Map(logbook);
            if (comments.Item1.Count > 0)
            {
                pages = await establishmentService.GetPages(onPage, comments.Item2);
            }
            logbookViewModel.PagingModel = new PagingViewModel()
            {
                CurrPage = page,
                Pages = pages,
                Area = "Manager",
                Controller = "Logbook",
                Action = "LogbookCommets"
            };
            if (!IsInitial)
            {
                ViewBag.Categories = cache.Get("Categories");
                ViewBag.Managers = cache.Get("Managers");
                return View("_ShowCommentsPartial", logbookViewModel);
            }
            return PartialView("_ShowCommentsPartial", logbookViewModel);
        }

        [Area("Manager")]
        [Route("Logbook/AddManagerComments")]
        [Authorize(Roles = "Manager")]
        [HttpPost]
        public async Task<IActionResult> AddManagerComment(int logbookId, string commentText, List<string> Categories)
        {
            var logbook = await logbookService.FindLogbookById(logbookId);
            try
            {
                if (!string.IsNullOrEmpty(commentText))
                {
                    var categoryIdsParsed = new List<int>();
                    foreach (var categoryId in Categories)
                    {
                        categoryIdsParsed.Add(int.Parse(categoryId));
                    }
                    var userManeger = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                    var userid = userManeger.GetUserId(User);
                    var model = cache.Get<IImigable>(userManager.GetUserName(User))?.ImageModel;
                    var manager = await userService.FindUserById(userid);
                    var comment = await managerCommentService.AddComment(commentText, manager, logbook, model?.ByteImage);
                    await managerCommentService.EditCategories(categoryIdsParsed, comment.Id);
                    cache.Remove(userManager.GetUserName(User));
                }

                await hubContext.Clients.All.SendAsync("Details", logbookId);
                return RedirectToAction("Details", new { id = logbookId });
            }
            catch (ArgumentException ex )
            {
                return View("_CustomError", new CustomErrorViewModel { ExceptionMessage = ex.Message });                
            }
        }

        [Area("Manager")]
        [Authorize(Roles = "Manager")]
        [HttpPost]
        public async Task<IActionResult> EditManegerComment(string newcommentText, int LogbookId, int commentId, int page)
        {
            try
            {
                var CommentToEdit = await managerCommentService.FindCommentById(commentId);
                await managerCommentService.EditComment(newcommentText, CommentToEdit);
                return RedirectToAction("Details", new { id = LogbookId, page = page });
            }
            catch (ArgumentException ex)
            {
                return View("_CustomError", new CustomErrorViewModel { ExceptionMessage = ex.Message });
            }
        }
        [Area("Manager")]
        [Authorize(Roles = "Manager")]
        [Route("/Manager/Logbook/ManagerCommentsOfManager/{id}/{logbookId}/{page}")]
        public async Task<IActionResult> ManagerCommentsOfManager(string id, int logbookId, int page = 1)
        {
            try
            {
                var pages = 0;
                var comments_count = await managerCommentService.ManagerCommentsOfManager(id, logbookId, page - 1, onPage);

                var logbook = await logbookService.FindLogbookById(logbookId);

                var logbookViewModel = logbookMapper.Map(logbook);
                var commentViewModels = comments_count.Item1.Select(c => this.commentsMapper.Map(c));
                logbookViewModel.commentsViewModels = commentViewModels.ToList();
                if (comments_count.Item1.Count() > 0)
                {
                    pages = await establishmentService.GetPages(onPage, comments_count.Item2);
                }
                logbookViewModel.FilterId = id;
                logbookViewModel.FilterName = commentViewModels.First().AutorName;
                logbookViewModel.PagingModel = new PagingViewModel()
                {
                    CurrPage = page,
                    Pages = pages,
                    Area = "Manager",
                    Controller = "Logbook",
                    Action = "ManagerCommentsOfManager"
                };
                ViewBag.Categories = cache.Get("Categories");
                return View(logbookViewModel);
            }
            catch (ArgumentException ex)
            {
                return View("_CustomError", new CustomErrorViewModel { ExceptionMessage = ex.Message });
            }
        }

        [Area("Manager")]
        [Authorize(Roles = "Manager")]
        [Route("/Manager/Logbook/CategoryComments/{logbookId}/{id}/{page}")]
        public async Task<IActionResult> CategoryComments(int id, int logbookId, int page = 1)
        {
            try
            {
                var comments_count = await managerCommentService.ManagerCommentsByCategory(id, logbookId, page - 1, onPage);
                var logbook = await logbookService.FindLogbookById(logbookId);
                var logbookModel = logbookMapper.Map(logbook);

                var commentViewModels = comments_count.Item1.Select(c => this.commentsMapper.Map(c)).ToList();
                logbookModel.commentsViewModels = commentViewModels;
                var pages = 0;
                if (commentViewModels.Count() > 0)
                {
                    pages = await establishmentService.GetPages(onPage, comments_count.Item2);
                }
                var category = await this.categoryService.FindCategoryById(id);
                logbookModel.FilterName = category.Name;
                logbookModel.FilterId = category.Id.ToString();
                logbookModel.PagingModel = new PagingViewModel()
                {
                    CurrPage = page,
                    Pages = pages,
                    Area = "Manager",
                    Controller = "Logbook",
                    Action = "CategoryComments"
                };
                ViewBag.Categories = cache.Get("Categories");
                return View(logbookModel);
            }
            catch (ArgumentException ex)
            {
                return View("_CustomError", new CustomErrorViewModel { ExceptionMessage = ex.Message });
            }
        }

        [Area("Manager")]
        [Authorize(Roles = "Manager")]
        [Route("Logbook/StateEdit")]
        public async Task StateEdit([FromBody]StateEditModel model)
        {
            await managerCommentService.EditState(model.CommentId, model.State);
        }

        [Area("Manager")]
        [Authorize(Roles = "Manager")]
        [Route("Logbook/EditCategories/{id}")]
        public async Task<IActionResult> EditCategories(string commentId, List<string> Categories, int id, int page = 1, string currAction = "Details", string filter = null)
        {
            var categoryIdsParsed = new List<int>();
            if (Categories.Count > 0)
                foreach (var categoryId in Categories)
                {
                    categoryIdsParsed.Add(int.Parse(categoryId));
                }
            var commentIdParsed = int.Parse(commentId);
            await managerCommentService.EditCategories(categoryIdsParsed, commentIdParsed);
            if (currAction == "Details")
                return RedirectToAction(currAction, new { id = id, page = page });
            else if (currAction == "CategoryComments")
            {
                var comment = await managerCommentService.FindCommentById(commentIdParsed);
                var filterParsed = int.Parse(filter);
                return RedirectToAction(currAction, new { logbookId = comment.LogbookId, id = filterParsed, page = page });
            }
            else
            {
                var comment = await managerCommentService.FindCommentById(commentIdParsed);
                return RedirectToAction(currAction, new { logbookId = comment.LogbookId, id = filter, page = page });
            }
        }

        [Area("Manager")]
        [Authorize(Roles = "Manager")]
        [Route("/Manager/Logbook/DeleteImage/{id}/{commentId}")]
        public async Task<IActionResult> DeleteImage(int commentId, int id, int page = 1, string currAction = "Details", string filter = null)
        {

            await managerCommentService.DeleteImage(commentId);
            return RedirectToAction(currAction, new { id = id, page = page });

        }

        [Area("Manager")]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> DisplayImage(int id)
        {
            var comment = await managerCommentService.FindCommentById(id);
            var model = commentsMapper.Map(comment);
            return View(model);

        }

    }
}
