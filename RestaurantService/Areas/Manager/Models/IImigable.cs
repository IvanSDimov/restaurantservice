﻿using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Manager.Models
{
    public interface IImigable
    {
        ImageViewModel ImageModel { get; set; }
    }
}
