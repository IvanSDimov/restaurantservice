﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Manager.Models
{
    public class AllManagerCommentsViewModel
    {
        public ICollection<ManagerCommentViewModel> managerCommentViewModels { get; set; }
    }
}
