﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Manager.Models
{
    public class AllLogbookViewModel
    {
        public ICollection<LogbookViewModel> logbookViewModels { get; set; }
        public Dictionary<Tuple<string, int>, List<LogbookViewModel>> logbookViewModelsWithEstablishments { get; set; }
        public int MaxLogbooksInEstablishment { get; set; }
    }
}
