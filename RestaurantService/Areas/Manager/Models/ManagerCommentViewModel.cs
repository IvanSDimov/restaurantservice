﻿using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Manager.Models
{
    public class ManagerCommentViewModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string AutorName { get; set; }
        public string AuthorId { get; set; }
        public string AuthorAvatar { get; set; }
        public string LogbookName { get; set; }
        public int? LogbookId { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsDone { get; set; }
        public IList<Category> Category { get; set; }
        public byte[] ByteImage { get; set; }
        public IList<Category>Categories { get; set; }
    }
}
