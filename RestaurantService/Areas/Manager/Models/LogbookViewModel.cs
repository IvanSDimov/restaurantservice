﻿using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Manager.Models
{
    public class LogbookViewModel:IImigable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ManagerComment> managerComments { get; set; }
        public ICollection<ManagerCommentViewModel> commentsViewModels { get; set; }
        public PagingViewModel PagingModel { get; set; }
        public ImageViewModel ImageModel { get; set; }
        public string FilterName { get; set; }
        public string FilterId { get; set; }
    }
}
