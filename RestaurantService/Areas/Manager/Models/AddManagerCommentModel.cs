﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Manager.Models
{
    public class AddManagerCommentModel
    {
        public string commentText { get; set; }
        public int logbookId { get; set; }
    }
}
