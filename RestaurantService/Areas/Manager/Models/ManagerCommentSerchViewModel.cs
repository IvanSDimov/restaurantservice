﻿using RestaurantService.Data.Models;
using RestaurantService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Manager.Models
{
    public class ManagerCommentSerchViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Category Category { get; set; }
        public ApplicationUser Manager { get; set; }
    }
}
