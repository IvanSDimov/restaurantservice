﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Manager.Models
{
    public class StateEditModel
    {
        public int CommentId { get; set; }
        public bool State { get; set; }
    }
}
