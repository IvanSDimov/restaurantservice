﻿using RestaurantService.Areas.Admin.Models;
using RestaurantService.Areas.Manager.Models;
using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Manager.Mappers
{
    public class ManagerCommentMapper : IMapper<ManagerCommentViewModel, ManagerComment>
    {
        public ManagerCommentViewModel Map(ManagerComment entity)
        {
            return new ManagerCommentViewModel
            {
                Id = entity.Id,
                AutorName = entity.Author?.UserName,
                AuthorId = entity.Author?.Id,
                AuthorAvatar = "/images/avatars/" + entity.Author?.Avatar + ".jpg",
                LogbookName = entity.Logbook?.Name,
                LogbookId = entity.LogbookId,
                Text = entity.Text,
                DateCreated = entity.DateCreated,
                IsDone = entity.IsDone,
                ByteImage=entity.ByteImage,
                Categories=entity.Category
            };
        }
    }
}
