﻿using RestaurantService.Areas.Manager.Models;
using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Manager.Mappers
{
    public class AllManagerCommentsMapper : IMapper<AllManagerCommentsViewModel,List<ManagerComment>> 
    {
        public IMapper<ManagerCommentViewModel, ManagerComment> mapper;

        public AllManagerCommentsMapper (IMapper<ManagerCommentViewModel, ManagerComment> mapper)
        {
            this.mapper = mapper;
        }

        public AllManagerCommentsViewModel Map(List<ManagerComment> entity)
        {
            return new AllManagerCommentsViewModel
            {
                managerCommentViewModels = entity.Select(e => mapper.Map(e)).ToList()
            };
        }
    }
}
