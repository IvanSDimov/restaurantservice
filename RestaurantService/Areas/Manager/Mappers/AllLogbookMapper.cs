﻿using RestaurantService.Areas.Manager.Models;
using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Manager.Mappers
{
    public class AllLogbookMapper : IMapper<AllLogbookViewModel,List<Logbook>>
    {
        private readonly IMapper<LogbookViewModel, Logbook> LogbookMapper;
        public AllLogbookMapper(IMapper<LogbookViewModel, Logbook> LogbookMapper)
        {
            this.LogbookMapper = LogbookMapper;
        }
        public AllLogbookViewModel Map(List<Logbook> logbooks)
        {
            return new AllLogbookViewModel()
            {
                logbookViewModels = logbooks.Select(lg => LogbookMapper.Map(lg)).ToList()
            };
        }
    }
}
