﻿using RestaurantService.Areas.Manager.Models;
using RestaurantService.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantService.Areas.Manager.Mappers
{
    public class LogbookMapper : IMapper<LogbookViewModel, Logbook>
    {
        private IMapper<ManagerCommentViewModel, ManagerComment> commentMapper;

        public LogbookMapper(IMapper<ManagerCommentViewModel, ManagerComment> commentMapper)
        {
            this.commentMapper = commentMapper;
        }

        public LogbookViewModel Map(Logbook entity)
        {
           var model=new LogbookViewModel()
            {
                Id = entity.Id,
                Name = entity.Name,
                managerComments = entity.ManagerComments,
                
            };
            if (entity.ManagerComments != null && entity.ManagerComments.Count > 0)
            {
                model.commentsViewModels = entity.ManagerComments.Select(c => commentMapper.Map(c)).ToList();
            }
            return model;
        }
    }
}
